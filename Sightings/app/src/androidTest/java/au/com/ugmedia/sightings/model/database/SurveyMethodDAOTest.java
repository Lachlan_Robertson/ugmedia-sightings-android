package au.com.ugmedia.sightings.model.database;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.ugmedia.sightings.mocks.model.database.MockSightingsAssetHelper;
import au.com.ugmedia.sightings.mocks.model.database.MockSurveyMethodDAO;
import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

public class SurveyMethodDAOTest extends AndroidTestCase {
    private static final String TEST_CONTEXT_PREFIX = "SurveyMethodDAOTest_";
    private MockSurveyMethodDAO surveyMethodDAO;
    private JSONObject surveyMethod;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), TEST_CONTEXT_PREFIX);
        surveyMethodDAO = new MockSurveyMethodDAO(new MockSightingsAssetHelper(context));
        surveyMethod = new JSONObject();

        surveyMethod.put(SurveyMethodKeys.METHOD_NAME, "Test method");
        surveyMethod.put(SurveyMethodKeys.TIMES_USED, "1");
    }

    public void testGetAllSurveyMethodsWithData() {
        try {
            surveyMethodDAO.addSurveyMethod(surveyMethod);
            assertEquals("No survey methods available", 1, surveyMethodDAO.getAllSurveyMethods().size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetAllSurveyMethodsWithoutData() {
        try {
            assertEquals("Survey methods in DB", 0, surveyMethodDAO.getAllSurveyMethods().size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testResetTimesUsed() {
        try {
            surveyMethodDAO.addSurveyMethod(surveyMethod);
            surveyMethodDAO.resetTimesUsed();
            assertEquals("Survey method \"times used\" not reset",
                    0, surveyMethodDAO.getAllSurveyMethods().get(0).getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testIncrementTimesUsedWithValidName() {
        try {
            surveyMethodDAO.addSurveyMethod(surveyMethod);
            surveyMethodDAO.incrementTimesUsed(surveyMethod.getString(SurveyMethodKeys.METHOD_NAME));
            assertEquals("Survey method \"times used\" not incremented",
                    2, surveyMethodDAO.getAllSurveyMethods().get(0).getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testIncrementTimesUsedWithInvalidName() {
        try {
            surveyMethodDAO.addSurveyMethod(surveyMethod);
            surveyMethodDAO.incrementTimesUsed("Invalid");
            assertEquals("Survey method \"times used\" incremented",
                    1, surveyMethodDAO.getAllSurveyMethods().get(0).getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testDecrementTimesUsedWithValidName() {
        try {
            surveyMethodDAO.addSurveyMethod(surveyMethod);
            surveyMethodDAO.decrementTimesUsed(surveyMethod.getString(SurveyMethodKeys.METHOD_NAME));
            assertEquals("Survey method \"times used\" not decremented",
                    0, surveyMethodDAO.getAllSurveyMethods().get(0).getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testDecrementTimesUsedWithInvalidName() {
        try {
            surveyMethodDAO.addSurveyMethod(surveyMethod);
            surveyMethodDAO.decrementTimesUsed("Invalid");
            assertEquals("Survey method \"times used\" decremented",
                    1, surveyMethodDAO.getAllSurveyMethods().get(0).getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }
}