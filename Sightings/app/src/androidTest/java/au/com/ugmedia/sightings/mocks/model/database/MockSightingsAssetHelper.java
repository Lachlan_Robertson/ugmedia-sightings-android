package au.com.ugmedia.sightings.mocks.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MockSightingsAssetHelper extends SQLiteOpenHelper {
    private static final String TAG = MockSightingsAssetHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "junit_test_sightings";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_SIGHTINGS_TABLE_QUERY =
            "CREATE TABLE \"Sightings\" (" +
                    "\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , " +
                    "\"Timestamp\" DATETIME NOT NULL  DEFAULT CURRENT_TIMESTAMP, " +
                    "\"Data\" TEXT NOT NULL )";
    private static final String CREATE_SPECIES_TABLE_QUERY =
            "CREATE TABLE \"Species\" (" +
                    "\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , " +
                    "\"Scientific_Name\" TEXT NOT NULL , " +
                    "\"Use_Count\" INTEGER NOT NULL  DEFAULT 0, " +
                    "\"Times_Recorded\" INTEGER NOT NULL  DEFAULT 0, " +
                    "\"Data\" TEXT NOT NULL )";
    private static final String CREATE_SURVEY_METHOD_TABLE_QUERY =
            "CREATE TABLE \"Survey_Method\" (" +
                    "\"_id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , " +
                    "\"Method_Name\" TEXT, " +
                    "\"Times_Used\" INTEGER)";
    private static final String DROP_TABLES_QUERY =
            "DROP TABLE IF EXISTS \"Sightings\";" +
                    "DROP TABLE IF EXISTS \"Species\"" +
                    "DROP TABLE IF EXISTS \"Survey_Method\"";

    public MockSightingsAssetHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SIGHTINGS_TABLE_QUERY);
        db.execSQL(CREATE_SPECIES_TABLE_QUERY);
        db.execSQL(CREATE_SURVEY_METHOD_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLES_QUERY);
        onCreate(db);
    }
}
