package au.com.ugmedia.sightings.mocks.model.util;

import au.com.ugmedia.sightings.model.util.SightingsMetadata;

public class MockSightingsMetadata extends SightingsMetadata {
    @Override
    public String getDeviceId() {
        return "Test device ID";
    }

    @Override
    public String getDeviceName() {
        return "Test device name";
    }

    @Override
    public String getAppId() {
        return "Test app ID";
    }

    @Override
    public String getNewGUID() {
        return "Test GUID";
    }
}
