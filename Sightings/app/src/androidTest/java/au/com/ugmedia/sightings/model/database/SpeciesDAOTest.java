package au.com.ugmedia.sightings.model.database;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.ugmedia.sightings.mocks.model.database.MockSightingsAssetHelper;
import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;

public class SpeciesDAOTest extends AndroidTestCase {
    private static final String TEST_CONTEXT_PREFIX = "SpeciesDAOTest_";
    private SpeciesDAO speciesDAO;
    private JSONObject species;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), TEST_CONTEXT_PREFIX);
        speciesDAO = new SpeciesDAO(new MockSightingsAssetHelper(context));
        species = new JSONObject();

        JSONObject data = new JSONObject();
        JSONObject metadata = new JSONObject();

        String scientificName = "Test species";
        String useCount = "1";
        String group = "JUnit";
        String genus = "Testing";
        String commonName = "Testing framework";
        String geography = "Geography";
        String lastUpdated = "NOW";
        String italicise = "1";
        String builtIn = "1";

        metadata.put(SpeciesKeys.DATA_METADATA_BUILT_IN, builtIn);
        metadata.put(SpeciesKeys.DATA_METADATA_ITALICISE, italicise);
        metadata.put(SpeciesKeys.DATA_METADATA_LAST_UPDATED, lastUpdated);

        data.put(SpeciesKeys.DATA_GEOGRAPHY, geography);
        data.put(SpeciesKeys.DATA_COMMON_NAME, commonName);
        data.put(SpeciesKeys.DATA_GENUS_NAME, genus);
        data.put(SpeciesKeys.DATA_GROUP, group);
        data.put(SpeciesKeys.DATA_METADATA, metadata);

        species.put(SpeciesKeys.USE_COUNT, useCount);
        species.put(SpeciesKeys.SCIENTIFIC_NAME, scientificName);
        species.put(SpeciesKeys.DATA, data);
    }

    public void testAddSpeciesWithData() {
        try {
            assertEquals("Did not add species", 1, speciesDAO.addSpecies(species));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testAddSpeciesWithoutData() {
        try {
            assertEquals("Should not have been able to add species", -1, speciesDAO.addSpecies(null));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetAllSpeciesWithData() {
        try {
            speciesDAO.addSpecies(species);
            assertEquals("Records in DB does not match the number of inserted species", 1, speciesDAO.getAllSpecies().size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetAllSpeciesWithoutData() {
        try {
            assertEquals("Returned records invalid (no species inserted)", 0, speciesDAO.getAllSpecies().size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetMostUsedSpeciesWithMultipleSpecies() {
        try {
            JSONObject mostUsedSpecies = new JSONObject(species.toString());
            mostUsedSpecies.put(SpeciesKeys.SCIENTIFIC_NAME, "New name");
            mostUsedSpecies.put(SpeciesKeys.USE_COUNT, "2");

            speciesDAO.addSpecies(species);
            speciesDAO.addSpecies(mostUsedSpecies);

            assertEquals("Did not get most used species",
                    mostUsedSpecies.getString(SpeciesKeys.SCIENTIFIC_NAME), speciesDAO.getMostUsedSpecies());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetMostUsedSpeciesWithSingleSpecies() {
        try {
            speciesDAO.addSpecies(species);

            assertEquals("Did not get most used species",
                    species.getString(SpeciesKeys.SCIENTIFIC_NAME), speciesDAO.getMostUsedSpecies());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetMostUsedSpeciesWithNoUsedCount() {
        try {
            JSONObject noUseCountSpecies = new JSONObject(species.toString());
            noUseCountSpecies.put(SpeciesKeys.USE_COUNT, "0");

            speciesDAO.addSpecies(noUseCountSpecies);

            assertEquals("Did not return default value", "N/A", speciesDAO.getMostUsedSpecies());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetMostUsedSpeciesWithoutData() {
        assertEquals("Did not return default value", "N/A", speciesDAO.getMostUsedSpecies());
    }

    public void testGetTimesUsedWithData() {
        try {
            speciesDAO.addSpecies(species);
            long expectedTimesUsed = Long.parseLong(species.getString(SpeciesKeys.USE_COUNT));

            assertEquals("Did not get number of times used",
                    expectedTimesUsed, speciesDAO.getTimesUsed(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetTimesUsedWithInvalidName() {
        try {
            speciesDAO.addSpecies(species);

            assertEquals("Did not get the default times used", 0, speciesDAO.getTimesUsed("Invalid"));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetTimesUsedWithoutData() {
        try {
            assertEquals("Did not get the default times used",
                    0, speciesDAO.getTimesUsed(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetGroupWithData() {
        try {
            speciesDAO.addSpecies(species);
            JSONObject data = new JSONObject(species.getString(SpeciesKeys.DATA));
            String expectedGroup = data.getString(SpeciesKeys.DATA_GROUP);

            assertEquals("Did not get stored group name",
                    expectedGroup, speciesDAO.getGroup(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetGroupWithInvalidName() {
        try {
            speciesDAO.addSpecies(species);

            assertEquals("Did not get stored group name", "", speciesDAO.getGroup("Invalid"));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetGroupWithoutData() {
        try {
            assertEquals("Did not get stored group name", "", speciesDAO.getGroup("Invalid"));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testSpeciesExistsWithData() {
        try {
            speciesDAO.addSpecies(species);
            assertTrue("Species does not exist", speciesDAO.speciesExists(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testSpeciesExistsWithInvalidName() {
        try {
            speciesDAO.addSpecies(species);
            assertFalse("Species did not return default value", speciesDAO.speciesExists("Invalid"));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testSpeciesExistsWithoutData() {
        try {
            assertFalse("Species did not return default value", speciesDAO.speciesExists(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testResetUseCount() {
        try {
            speciesDAO.addSpecies(species);
            speciesDAO.resetUseCount();

            assertEquals("Did not reset use count", 0, speciesDAO.getTimesUsed(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testIncrementUseCountWithValidName() {
        try {
            speciesDAO.addSpecies(species);
            speciesDAO.incrementUseCount(species.getString(SpeciesKeys.SCIENTIFIC_NAME));

            assertEquals("Did not increment use count", 2, speciesDAO.getTimesUsed(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testIncrementUseCountWithInvalidName() {
        try {
            speciesDAO.addSpecies(species);
            speciesDAO.incrementUseCount("Invalid");

            assertEquals("Incremented use count", 1, speciesDAO.getTimesUsed(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testDecrementUseCountWithValidName() {
        try {
            speciesDAO.addSpecies(species);
            speciesDAO.decrementUseCount(species.getString(SpeciesKeys.SCIENTIFIC_NAME));

            assertEquals("Did not decrement use count", 0, speciesDAO.getTimesUsed(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testDecrementUseCountWithInvalidName() {
        try {
            speciesDAO.addSpecies(species);
            speciesDAO.decrementUseCount("Invalid");

            assertEquals("Decremented use count", 1, speciesDAO.getTimesUsed(species.getString(SpeciesKeys.SCIENTIFIC_NAME)));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }
}