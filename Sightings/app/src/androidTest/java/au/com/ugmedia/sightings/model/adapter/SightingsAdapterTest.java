package au.com.ugmedia.sightings.model.adapter;

import android.content.res.Resources;
import android.test.AndroidTestCase;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.R;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;

public class SightingsAdapterTest extends AndroidTestCase {
    private SightingsAdapter adapter;
    private JSONObject itemWithDistanceTo;
    private JSONObject itemWithoutDistanceTo;
    private JSONObject emptyItem;
    private ArrayList<JSONObject> data;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        data = new ArrayList<>();
        itemWithDistanceTo = new JSONObject();
        itemWithoutDistanceTo = new JSONObject();
        emptyItem = new JSONObject();
        JSONObject itemData = new JSONObject();

        String scientificName = "JUnit";
        String commonName = "Testing framework";
        String health = "Alive";
        String photoTaken = "Photo";
        String distanceTo = "100.0";

        itemData.put(SightingsKeys.DATA_SCIENTIFIC_NAME, scientificName);
        itemData.put(SightingsKeys.DATA_COMMON_NAME, commonName);
        itemData.put(SightingsKeys.DATA_HEALTH, health);
        itemData.put(SightingsKeys.DATA_PHOTO_TAKEN, photoTaken);

        itemWithDistanceTo.put(SightingsKeys.DISTANCE_TO, distanceTo);
        itemWithDistanceTo.put(SightingsKeys.DATA, itemData);
        data.add(itemWithDistanceTo);

        itemWithoutDistanceTo.put(SightingsKeys.DISTANCE_TO, "");
        itemWithoutDistanceTo.put(SightingsKeys.DATA, itemData);
        data.add(itemWithoutDistanceTo);

        emptyItem.put(SightingsKeys.DATA, new JSONObject());
        data.add(emptyItem);

        adapter = new SightingsAdapter(getContext(), R.layout.list_item_sighting, data);
    }

    public void testGetViewWithDistanceTo() {
        View v = adapter.getView(0, null, null);
        TextView scientificNameView = (TextView) v.findViewById(R.id.list_item_scientific_name);
        TextView commonNameView = (TextView) v.findViewById(R.id.minor_detail_common_name);
        TextView isAliveView = (TextView) v.findViewById(R.id.minor_detail_is_alive);
        TextView photoTakenView = (TextView) v.findViewById(R.id.minor_detail_photo_taken);
        TextView distanceToView = (TextView) v.findViewById(R.id.minor_detail_distance_to);

        assertNotNull("View is null", v);
        assertNotNull("Scientific name view is null", scientificNameView);
        assertNotNull("Common name view is null", commonNameView);
        assertNotNull("Is alive view is null", isAliveView);
        assertNotNull("Photo taken view is null", photoTakenView);
        assertNotNull("Distance to view is null", distanceToView);

        try {
            JSONObject data = new JSONObject(itemWithDistanceTo.getString(SightingsKeys.DATA));
            float expectedDistanceTo = Float.parseFloat(itemWithDistanceTo.getString(SightingsKeys.DISTANCE_TO));

            assertEquals("Scientific names don't match",
                    data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME), scientificNameView.getText().toString());
            assertEquals("Common names don't match",
                    data.getString(SightingsKeys.DATA_COMMON_NAME), commonNameView.getText().toString());
            assertEquals("Is alive values don't match",
                    data.getString(SightingsKeys.DATA_HEALTH), isAliveView.getText().toString());
            assertEquals("Photo taken values don't match",
                    data.getString(SightingsKeys.DATA_PHOTO_TAKEN), photoTakenView.getText().toString());
            assertEquals("Distance to values don't match",
                    String.format("%.2fm", expectedDistanceTo), distanceToView.getText().toString());
        } catch (JSONException e) {
            fail("JSONException thrown!");
            e.printStackTrace();
        }
    }

    public void testGetViewWithoutDistanceTo() {
        View v = adapter.getView(1, null, null);
        TextView scientificNameView = (TextView) v.findViewById(R.id.list_item_scientific_name);
        TextView commonNameView = (TextView) v.findViewById(R.id.minor_detail_common_name);
        TextView isAliveView = (TextView) v.findViewById(R.id.minor_detail_is_alive);
        TextView photoTakenView = (TextView) v.findViewById(R.id.minor_detail_photo_taken);
        TextView distanceToView = (TextView) v.findViewById(R.id.minor_detail_distance_to);

        assertNotNull("View is null", v);
        assertNotNull("Scientific name view is null", scientificNameView);
        assertNotNull("Common name view is null", commonNameView);
        assertNotNull("Is alive view is null", isAliveView);
        assertNotNull("Photo taken view is null", photoTakenView);
        assertNotNull("Distance to view is null", distanceToView);

        try {
            JSONObject data = new JSONObject(itemWithoutDistanceTo.getString(SightingsKeys.DATA));
            Resources res = getContext().getResources();

            assertEquals("Scientific names don't match",
                    data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME), scientificNameView.getText().toString());
            assertEquals("Common names don't match",
                    data.getString(SightingsKeys.DATA_COMMON_NAME), commonNameView.getText().toString());
            assertEquals("Is alive values don't match",
                    data.getString(SightingsKeys.DATA_HEALTH), isAliveView.getText().toString());
            assertEquals("Photo taken values don't match",
                    data.getString(SightingsKeys.DATA_PHOTO_TAKEN), photoTakenView.getText().toString());
            assertEquals("Distance to values don't match", res.getString(R.string.default_distance_to), distanceToView.getText().toString());
        } catch (JSONException e) {
            fail("JSONException thrown!");
            e.printStackTrace();
        }
    }

    public void testGetViewWithNoData() {
        View v = adapter.getView(2, null, null);
        TextView scientificNameView = (TextView) v.findViewById(R.id.list_item_scientific_name);
        TextView commonNameView = (TextView) v.findViewById(R.id.minor_detail_common_name);
        TextView isAliveView = (TextView) v.findViewById(R.id.minor_detail_is_alive);
        TextView photoTakenView = (TextView) v.findViewById(R.id.minor_detail_photo_taken);
        TextView distanceToView = (TextView) v.findViewById(R.id.minor_detail_distance_to);
        Resources res = getContext().getResources();

        assertNotNull("View is null", v);
        assertNotNull("Scientific name view is null", scientificNameView);
        assertNotNull("Common name view is null", commonNameView);
        assertNotNull("Is alive view is null", isAliveView);
        assertNotNull("Photo taken view is null", photoTakenView);
        assertNotNull("Distance to view is null", distanceToView);

        assertEquals("Scientific names don't match", res.getString(R.string.default_scientific_name), scientificNameView.getText().toString());
        assertEquals("Common names don't match", res.getString(R.string.default_common_name), commonNameView.getText().toString());
        assertEquals("Is alive values don't match", res.getString(R.string.default_health), isAliveView.getText().toString());
        assertEquals("Photo taken values don't match", res.getString(R.string.default_photo_taken), photoTakenView.getText().toString());
        assertEquals("Distance to values don't match", res.getString(R.string.default_distance_to), distanceToView.getText().toString());
    }
}