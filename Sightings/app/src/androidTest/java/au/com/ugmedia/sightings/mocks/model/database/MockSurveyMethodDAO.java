package au.com.ugmedia.sightings.mocks.model.database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.ugmedia.sightings.model.database.SurveyMethodDAO;
import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

public class MockSurveyMethodDAO extends SurveyMethodDAO {
    private SQLiteOpenHelper helper;

    public MockSurveyMethodDAO(SQLiteOpenHelper helper) {
        super(helper);
        this.helper = helper;
    }

    public long addSurveyMethod(JSONObject surveyMethod) throws JSONException {
        long insertedRowId = -1;

        if (surveyMethod != null) {
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(METHOD_NAME_COLUMN, surveyMethod.get(SurveyMethodKeys.METHOD_NAME).toString());
            values.put(TIMES_USED_COLUMN, surveyMethod.getInt(SurveyMethodKeys.TIMES_USED));

            insertedRowId = db.insert(TABLE_NAME, null, values);

        }

        return insertedRowId;
    }
}
