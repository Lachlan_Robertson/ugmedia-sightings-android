package au.com.ugmedia.sightings.model;

import android.location.Location;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import au.com.ugmedia.sightings.mocks.model.util.MockSightingsMetadata;
import au.com.ugmedia.sightings.mocks.model.util.MockUTMLocation;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;
import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;
import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

public class AppDataSingletonTest extends AndroidTestCase {
    private static final String TEST_CONTEXT_PREFIX = "AppDataSingletonTest_";

    private RenamingDelegatingContext context;
    private AppDataSingleton appDataSingleton;
    private JSONObject sighting;
    private JSONObject species;
    private JSONObject surveyMethod;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        context = new RenamingDelegatingContext(getContext(), TEST_CONTEXT_PREFIX);
        appDataSingleton = AppDataSingleton.getInstance(context);
        appDataSingleton.getSightings().clear();
        appDataSingleton.getSpecies().clear();
        appDataSingleton.getSurveyMethods().clear();

        initSighting();
        initSpecies();
        initSurveyMethod();
    }

    // -- Sightings-related tests
    public void testAddSightingWithValidData() {
        try {
            assertTrue("Sighting not added", appDataSingleton.addSighting(sighting) != -1);
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testAddSightingWithInvalidData() {
        try {
            assertFalse("Sighting added", appDataSingleton.addSighting(null) != -1);
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testDuplicateSightingWithValidData() {
        try {
            long id = appDataSingleton.addSighting(sighting);
            assertTrue("Didn't duplicate sighting",
                    appDataSingleton.duplicateSighting(id, new MockUTMLocation(), new MockSightingsMetadata()));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testDuplicateSightingWithInvalidId() {
        try {
            appDataSingleton.addSighting(sighting);
            assertFalse("Did duplicate sighting",
                    appDataSingleton.duplicateSighting(0, new MockUTMLocation(), new MockSightingsMetadata()));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testUpdateSightingWithValidData() {
        try {
            long id = appDataSingleton.addSighting(sighting);

            JSONObject newSighting = new JSONObject(sighting.toString());
            newSighting.put(SightingsKeys.TIMESTAMP, "NOW");
            assertTrue("Didn't update sighting", appDataSingleton.updateSighting(id, newSighting));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testUpdateSightingWithInvalidId() {
        try {
            appDataSingleton.addSighting(sighting);

            JSONObject newSighting = new JSONObject(sighting.toString());
            newSighting.put(SightingsKeys.TIMESTAMP, "NOW");
            assertFalse("Did update sighting", appDataSingleton.updateSighting(0, newSighting));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testUpdateSightingWithInvalidSighting() {
        try {
            long id = appDataSingleton.addSighting(sighting);
            assertFalse("Did update sighting", appDataSingleton.updateSighting(id, null));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testRemoveSightingWithValidData() {
        try {
            long id = appDataSingleton.addSighting(sighting);
            assertTrue("Didn't remove sighting", appDataSingleton.removeSighting(id));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testRemoveSightingWithInvalidId() {
        try {
            appDataSingleton.addSighting(sighting);
            assertFalse("Did remove sighting", appDataSingleton.removeSighting(0));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testRemoveAllRecords() {
        try {
            appDataSingleton.addSighting(sighting);
            appDataSingleton.removeAllRecords();
            assertEquals("Didn't remove all records", 0, appDataSingleton.getSightings().size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testSortSightingsByDateNewestWithLocation() {
        Location l = new Location("Test-Provider");
        l.setLatitude(10.0);
        l.setLongitude(10.0);
        l.setAltitude(10.0);
        l.setAccuracy(10.0f);

        try {
            JSONObject newerSighting = new JSONObject(sighting.toString());
            DateFormat df = SimpleDateFormat.getDateTimeInstance();
            String timestamp = df.format(new Date().getTime() + 1000); // +1000ms forces it to be the newest
            newerSighting.put(SightingsKeys.TIMESTAMP, timestamp);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(newerSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DATE_NEWEST, l);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();
            JSONObject newestSighting = sightings.get(0);
            JSONObject oldestSighting = sightings.get(1);

            long newestDate = df.parse(newestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            long oldestDate = df.parse(oldestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            assertTrue("Sightings are not in the correct order", oldestDate < newestDate);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testSortSightingsByDateNewestWithoutLocation() {
        try {
            JSONObject newerSighting = new JSONObject(sighting.toString());
            DateFormat df = SimpleDateFormat.getDateTimeInstance();
            String timestamp = df.format(new Date().getTime() + 1000); // +1000ms forces it to be the newest
            newerSighting.put(SightingsKeys.TIMESTAMP, timestamp);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(newerSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DATE_NEWEST, null);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();
            JSONObject newestSighting = sightings.get(0);
            JSONObject oldestSighting = sightings.get(1);

            long newestDate = df.parse(newestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            long oldestDate = df.parse(oldestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            assertTrue("Sightings are not in the correct order", oldestDate < newestDate);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testSortSightingsByDateOldestWithLocation() {
        Location l = new Location("Test-Provider");
        l.setLatitude(10.0);
        l.setLongitude(10.0);
        l.setAltitude(10.0);
        l.setAccuracy(10.0f);

        try {
            JSONObject newerSighting = new JSONObject(sighting.toString());
            DateFormat df = SimpleDateFormat.getDateTimeInstance();
            String timestamp = df.format(new Date().getTime() + 1000); // +1000ms forces it to be the newest
            newerSighting.put(SightingsKeys.TIMESTAMP, timestamp);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(newerSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DATE_OLDEST, l);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();
            JSONObject oldestSighting = sightings.get(0);
            JSONObject newestSighting = sightings.get(1);

            long newestDate = df.parse(newestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            long oldestDate = df.parse(oldestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            assertTrue("Sightings are not in the correct order", oldestDate < newestDate);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testSortSightingsByDateOldestWithoutLocation() {
        try {
            JSONObject newerSighting = new JSONObject(sighting.toString());
            DateFormat df = SimpleDateFormat.getDateTimeInstance();
            String timestamp = df.format(new Date().getTime() + 1000); // +1000ms forces it to be the newest
            newerSighting.put(SightingsKeys.TIMESTAMP, timestamp);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(newerSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DATE_OLDEST, null);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();
            JSONObject oldestSighting = sightings.get(0);
            JSONObject newestSighting = sightings.get(1);

            long newestDate = df.parse(newestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            long oldestDate = df.parse(oldestSighting.getString(SightingsKeys.TIMESTAMP)).getTime();
            assertTrue("Sightings are not in the correct order", oldestDate < newestDate);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testSortSightingsByDistanceFurthestWithLocation() {
        Location l = new Location("Test-Provider");
        l.setLatitude(10.0);
        l.setLongitude(10.0);
        l.setAltitude(10.0);
        l.setAccuracy(10.0f);

        try {
            JSONObject furtherSighting = new JSONObject(sighting.toString());
            JSONObject data = new JSONObject(furtherSighting.getString(SightingsKeys.DATA));
            JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));
            location.put(SightingsKeys.DATA_LOCATION_LATITUDE, "50");
            location.put(SightingsKeys.DATA_LOCATION_LONGITUDE, "50");

            data.put(SightingsKeys.DATA_LOCATION, location);
            furtherSighting.put(SightingsKeys.DATA, data);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(furtherSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DISTANCE_FURTHEST, l);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();

            JSONObject furthestSighting = sightings.get(0);
            JSONObject closestSighting = sightings.get(1);

            assertTrue("Sightings not in correct order",
                    closestSighting.getDouble(SightingsKeys.DISTANCE_TO) < furthestSighting.getDouble(SightingsKeys.DISTANCE_TO));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testSortSightingsByDistanceFurthestWithout() {
        try {
            JSONObject furtherSighting = new JSONObject(sighting.toString());
            JSONObject data = new JSONObject(furtherSighting.getString(SightingsKeys.DATA));
            JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));
            location.put(SightingsKeys.DATA_LOCATION_LATITUDE, "50");
            location.put(SightingsKeys.DATA_LOCATION_LONGITUDE, "50");

            data.put(SightingsKeys.DATA_LOCATION, location);
            furtherSighting.put(SightingsKeys.DATA, data);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(furtherSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DISTANCE_FURTHEST, null);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();

            JSONObject furthestSighting = sightings.get(0);
            JSONObject closestSighting = sightings.get(1);

            assertEquals("Sighting has \"distance to\"", "", furthestSighting.optString(SightingsKeys.DISTANCE_TO, ""));
            assertEquals("Sighting has \"distance to\"", "", closestSighting.optString(SightingsKeys.DISTANCE_TO, ""));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testSortSightingsByDistanceClosestWithLocation() {
        Location l = new Location("Test-Provider");
        l.setLatitude(10.0);
        l.setLongitude(10.0);
        l.setAltitude(10.0);
        l.setAccuracy(10.0f);

        try {
            JSONObject furtherSighting = new JSONObject(sighting.toString());
            JSONObject data = new JSONObject(furtherSighting.getString(SightingsKeys.DATA));
            JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));
            location.put(SightingsKeys.DATA_LOCATION_LATITUDE, "50");
            location.put(SightingsKeys.DATA_LOCATION_LONGITUDE, "50");

            data.put(SightingsKeys.DATA_LOCATION, location);
            furtherSighting.put(SightingsKeys.DATA, data);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(furtherSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DISTANCE_CLOSEST, l);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();

            JSONObject closestSighting = sightings.get(0);
            JSONObject furthestSighting = sightings.get(1);

            assertTrue("Sightings not in correct order",
                    closestSighting.getDouble(SightingsKeys.DISTANCE_TO) < furthestSighting.getDouble(SightingsKeys.DISTANCE_TO));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testSortSightingsByDistanceClosestWithout() {
        try {
            JSONObject furtherSighting = new JSONObject(sighting.toString());
            JSONObject data = new JSONObject(furtherSighting.getString(SightingsKeys.DATA));
            JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));
            location.put(SightingsKeys.DATA_LOCATION_LATITUDE, "50");
            location.put(SightingsKeys.DATA_LOCATION_LONGITUDE, "50");

            data.put(SightingsKeys.DATA_LOCATION, location);
            furtherSighting.put(SightingsKeys.DATA, data);

            appDataSingleton.addSighting(sighting);
            appDataSingleton.addSighting(furtherSighting);
            appDataSingleton.sortSightings(AppDataSingleton.SightingsSortMethod.DISTANCE_FURTHEST, null);

            ArrayList<JSONObject> sightings = appDataSingleton.getSightings();

            JSONObject closestSighting = sightings.get(0);
            JSONObject furthestSighting = sightings.get(1);

            assertEquals("Sighting has \"distance to\"", "", closestSighting.optString(SightingsKeys.DISTANCE_TO, ""));
            assertEquals("Sighting has \"distance to\"", "", furthestSighting.optString(SightingsKeys.DISTANCE_TO, ""));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    // -- Species-related tests
    public void testAddSpeciesWithValidData() {
        try {
            appDataSingleton.addSpecies(species);
            assertEquals("Species not added", 1, appDataSingleton.getSpecies().size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testAddSpeciesWithInvalidData() {
        try {
            appDataSingleton.addSpecies(null);
            assertEquals("Species added", 0, appDataSingleton.getSpecies().size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testIncrementSpeciesCountWithValidName() {
        try {
            appDataSingleton.addSpecies(species);
            appDataSingleton.incrementSpeciesCount(species.getString(SpeciesKeys.SCIENTIFIC_NAME));

            JSONObject updatedSpecies = appDataSingleton.getSpecies().get(0);
            assertEquals("Didn't increment use count", 2, updatedSpecies.getInt(SpeciesKeys.USE_COUNT));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testIncrementSpeciesCountWithInvalidName() {
        try {
            appDataSingleton.addSpecies(species);
            appDataSingleton.incrementSpeciesCount("Invalid");

            JSONObject updatedSpecies = appDataSingleton.getSpecies().get(0);
            assertEquals("Incremented use count", 1, updatedSpecies.getInt(SpeciesKeys.USE_COUNT));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testDecrementSpeciesCountWithValidName() {
        try {
            appDataSingleton.addSpecies(species);
            appDataSingleton.decrementSpeciesCount(species.getString(SpeciesKeys.SCIENTIFIC_NAME));

            JSONObject updatedSpecies = appDataSingleton.getSpecies().get(0);
            assertEquals("Didn't decrement use count", 0, updatedSpecies.getInt(SpeciesKeys.USE_COUNT));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testDecrementSpeciesCountWithInvalidName() {
        try {
            appDataSingleton.addSpecies(species);
            appDataSingleton.decrementSpeciesCount(null);

            JSONObject updatedSpecies = appDataSingleton.getSpecies().get(0);
            assertEquals("Did decrement use count", 1, updatedSpecies.getInt(SpeciesKeys.USE_COUNT));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    // -- SurveyMethod-related tests
    public void testIncrementSurveyMethodCountWithValidName() {
        try {
            appDataSingleton.getSurveyMethods().add(surveyMethod);
            appDataSingleton.incrementSurveyMethodCount(surveyMethod.getString(SurveyMethodKeys.METHOD_NAME));

            JSONObject updatedSurveyMethod = appDataSingleton.getSurveyMethods().get(0);
            assertEquals("Didn't increment survey method", 2, updatedSurveyMethod.getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testIncrementSurveyMethodCountWithInvalidName() {
        try {
            appDataSingleton.getSurveyMethods().add(surveyMethod);
            appDataSingleton.incrementSurveyMethodCount("Invalid");

            JSONObject updatedSurveyMethod = appDataSingleton.getSurveyMethods().get(0);
            assertEquals("Incremented survey method", 1, updatedSurveyMethod.getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testDecrementSurveyMethodCountWithValidName() {
        try {
            appDataSingleton.getSurveyMethods().add(surveyMethod);
            appDataSingleton.decrementSurveyMethodCount(surveyMethod.getString(SurveyMethodKeys.METHOD_NAME));

            JSONObject updatedSurveyMethod = appDataSingleton.getSurveyMethods().get(0);
            assertEquals("Didn't decrement survey method", 0, updatedSurveyMethod.getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    public void testDecrementSurveyMethodCountWithInvalidName() {
        try {
            appDataSingleton.getSurveyMethods().add(surveyMethod);
            appDataSingleton.decrementSurveyMethodCount("Invalid");

            JSONObject updatedSurveyMethod = appDataSingleton.getSurveyMethods().get(0);
            assertEquals("Decremented survey method", 1, updatedSurveyMethod.getInt(SurveyMethodKeys.TIMES_USED));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }

    // -- Non-test, helper methods for this class
    private void initSighting() throws Exception {
        sighting = new JSONObject();

        JSONObject data = new JSONObject();
        JSONObject location = new JSONObject();
        JSONObject metadata = new JSONObject();

        String timestamp = SimpleDateFormat.getDateTimeInstance().format(new Date());
        String scientificName = "JUnit";
        String commonName = "Testing framework";
        String quantity = "1";
        String photoTaken = "Photo";
        String health = "Alive";
        String notes = "Notes";
        String siteName = "Site name";
        String surveyMethod = "Survey method";
        String latitude = "0";
        String longitude = "0";
        String altitude = "0";
        String distance = "0";
        String accuracy = "0";
        String easting = "0";
        String northing = "0";
        String hemisphere = "North";
        String zone = "0";
        String deviceId = "Device ID";
        String deviceName = "Device name";
        String guid = "GUID";
        String appId = "App ID";

        metadata.put(SightingsKeys.DATA_METADATA_APPLICATION_ID, appId);
        metadata.put(SightingsKeys.DATA_METADATA_GUID, guid);
        metadata.put(SightingsKeys.DATA_METADATA_DEVICE_NAME, deviceName);
        metadata.put(SightingsKeys.DATA_METADATA_DEVICE_ID, deviceId);

        location.put(SightingsKeys.DATA_LOCATION_ZONE, zone);
        location.put(SightingsKeys.DATA_LOCATION_HEMISPHERE, hemisphere);
        location.put(SightingsKeys.DATA_LOCATION_NORTHING, northing);
        location.put(SightingsKeys.DATA_LOCATION_EASTING, easting);
        location.put(SightingsKeys.DATA_LOCATION_ACCURACY, accuracy);
        location.put(SightingsKeys.DATA_LOCATION_DISTANCE, distance);
        location.put(SightingsKeys.DATA_LOCATION_ALTITUDE, altitude);
        location.put(SightingsKeys.DATA_LOCATION_LONGITUDE, longitude);
        location.put(SightingsKeys.DATA_LOCATION_LATITUDE, latitude);

        data.put(SightingsKeys.DATA_SCIENTIFIC_NAME, scientificName);
        data.put(SightingsKeys.DATA_COMMON_NAME, commonName);
        data.put(SightingsKeys.DATA_QUANTITY, quantity);
        data.put(SightingsKeys.DATA_PHOTO_TAKEN, photoTaken);
        data.put(SightingsKeys.DATA_HEALTH, health);
        data.put(SightingsKeys.DATA_NOTES, notes);
        data.put(SightingsKeys.DATA_SITE_NAME, siteName);
        data.put(SightingsKeys.DATA_SURVEY_METHOD, surveyMethod);
        data.put(SightingsKeys.DATA_METADATA, metadata);
        data.put(SightingsKeys.DATA_LOCATION, location);

        sighting.put(SightingsKeys.TIMESTAMP, timestamp);
        sighting.put(SightingsKeys.DATA, data);
    }

    private void initSpecies() throws JSONException {
        species = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject metadata = new JSONObject();
        JSONObject sightingData = new JSONObject(sighting.getString(SightingsKeys.DATA));

        metadata.put(SpeciesKeys.DATA_METADATA_BUILT_IN, "false");
        metadata.put(SpeciesKeys.DATA_METADATA_ITALICISE, "false");
        metadata.put(SpeciesKeys.DATA_METADATA_LAST_UPDATED, "NOW");

        data.put(SpeciesKeys.DATA_COMMON_NAME, sightingData.getString(SightingsKeys.DATA_COMMON_NAME));
        data.put(SpeciesKeys.DATA_GENUS_NAME, "Genus");
        data.put(SpeciesKeys.DATA_GEOGRAPHY, "Geography");
        data.put(SpeciesKeys.DATA_GROUP, "Group");
        data.put(SpeciesKeys.DATA_METADATA, metadata);

        species.put(SpeciesKeys.SCIENTIFIC_NAME, sightingData.getString(SightingsKeys.DATA_SCIENTIFIC_NAME));
        species.put(SpeciesKeys.USE_COUNT, "1");
        species.put(SpeciesKeys.DATA, data);
    }

    private void initSurveyMethod() throws JSONException {
        surveyMethod = new JSONObject();
        surveyMethod.put(SurveyMethodKeys.METHOD_NAME, "Method name");
        surveyMethod.put(SurveyMethodKeys.TIMES_USED, "1");
    }
}