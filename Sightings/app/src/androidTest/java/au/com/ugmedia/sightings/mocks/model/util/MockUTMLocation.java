package au.com.ugmedia.sightings.mocks.model.util;

import android.location.Location;

import au.com.ugmedia.sightings.model.util.UTMLocation;

public class MockUTMLocation extends UTMLocation {
    private double latitude;
    private double longitude;
    private double altitude;
    private double accuracy;

    public MockUTMLocation() {
        latitude = INVALID_VALUE_FLAG;
        longitude = INVALID_VALUE_FLAG;
        altitude = INVALID_VALUE_FLAG;
        accuracy = INVALID_VALUE_FLAG;
    }

    public MockUTMLocation(Location l) {
        if (l == null) {
            latitude = INVALID_VALUE_FLAG;
            longitude = INVALID_VALUE_FLAG;
            altitude = INVALID_VALUE_FLAG;
            accuracy = INVALID_VALUE_FLAG;
        } else {
            latitude = l.getLatitude();
            longitude = l.getLongitude();
            altitude = l.getAltitude();
            accuracy = l.getAccuracy();
        }
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getEasting() {
        return 0.0;
    }

    public double getNorthing() {
        return 0.0;
    }

    public String getHemisphere() {
        return "North";
    }

    public double getZone() {
        return 0.0;
    }

    public String getZoneAsString() {
        return getZone() + "";
    }
}
