package au.com.ugmedia.sightings.model.database;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import au.com.ugmedia.sightings.mocks.model.database.MockSightingsAssetHelper;
import au.com.ugmedia.sightings.mocks.model.util.MockSightingsMetadata;
import au.com.ugmedia.sightings.mocks.model.util.MockUTMLocation;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;

public class SightingsDAOTest extends AndroidTestCase {
    private static final String TEST_CONTEXT_PREFIX = "SightingsDAOTest_";
    private SightingsDAO sightingsDAO;
    private JSONObject sighting;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), TEST_CONTEXT_PREFIX);
        sightingsDAO = new SightingsDAO(new MockSightingsAssetHelper(context));
        sighting = new JSONObject();

        JSONObject data = new JSONObject();
        JSONObject location = new JSONObject();
        JSONObject metadata = new JSONObject();

        Date timestamp = new Date();
        String scientificName = "JUnit";
        String commonName = "Testing framework";
        String quantity = "1";
        String photoTaken = "Photo";
        String health = "Alive";
        String notes = "Notes";
        String siteName = "Site name";
        String surveyMethod = "Survey method";
        String latitude = "0";
        String longitude = "0";
        String altitude = "0";
        String distance = "0";
        String accuracy = "0";
        String easting = "0";
        String northing = "0";
        String hemisphere = "North";
        String zone = "0";
        String deviceId = "Device ID";
        String deviceName = "Device name";
        String guid = "GUID";
        String appId = "App ID";

        metadata.put(SightingsKeys.DATA_METADATA_APPLICATION_ID, appId);
        metadata.put(SightingsKeys.DATA_METADATA_GUID, guid);
        metadata.put(SightingsKeys.DATA_METADATA_DEVICE_NAME, deviceName);
        metadata.put(SightingsKeys.DATA_METADATA_DEVICE_ID, deviceId);

        location.put(SightingsKeys.DATA_LOCATION_ZONE, zone);
        location.put(SightingsKeys.DATA_LOCATION_HEMISPHERE, hemisphere);
        location.put(SightingsKeys.DATA_LOCATION_NORTHING, northing);
        location.put(SightingsKeys.DATA_LOCATION_EASTING, easting);
        location.put(SightingsKeys.DATA_LOCATION_ACCURACY, accuracy);
        location.put(SightingsKeys.DATA_LOCATION_DISTANCE, distance);
        location.put(SightingsKeys.DATA_LOCATION_ALTITUDE, altitude);
        location.put(SightingsKeys.DATA_LOCATION_LONGITUDE, longitude);
        location.put(SightingsKeys.DATA_LOCATION_LATITUDE, latitude);

        data.put(SightingsKeys.DATA_SCIENTIFIC_NAME, scientificName);
        data.put(SightingsKeys.DATA_COMMON_NAME, commonName);
        data.put(SightingsKeys.DATA_QUANTITY, quantity);
        data.put(SightingsKeys.DATA_PHOTO_TAKEN, photoTaken);
        data.put(SightingsKeys.DATA_HEALTH, health);
        data.put(SightingsKeys.DATA_NOTES, notes);
        data.put(SightingsKeys.DATA_SITE_NAME, siteName);
        data.put(SightingsKeys.DATA_SURVEY_METHOD, surveyMethod);
        data.put(SightingsKeys.DATA_METADATA, metadata);
        data.put(SightingsKeys.DATA_LOCATION, metadata);

        sighting.put(SightingsKeys.TIMESTAMP, timestamp);
        sighting.put(SightingsKeys.DATA, data);
    }

    public void testAddNewSightingWithData() {
        try {
            assertEquals("Sighting was not added", 1, sightingsDAO.addNewSighting(sighting));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testAddSightingWithoutData() {
        try {
            assertEquals("Sighting should not have been added", -1, sightingsDAO.addNewSighting(null));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testDuplicateSightingWithData() {
        try {
            long id = sightingsDAO.addNewSighting(sighting);
            MockUTMLocation utmLocation = new MockUTMLocation();
            MockSightingsMetadata metadata = new MockSightingsMetadata();
            assertEquals("Sighting was not duplicated", 2, sightingsDAO.duplicateSighting(id, utmLocation, metadata));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testDuplicateSightingWithoutData() {
        try {
            MockUTMLocation utmLocation = new MockUTMLocation();
            MockSightingsMetadata metadata = new MockSightingsMetadata();
            assertEquals("Sighting was not duplicated", -1, sightingsDAO.duplicateSighting(0, utmLocation, metadata));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetSightingWithData() {
        try {
            long id = sightingsDAO.addNewSighting(sighting);
            JSONObject storedSighting = sightingsDAO.getSighting(id);
            assertNotNull("Sighting is null", storedSighting);

            String timestamp = storedSighting.getString(SightingsKeys.TIMESTAMP);
            String data = storedSighting.getString(SightingsKeys.DATA);

            assertEquals("Timestamps don't match", sighting.getString(SightingsKeys.TIMESTAMP), timestamp);
            assertEquals("Data fields don't match", sighting.getString(SightingsKeys.DATA), data);
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetSightingWithoutData() {
        try {
            JSONObject storedSighting = sightingsDAO.getSighting(0);
            assertNull("Sighting isn't null", storedSighting);
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetAllSightingsWithData() {
        try {
            sightingsDAO.addNewSighting(sighting);
            ArrayList<JSONObject> sightings = sightingsDAO.getAllSightings();

            assertNotNull("Sightings is null", sightings);
            assertEquals("Sightings is empty", 1, sightings.size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetAllSightingsWithoutData() {
        try {
            ArrayList<JSONObject> sightings = sightingsDAO.getAllSightings();
            assertNotNull("Sightings is null", sightings);
            assertEquals("Sightings isn't empty", 0, sightings.size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetNumberOfRecordsWithData() {
        try {
            sightingsDAO.addNewSighting(sighting);
            assertEquals("Number of records not same as in DB", 1, sightingsDAO.getNumberOfRecords());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetNumberOfRecordsWithoutData() {
        assertEquals("Number of records not 0", 0, sightingsDAO.getNumberOfRecords());
    }

    public void testGetUniqueNumberOfRecordsWithDuplicates() {
        try {
            sightingsDAO.addNewSighting(sighting);
            sightingsDAO.addNewSighting(sighting);

            assertEquals("Number of unique records greater than actual unique records", 1, sightingsDAO.getUniqueNumberOfRecords());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetUniqueNumberOfRecordsWithNoDuplicates() {
        try {
            JSONObject newSighting = new JSONObject();
            JSONObject data = new JSONObject(sighting.getString(SightingsKeys.DATA));

            data.put(SightingsKeys.DATA_SCIENTIFIC_NAME, "Unique scientific name");
            newSighting.put(SightingsKeys.TIMESTAMP, sighting.getString(SightingsKeys.TIMESTAMP));
            newSighting.put(SightingsKeys.DATA, data);

            sightingsDAO.addNewSighting(sighting);
            sightingsDAO.addNewSighting(newSighting);

            assertEquals("Number of unique records not equal to number of inserted unique records",
                    2, sightingsDAO.getUniqueNumberOfRecords());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testGetUniqueNumberOfRecordsWithNoData() {
        try {
            assertEquals("Didn't return 0 (no records in DB)", 0, sightingsDAO.getUniqueNumberOfRecords());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testUpdateSightingWithData() {
        try {
            long id = sightingsDAO.addNewSighting(sighting);
            JSONObject updatedSighting = new JSONObject(sighting.toString());
            updatedSighting.put(SightingsKeys.TIMESTAMP, new Date());

            assertEquals("Did not update 1 sighting", 1, sightingsDAO.updateSighting(id, updatedSighting));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testUpdateSightingWithoutData() {
        try {
            assertEquals("Should not have updated any records", 0, sightingsDAO.updateSighting(0, sighting));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testUpdateSightingsWithNull() {
        try {
            long id = sightingsDAO.addNewSighting(sighting);
            assertEquals("Should not be able to update with null", 0, sightingsDAO.updateSighting(id, null));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testRemoveAllSightingsWithData() {
        try {
            sightingsDAO.addNewSighting(sighting);
            assertEquals("Did not remove Sighting", 1, sightingsDAO.removeAllSightings());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testRemoveAllSightingsWithoutData() {
        assertEquals("Should not have had any records to remove", 0, sightingsDAO.removeAllSightings());
    }

    public void testRemoveSightingWithData() {
        try {
            long id = sightingsDAO.addNewSighting(sighting);
            assertEquals("Did not remove Sighting", 1, sightingsDAO.removeSighting(id));
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown!");
        }
    }

    public void testRemoveSightingWithoutData() {
        assertEquals("Should not have had any records to remove", 0, sightingsDAO.removeSighting(0));
    }
}