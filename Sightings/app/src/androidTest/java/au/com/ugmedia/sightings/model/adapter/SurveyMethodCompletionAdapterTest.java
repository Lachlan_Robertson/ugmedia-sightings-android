package au.com.ugmedia.sightings.model.adapter;

import android.test.AndroidTestCase;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.R;
import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

public class SurveyMethodCompletionAdapterTest extends AndroidTestCase {
    private SurveyMethodCompletionAdapter adapter;
    private JSONObject item;
    private ArrayList<JSONObject> data;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        data = new ArrayList<>();
        item = new JSONObject();

        String methodName = "Unit testing";
        String timesUsed = "1";

        item.put(SurveyMethodKeys.METHOD_NAME, methodName);
        item.put(SurveyMethodKeys.TIMES_USED, timesUsed);

        data.add(item);

        adapter = new SurveyMethodCompletionAdapter(getContext(), R.layout.list_item_survey_method_dropdown, data);
    }

    public void testGetView() {
        View v = adapter.getView(0, null, null);
        TextView surveyMethodNameView = (TextView) v.findViewById(R.id.list_item_survey_method_survey_name);
        TextView timesUsedView = (TextView) v.findViewById(R.id.list_item_survey_method_times_used);

        assertNotNull("View is null", v);
        assertNotNull("Survey method name view is null", surveyMethodNameView);
        assertNotNull("Times used view is null", timesUsedView);

        try {
            String expectedTimesUsed = getContext().getString(R.string.list_item_survey_method_times_used, item.get(SurveyMethodKeys.TIMES_USED));
            assertEquals("Survey method names don't match", item.getString(SurveyMethodKeys.METHOD_NAME), surveyMethodNameView.getText().toString());
            assertEquals("Times used don't match", expectedTimesUsed, timesUsedView.getText().toString());
        } catch (JSONException e) {
            fail("JSONException thrown!");
            e.printStackTrace();
        }
    }

    public void testGetCount() {
        assertEquals("Size and count do not match", data.size(), adapter.getCount());
    }

    public void testGetItem() {
        assertEquals("Stored item and adapter item do not match", item, adapter.getItem(0));
    }
}
