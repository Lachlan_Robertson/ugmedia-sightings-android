package au.com.ugmedia.sightings.model.util;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import au.com.ugmedia.sightings.mocks.model.database.MockSightingsAssetHelper;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;

public class FileHandlerTest extends AndroidTestCase {
    private static final String TEST_CONTEXT_PREFIX = "SightingsDAOTest_";
    private FileHandler fileHandler;
    private ArrayList<JSONObject> sightings;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(), TEST_CONTEXT_PREFIX);
        fileHandler = new FileHandler(1, new MockSightingsAssetHelper(context));
        sightings = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            JSONObject sighting = new JSONObject();
            JSONObject data = new JSONObject();
            JSONObject location = new JSONObject();
            JSONObject metadata = new JSONObject();

            metadata.put(SightingsKeys.DATA_METADATA_DEVICE_NAME, "Device Name " + i);
            metadata.put(SightingsKeys.DATA_METADATA_APPLICATION_ID, "App ID " + i);
            metadata.put(SightingsKeys.DATA_METADATA_DEVICE_ID, "Device ID " + i);
            metadata.put(SightingsKeys.DATA_METADATA_GUID, "GUID " + i);

            location.put(SightingsKeys.DATA_LOCATION_ACCURACY, "Acc" + i);
            location.put(SightingsKeys.DATA_LOCATION_ALTITUDE, "Alt" + i);
            location.put(SightingsKeys.DATA_LOCATION_DISTANCE, "Distance" + i);
            location.put(SightingsKeys.DATA_LOCATION_EASTING, "Easting" + i);
            location.put(SightingsKeys.DATA_LOCATION_HEMISPHERE, "Hemisphere" + i);
            location.put(SightingsKeys.DATA_LOCATION_LATITUDE, "Lat" + i);
            location.put(SightingsKeys.DATA_LOCATION_LONGITUDE, "Lng" + i);
            location.put(SightingsKeys.DATA_LOCATION_NORTHING, "North" + i);
            location.put(SightingsKeys.DATA_LOCATION_ZONE, "Zone" + i);

            data.put(SightingsKeys.DATA_SCIENTIFIC_NAME, "Sci" + i);
            data.put(SightingsKeys.DATA_COMMON_NAME, "Common" + i);
            data.put(SightingsKeys.DATA_QUANTITY, "Quantity" + i);
            data.put(SightingsKeys.DATA_HEALTH, "Health" + i);
            data.put(SightingsKeys.DATA_PHOTO_TAKEN, "Photo" + i);
            data.put(SightingsKeys.DATA_NOTES, "Notes" + i);
            data.put(SightingsKeys.DATA_SITE_NAME, "Site" + i);
            data.put(SightingsKeys.DATA_SURVEY_METHOD, "Survey" + i);
            data.put(SightingsKeys.DATA_LOCATION, location);
            data.put(SightingsKeys.DATA_METADATA, metadata);


            sighting.put(SightingsKeys.TIMESTAMP, "Time" + i);
            sighting.put(SightingsKeys.DATA, data);

            sightings.add(sighting);
        }
    }

    public void testCreateFilesOnlyCsv() {
        ArrayList<String> filenames = fileHandler.createFiles(sightings, false, false);
        assertEquals("Incorrect number of filenames", 1, filenames.size());

        for (int i = 0; i < filenames.size(); i++) {
            File file = new File(filenames.get(i));
            String fileExtension = file.getName().substring(file.getName().lastIndexOf("."));
            assertTrue("File doesn't exist", file.exists());
            assertEquals("File is not CSV", ".csv", fileExtension);

            if (file.exists()) {
                file.delete();
            }
        }
    }

    public void testCreateFilesCsvAndGpx() {
        ArrayList<String> filenames = fileHandler.createFiles(sightings, true, false);
        assertEquals("Incorrect number of filenames", 2, filenames.size());

        for (int i = 0; i < filenames.size(); i++) {
            File file = new File(filenames.get(i));
            String fileExtension = file.getName().substring(file.getName().lastIndexOf("."));
            assertTrue("File doesn't exist", file.exists());
            assertTrue("Files are not CSV or GPX", fileExtension.equals(".csv") || fileExtension.equals(".gpx"));

            if (file.exists()) {
                file.delete();
            }
        }
    }

    public void testCreateFilesCsvAndKml() {
        ArrayList<String> filenames = fileHandler.createFiles(sightings, false, true);
        assertEquals("Incorrect number of filenames", 2, filenames.size());

        for (int i = 0; i < filenames.size(); i++) {
            File file = new File(filenames.get(i));
            String fileExtension = file.getName().substring(file.getName().lastIndexOf("."));
            assertTrue("File doesn't exist", file.exists());
            assertTrue("Files are not CSV or KML", fileExtension.equals(".csv") || fileExtension.equals(".kml"));

            if (file.exists()) {
                file.delete();
            }
        }
    }

    public void testCreateFilesAll() {
        ArrayList<String> filenames = fileHandler.createFiles(sightings, true, true);
        assertEquals("Incorrect number of filenames", 3, filenames.size());

        for (int i = 0; i < filenames.size(); i++) {
            File file = new File(filenames.get(i));
            String fileExtension = file.getName().substring(file.getName().lastIndexOf("."));
            assertTrue("File doesn't exist", file.exists());
            assertTrue("Files are not CSV, GPX or KML",
                    fileExtension.equals(".csv") || fileExtension.equals(".gpx") || fileExtension.equals(".kml"));

            if (file.exists()) {
                file.delete();
            }
        }
    }

    public void testGetSpeciesFromStreamWithValidInput() {
        String fileData = "Group,Geography,GenusName,ScientificName,CommonName,Italicise\n" +
                "JUnit,Geography,Test,Framework,Testing Framework,0";
        InputStream is = new ByteArrayInputStream(fileData.getBytes());

        try {
            assertEquals("Did not create array of correct length from InputStream",
                    1, fileHandler.getSpeciesFromStream(is).size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testGetSpeciesFromStreamWithTrailingLinebreak() {
        String fileData = "Group,Geography,GenusName,ScientificName,CommonName,Italicise\n" +
                "JUnit,Geography,Test,Framework,Testing Framework,0\n";
        InputStream is = new ByteArrayInputStream(fileData.getBytes());

        try {
            assertEquals("Did not create array of correct length from InputStream",
                    1, fileHandler.getSpeciesFromStream(is).size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testGetSpeciesFromStreamWithTooFewFields() {
        String fileData = "Group,Geography,GenusName,ScientificName,CommonName\n" +
                "JUnit,Geography,Test,Framework,Testing Framework";
        InputStream is = new ByteArrayInputStream(fileData.getBytes());

        try {
            assertEquals("Did not create empty array", 0, fileHandler.getSpeciesFromStream(is).size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }

    public void testGetSpeciesFromStreamWithTooManyFields() {
        String fileData = "Group,Geography,GenusName,ScientificName,CommonName,Italicise,Too Many\n" +
                "JUnit,Geography,Test,Framework,Testing Framework,0,Extra";
        InputStream is = new ByteArrayInputStream(fileData.getBytes());

        try {
            assertEquals("Did not create empty array", 0, fileHandler.getSpeciesFromStream(is).size());
        } catch (JSONException e) {
            e.printStackTrace();
            fail("JSONException thrown");
        }
    }
}