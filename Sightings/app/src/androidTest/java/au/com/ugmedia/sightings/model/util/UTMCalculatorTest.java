package au.com.ugmedia.sightings.model.util;

import android.test.AndroidTestCase;

public class UTMCalculatorTest extends AndroidTestCase {
    private UTMCalculator calculator;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        calculator = new UTMCalculator();
    }

    public void testGetEastingNorthing() {
        double lat = 50;
        double lng = 50;
        double zone = Math.floor((lng + 180.0) / 6) + 1;
        double[] eastNorth = calculator.getEastingNorthing(lat, lng, zone);

        assertEquals("Easting is incorrect", 428333.55249494105, eastNorth[0]);
        assertEquals("Northing is incorrect", 428333.55249494105, eastNorth[0]);
    }
}