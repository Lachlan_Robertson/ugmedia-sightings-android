package au.com.ugmedia.sightings;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.view.KeyEvent;

public class AddSightingActivityTest extends ActivityInstrumentationTestCase2<AddSightingActivity> {
    private AddSightingActivity activity;
    private Instrumentation instrumentation;

    public AddSightingActivityTest() {
        super(AddSightingActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(true);

        instrumentation = getInstrumentation();
        activity = getActivity();
    }

    @UiThreadTest
    public void testInitGui() {
        activity.initGui();

        assertNotNull("Timestamp View is null", activity.timestampView);
        assertNotNull("Common name View is null", activity.commonNameView);
        assertNotNull("Altitude View is null", activity.altitudeView);
        assertNotNull("Accuracy View is null", activity.accuracyView);
        assertNotNull("Photo View is null", activity.photoView);
        assertNotNull("Health View is null", activity.healthView);
        assertNotNull("Notes View is null", activity.notesView);
        assertNotNull("Site name View is null", activity.siteNameView);
        assertNotNull("Distance View is null", activity.distanceView);
        assertNotNull("Latitude View is null", activity.latitudeView);
        assertNotNull("Longitude View is null", activity.longitudeView);
        assertNotNull("Quantity View is null", activity.quantityView);
        assertNotNull("Species View is null", activity.speciesAutoCompleteView);
        assertNotNull("Survey method View is null", activity.surveyMethodAutoCompleteView);
        assertNotNull("Map fragment is null", activity.mapFragment);

        assertTrue("Timestamp View doesn't have text", !activity.timestampView.getText().toString().equals(""));
    }

    public void testAddSightingMenuItemNoSpeciesName() {
        long expectedNumOfSightings = activity.getAppDataSingleton().getSightings().size();
        instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
        instrumentation.invokeMenuActionSync(activity, R.id.action_save, 0);

        assertEquals("Added invalid sighting", expectedNumOfSightings, activity.getAppDataSingleton().getSightings().size());
    }

    public void testAddSightingMenuItemWithSpeciesName() {
        long expectedNumOfSightings = activity.getAppDataSingleton().getSightings().size() + 1;

        try {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.speciesAutoCompleteView.setText("Species");
                }
            });
            instrumentation.waitForIdleSync();
            instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
            instrumentation.invokeMenuActionSync(activity, R.id.action_save, 0);
            assertEquals("Did not add sighting", expectedNumOfSightings, activity.getAppDataSingleton().getSightings().size());
        } catch (Throwable e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }
}
