package au.com.ugmedia.sightings.model.adapter;

import android.test.AndroidTestCase;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.R;
import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;

public class SpeciesCompletionAdapterTest extends AndroidTestCase {
    private SpeciesCompletionAdapter adapter;
    private JSONObject item;
    private ArrayList<JSONObject> data;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        data = new ArrayList<>();
        item = new JSONObject();
        JSONObject itemData = new JSONObject();

        String scientificName = "JUnit";
        String commonName = "Testing framework";
        String useCount = "1";

        itemData.put(SpeciesKeys.DATA_COMMON_NAME, commonName);

        item.put(SpeciesKeys.SCIENTIFIC_NAME, scientificName);
        item.put(SpeciesKeys.USE_COUNT, useCount);
        item.put(SpeciesKeys.DATA, itemData);
        data.add(item);

        adapter = new SpeciesCompletionAdapter(getContext(), R.layout.list_item_species_dropdown, data);
    }

    public void testGetView() {
        View v = adapter.getView(0, null, null);
        TextView sciNameView = (TextView) v.findViewById(R.id.list_item_species_scientific_name);
        TextView commonNameView = (TextView) v.findViewById(R.id.list_item_species_common_name);
        TextView useCountView = (TextView) v.findViewById(R.id.list_item_species_use_count);

        assertNotNull("View is null", v);
        assertNotNull("Scientific name view is null", sciNameView);
        assertNotNull("Common name view is null", commonNameView);
        assertNotNull("Is alive view is null", useCountView);

        try {
            JSONObject data = new JSONObject(item.getString(SpeciesKeys.DATA));
            String expectedUseCount = getContext().getString(R.string.list_item_species_use_count, item.getString(SpeciesKeys.USE_COUNT));

            assertEquals("Scientific names don't match", item.getString(SpeciesKeys.SCIENTIFIC_NAME), sciNameView.getText().toString());
            assertEquals("Common names don't match", data.getString(SpeciesKeys.DATA_COMMON_NAME), commonNameView.getText().toString());
            assertEquals("Use counts don't match", expectedUseCount, useCountView.getText().toString());
        } catch (JSONException e) {
            fail("JSONException thrown!");
            e.printStackTrace();
        }
    }

    public void testGetCount() {
        assertEquals("Size and count do not match", data.size(), adapter.getCount());
    }

    public void testGetItem() {
        assertEquals("Stored item and adapter item do not match", item, adapter.getItem(0));
    }

    public void testGetCommonName() {
        try {
            JSONObject data = new JSONObject(item.getString(SpeciesKeys.DATA));
            assertEquals("Common names do not match", data.getString(SpeciesKeys.DATA_COMMON_NAME), adapter.getCommonName(0));
        } catch (JSONException e) {
            fail("JSONException thrown!");
            e.printStackTrace();
        }
    }
}