package au.com.ugmedia.sightings.model.util.jsonKeys;

/**
 * Keys for sighting JSONObjects
 */
public final class SightingsKeys {
    public static final String ID = "_id";
    public static final String TIMESTAMP = "Timestamp";
    public static final String DISTANCE_TO = "Distance_To";

    public static final String DATA = "Data";
    public static final String DATA_SCIENTIFIC_NAME = "Scientific_Name";
    public static final String DATA_COMMON_NAME = "Common_Name";
    public static final String DATA_QUANTITY = "Quantity";
    public static final String DATA_PHOTO_TAKEN = "Photo_Taken";
    public static final String DATA_HEALTH = "Health";
    public static final String DATA_NOTES = "Notes";
    public static final String DATA_SITE_NAME = "Site_Name";
    public static final String DATA_SURVEY_METHOD = "Survey_Method";

    public static final String DATA_LOCATION = "Location";
    public static final String DATA_LOCATION_LATITUDE = "Latitude";
    public static final String DATA_LOCATION_LONGITUDE = "Longitude";
    public static final String DATA_LOCATION_ALTITUDE = "Altitude";
    public static final String DATA_LOCATION_DISTANCE = "Distance";
    public static final String DATA_LOCATION_ACCURACY = "Accuracy";
    public static final String DATA_LOCATION_EASTING = "Easting";
    public static final String DATA_LOCATION_NORTHING = "Northing";
    public static final String DATA_LOCATION_HEMISPHERE = "Hemisphere";
    public static final String DATA_LOCATION_ZONE = "Zone";

    public static final String DATA_METADATA = "Metadata";
    public static final String DATA_METADATA_DEVICE_ID = "Device_ID";
    public static final String DATA_METADATA_DEVICE_NAME = "Device_Name";
    public static final String DATA_METADATA_GUID = "GUID";
    public static final String DATA_METADATA_APPLICATION_ID = "Application_ID";
}
