package au.com.ugmedia.sightings.model.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.R;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;

/**
 * An adapter that displays relevant Sightings data to the user.
 * <br>
 * The data displayed is intended for use in a ListView and contains only generic data that requires
 * a user to drill down into.
 */
public class SightingsAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = SightingsAdapter.class.getSimpleName();

    private int resId;

    /**
     * Default constructor.
     *
     * @param context The Context associated with this adapter
     * @param resId   The ID of layout file.
     * @param values  An array of sightings to be displayed to the user
     */
    public SightingsAdapter(Context context, int resId, ArrayList<JSONObject> values) {
        super(context, resId, values);
        this.resId = resId;
    }

    /**
     * Finds and returns the view at the given position, creating the View if required.
     *
     * @param position    The position of the item to search for
     * @param convertView The view associated with the adapter
     * @param parent      The parent 'container' of the convertView
     * @return The view at the specified position
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            v = inflater.inflate(resId, parent, false);
        }

        bindView(position, v);
        return v;
    }

    /**
     * Binds certain properties of sightings to subviews for correct display in the parent View.
     * <br>
     * The data bound is either taken from the Sighting or a default value and may or may not
     * include "distance to".
     *
     * @param position The position of the sighting object in the associated ArrayList
     * @param v        The parent View
     */
    private void bindView(int position, View v) {
        JSONObject sighting = super.getItem(position);

        if (sighting != null) {
            try {
                TextView scientificNameView = (TextView) v.findViewById(R.id.list_item_scientific_name);
                TextView commonNameView = (TextView) v.findViewById(R.id.minor_detail_common_name);
                TextView isAliveView = (TextView) v.findViewById(R.id.minor_detail_is_alive);
                TextView photoTakenView = (TextView) v.findViewById(R.id.minor_detail_photo_taken);
                TextView distanceToView = (TextView) v.findViewById(R.id.minor_detail_distance_to);
                Resources res = getContext().getResources();

                JSONObject data = new JSONObject(sighting.get(SightingsKeys.DATA).toString());
                String scientificName = data.optString(SightingsKeys.DATA_SCIENTIFIC_NAME, res.getString(R.string.default_scientific_name));
                String commonName = data.optString(SightingsKeys.DATA_COMMON_NAME, res.getString(R.string.default_common_name));
                String health = data.optString(SightingsKeys.DATA_HEALTH, res.getString(R.string.default_health));
                String photoTaken = data.optString(SightingsKeys.DATA_PHOTO_TAKEN, res.getString(R.string.default_photo_taken));
                String distanceTo = res.getString(R.string.default_distance_to);

                String distanceToAsString = sighting.optString(SightingsKeys.DISTANCE_TO, null);
                if (distanceToAsString != null && !distanceToAsString.equals("")) {
                    float distance = Float.parseFloat(distanceToAsString);
                    distanceTo = String.format("%.2fm", distance);
                }

                scientificNameView.setText(scientificName);
                commonNameView.setText(commonName);
                isAliveView.setText(health);
                photoTakenView.setText(photoTaken);
                distanceToView.setText(distanceTo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
