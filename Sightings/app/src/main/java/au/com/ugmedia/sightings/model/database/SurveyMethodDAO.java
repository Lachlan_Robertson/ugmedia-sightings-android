package au.com.ugmedia.sightings.model.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

/**
 * A Data Access Object that provides a simple way to make various requests to the Species table
 * in the database.
 */
public class SurveyMethodDAO {
    private static final String TAG = SpeciesDAO.class.getSimpleName();

    public static final String TABLE_NAME = "Survey_Method";

    public static final String ID_COLUMN = "_id";
    public static final String METHOD_NAME_COLUMN = "Method_Name";
    public static final String TIMES_USED_COLUMN = "Times_Used";

    private SQLiteOpenHelper dbHelper;

    /**
     * Constructor.
     *
     * @param helper The helper required to actually access the database
     */
    public SurveyMethodDAO(SQLiteOpenHelper helper) {
        dbHelper = helper;
    }

    /**
     * Gets all survey methods from the database, returning everything except their _id.
     *
     * @return An ArrayList of survey methods
     * @throws JSONException If the stored data is malformed or missing
     */
    public ArrayList<JSONObject> getAllSurveyMethods() throws JSONException {
        ArrayList<JSONObject> allSurveyMethods = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {METHOD_NAME_COLUMN, TIMES_USED_COLUMN};
        Cursor c = db.query(true, TABLE_NAME, columns, null, null, null, null, null, null);

        while (c.moveToNext()) {
            JSONObject surveyMethod = new JSONObject();

            String methodName = c.getString(c.getColumnIndexOrThrow(METHOD_NAME_COLUMN));
            long timesUsed = c.getLong(c.getColumnIndexOrThrow(TIMES_USED_COLUMN));

            surveyMethod.put(SurveyMethodKeys.METHOD_NAME, methodName);
            surveyMethod.put(SurveyMethodKeys.TIMES_USED, timesUsed);

            allSurveyMethods.add(surveyMethod);
        }

        c.close();
        return allSurveyMethods;
    }

    /**
     * Sets the TIMES_USED of every survey method in database to 0.
     */
    public void resetTimesUsed() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "UPDATE " + TABLE_NAME + " SET "
                + TIMES_USED_COLUMN + " = 0";
        db.execSQL(sql);
    }

    /**
     * Increments the TIMES_USED of a particular survey method.
     * <br>
     * If the survey method does not exist, no records will be updated.
     *
     * @param methodName The method name of the species to search for
     */
    public void incrementTimesUsed(String methodName) {
        updateTimesUsed(methodName, "+1");
    }

    /**
     * Decrements the TIMES_USED of a particular survey method.
     * <br>
     * If the survey method does not exist, no records will be updated.
     *
     * @param methodName The method name of the species to search for
     */
    public void decrementTimesUsed(String methodName) {
        updateTimesUsed(methodName, "-1");
    }

    /**
     * Updates a survey method's TIMES_USED by a given amount; increasing / decreasing it as needed.
     * <br>
     * If the survey method does not exist, no records will be updated.
     *
     * @param methodName The method name of the species to search for
     * @param amount     The amount to increase / decrease USE_COUNT by
     */
    private void updateTimesUsed(String methodName, String amount) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String whereClause = METHOD_NAME_COLUMN + " = \"" + methodName + "\"";
        //Standard "update" method doesn't work. Must fall back to standard SQL
        String sql = "UPDATE " + TABLE_NAME + " SET "
                + TIMES_USED_COLUMN + " = " + TIMES_USED_COLUMN + " " + amount + " WHERE "
                + whereClause;
        db.execSQL(sql);
    }
}
