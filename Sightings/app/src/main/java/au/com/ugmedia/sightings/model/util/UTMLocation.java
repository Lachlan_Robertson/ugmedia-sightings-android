package au.com.ugmedia.sightings.model.util;

import android.location.Location;

/**
 * A convenience class that contains the location data required by a sighting.
 */
public class UTMLocation {
    public static final double INVALID_VALUE_FLAG = Double.MAX_VALUE;
    public static final String INVALID_VALUE_FLAG_AS_STRING = "";

    private UTMCalculator calculator;

    private double latitude;
    private double longitude;
    private double altitude;
    private double accuracy;
    private double easting;
    private double northing;
    private String hemisphere;
    private double zone;

    public UTMLocation() {
        calculator = new UTMCalculator();
        latitude = INVALID_VALUE_FLAG;
        longitude = INVALID_VALUE_FLAG;
        altitude = INVALID_VALUE_FLAG;
        accuracy = INVALID_VALUE_FLAG;
    }

    public UTMLocation(Location l) {
        calculator = new UTMCalculator();

        if (l == null) {
            latitude = INVALID_VALUE_FLAG;
            longitude = INVALID_VALUE_FLAG;
            altitude = INVALID_VALUE_FLAG;
            accuracy = INVALID_VALUE_FLAG;
        } else {
            latitude = l.getLatitude();
            longitude = l.getLongitude();
            altitude = l.getAltitude();
            accuracy = l.getAccuracy();
        }
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public double getEasting() {
        if (latitude == INVALID_VALUE_FLAG || longitude == INVALID_VALUE_FLAG) {
            easting = INVALID_VALUE_FLAG;
        } else {
            easting = calculator.getEastingNorthing(latitude, longitude, getZone())[UTMCalculator.EASTING_INDEX];
        }
        return easting;
    }

    public double getNorthing() {
        if (latitude == INVALID_VALUE_FLAG || longitude == INVALID_VALUE_FLAG) {
            northing = INVALID_VALUE_FLAG;
        } else {
            northing = calculator.getEastingNorthing(latitude, longitude, getZone())[UTMCalculator.NORTHING_INDEX];
        }
        return northing;
    }

    public String getHemisphere() {
        if (latitude == INVALID_VALUE_FLAG || longitude == INVALID_VALUE_FLAG) {
            hemisphere = INVALID_VALUE_FLAG_AS_STRING;
        } else {
            if (latitude >= 0) {
                hemisphere = "North";
            } else {
                hemisphere = "South";
            }
        }
        return hemisphere;
    }

    public double getZone() {
        if (longitude == INVALID_VALUE_FLAG) {
            zone = INVALID_VALUE_FLAG;
        } else {
            zone = Math.floor((longitude + 180.0) / 6) + 1;
        }
        return zone;
    }

    public String getZoneAsString() {
        return getZone() + "";
    }

    @Override
    public String toString() {
        return "UTMLocation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", altitude=" + altitude +
                ", accuracy=" + accuracy +
                ", easting=" + easting +
                ", northing=" + northing +
                ", hemisphere='" + hemisphere + '\'' +
                ", zone='" + zone + '\'' +
                '}';
    }
}
