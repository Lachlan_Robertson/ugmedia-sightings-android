package au.com.ugmedia.sightings.model.util.gps;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * A class responsible for the querying of GPS data.
 * Features simple methods to query and return location-based data.
 */
public class GPSHelper extends GPSSubject implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = GPSHelper.class.getSimpleName();

    private static final long INTERVAL = 1 * 100; //0.1sec
    private static final long FASTEST_INTERVAL = 1 * 10; //0.01sec

    private Context context;
    private GoogleApiClient client;
    private LocationRequest request;

    /**
     * Default constructor. Builds the GPS client from context.
     *
     * @param context The underlying context required for the GPS client.
     */
    public GPSHelper(Context context) {
        this.context = context;
        buildGoogleApiClient();
        client.connect();

        request = new LocationRequest();
        request.setInterval(INTERVAL);
        request.setFastestInterval(FASTEST_INTERVAL);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * A convenience method that constructs a new GoogleApiClient with associated listeners.
     */
    private void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Starts listening for location updates that are then handled through onLocationChanged and forwarded to subject listeners.
     * This occurs only if the client is connected (not connecting).
     */
    protected void startLocationUpdates() {
        if (client.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, request, this);
        }
    }

    /**
     * Called when the device detects that the location has changed, updates location and notifies all listeners.
     *
     * @param location The new Location object
     */
    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        notifyOfNewLocation();
    }

    /**
     * Stops listening for location changes on this object.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(client, this);
    }

    /**
     * Called when a GoogleApiClient successfully finishes connecting.
     * Requests the new location that will be returned when a new location is available.
     *
     * @param connectionHint Information about the connection.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        startRequestingLocations();
    }

    /**
     * Requests a new location by starting an update 'service' which will return the next available location.
     * <p/>
     * Note that this method returns instantly and location may not be defined yet or may not be the most recent value.
     */
    @Override
    public void startRequestingLocations() {
        startLocationUpdates();
    }

    /**
     * Requests to stop listening for new updates by stopping the GPS request 'service'.
     */
    public void stopRequestingLocations() {
        stopLocationUpdates();
    }

    /**
     * Returns the value of the last stored location.
     *
     * @return The last queried location or null if it does not exist
     */
    public Location getLocation() {
        return location;
    }

    // ---- Unused Methods ----

    /**
     * Called when a client's connection is suspended.
     * Unused.
     *
     * @param i An integer flag
     */
    @Override
    public void onConnectionSuspended(int i) {
        //Unused
    }

    /**
     * Called when a client fails to successfully connect.
     * Unused.
     *
     * @param connectionResult The result of the connection containing connection information
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Unused
    }
}
