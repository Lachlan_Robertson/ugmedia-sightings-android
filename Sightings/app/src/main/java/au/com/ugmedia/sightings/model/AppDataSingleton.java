package au.com.ugmedia.sightings.model;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import au.com.ugmedia.sightings.model.database.DatabaseHelperSingleton;
import au.com.ugmedia.sightings.model.database.SightingsDAO;
import au.com.ugmedia.sightings.model.database.SpeciesDAO;
import au.com.ugmedia.sightings.model.database.SurveyMethodDAO;
import au.com.ugmedia.sightings.model.util.SightingsMetadata;
import au.com.ugmedia.sightings.model.util.UTMLocation;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;
import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;
import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

/**
 * A Singleton that provides access to other helper classes for GPS locations, database manipulation
 * and storage of application data.
 * <br>
 * By storing data in one location it means the data only needs to be loaded once and helps reduce
 * further strain on the app later on. This also makes the app much faster as it reduces the number
 * of database calls needed to keep data up-to-date.
 */
public class AppDataSingleton {
    private static final String TAG = AppDataSingleton.class.getSimpleName();

    private static AppDataSingleton instance;
    private static ArrayList<JSONObject> sightings;
    private static ArrayList<JSONObject> species;
    private static ArrayList<JSONObject> surveyMethods;

    private Context context;
    private SQLiteOpenHelper databaseHelper;
    private SightingsDAO sightingsDAO;
    private SpeciesDAO speciesDAO;
    private SurveyMethodDAO surveyMethodDAO;

    public enum SightingsSortMethod {
        DATE_NEWEST, DATE_OLDEST, DISTANCE_CLOSEST, DISTANCE_FURTHEST;

        public static SightingsSortMethod convertToSortMethod(String sortMethodAsString) {
            SightingsSortMethod sortMethod;
            try {
                sortMethod = valueOf(sortMethodAsString);
            } catch (Exception e) {
                sortMethod = DATE_NEWEST;
            }

            return sortMethod;
        }
    }

    /**
     * Private constructor as per singleton design.
     * <br>
     * Also initialises data by loading previous data from the database.
     *
     * @param context The context used to create various other objects
     * @throws JSONException If database data is malformed or missing
     */
    private AppDataSingleton(Context context) throws JSONException {
        this.context = context;
        initSightings();
        initSpecies();
        initSurveyMethods();
    }

    /**
     * Gets and returns an instance to this object.
     *
     * @return An instance of this object.
     */
    public static AppDataSingleton getInstance(Context context) throws JSONException {
        if (instance == null)
            instance = new AppDataSingleton(context);
        return instance;
    }

    // -- BEGIN: Sightings methods

    /**
     * Initialises the sightings ArrayList by loading in all data stored in the database.
     *
     * @throws JSONException If the database data is malformed or missing
     */
    private void initSightings() throws JSONException {
        sightings = getSightingsDAO().getAllSightings();
    }

    /**
     * Returns the sightings array utilised by this class
     *
     * @return The sightings ArrayList
     */
    public ArrayList<JSONObject> getSightings() {
        return sightings;
    }

    /**
     * Adds a sighting to the database and associated ArrayList and updates species and survey
     * method counts as appropriate.
     * <br>
     * Clients should note that if the data cannot be inserted no exceptions will be raised and no
     * data will be added (both to the ArrayList or the database).
     *
     * @param sighting The sighting to add
     * @return The inserted row id on success or -1 on failure
     * @throws JSONException If the data is malformed or missing
     */
    public long addSighting(JSONObject sighting) throws JSONException {
        long insertedRowId = getSightingsDAO().addNewSighting(sighting);

        if (insertedRowId != -1) {
            sighting.put(SightingsKeys.ID, insertedRowId);
            JSONObject data = new JSONObject(sighting.get(SightingsKeys.DATA).toString());
            String scientificName = data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
            String methodName = data.getString(SightingsKeys.DATA_SURVEY_METHOD);

            getSpeciesDAO().incrementUseCount(scientificName);
            getSurveyMethodDAO().incrementTimesUsed(data.getString(SightingsKeys.DATA_SURVEY_METHOD));

            sightings.add(sighting);
            incrementSpeciesCount(scientificName);
            incrementSurveyMethodCount(methodName);
        }

        return insertedRowId;
    }

    /**
     * Duplicates a sighting, inserting a new record like an existing sighting and increasing species
     * and survey method counts as needed.
     * <br>
     * Clients should note that if the data cannot be inserted no exceptions will be raised and no
     * data will be added (both to the ArrayList or the database).
     *
     * @param originalSightingId The _id of the original sighting to duplicate
     * @param location           The location of the new sighting
     * @param metadata           The metadata of the new sighting
     * @return The inserted row id on success or -1 on failure
     * @throws JSONException If the data is malformed or missing
     */
    public boolean duplicateSighting(long originalSightingId, UTMLocation location, SightingsMetadata metadata) throws JSONException {
        boolean successful = true;
        long addedRowId = getSightingsDAO().duplicateSighting(originalSightingId, location, metadata);

        if (addedRowId != -1) {
            JSONObject insertedRecord = getSightingsDAO().getSighting(addedRowId);
            insertedRecord.put(SightingsKeys.ID, addedRowId);
            JSONObject data = new JSONObject(insertedRecord.get(SightingsKeys.DATA).toString());
            String scientificName = data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
            String methodName = data.getString(SightingsKeys.DATA_SURVEY_METHOD);

            getSpeciesDAO().incrementUseCount(scientificName);
            getSurveyMethodDAO().incrementTimesUsed(methodName);

            sightings.add(insertedRecord);
            incrementSpeciesCount(scientificName);
            incrementSurveyMethodCount(methodName);
        } else {
            successful = false;
        }

        return successful;
    }

    /**
     * Updates a sighting with the given _id to use new data and updates species and survey method
     * counts.
     *
     * @param id          The _id of the sighting
     * @param newSighting The JSONObject containing the data to update the sighting to
     * @return true if the sighting was updated, false otherwise
     * @throws JSONException If the data is malformed or missing
     */
    public boolean updateSighting(long id, JSONObject newSighting) throws JSONException {
        boolean successful = true;
        JSONObject oldSighting = getSightingsDAO().getSighting(id);

        if (getSightingsDAO().updateSighting(id, newSighting) > 0) {
            newSighting.put(SightingsKeys.ID, id);
            JSONObject oldData = new JSONObject(oldSighting.get(SightingsKeys.DATA).toString());
            String oldScientificName = oldData.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
            String oldMethodName = oldData.getString(SightingsKeys.DATA_SURVEY_METHOD);

            JSONObject newData = new JSONObject(newSighting.get(SightingsKeys.DATA).toString());
            String newScientificName = newData.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
            String newMethodName = newData.getString(SightingsKeys.DATA_SURVEY_METHOD);

            getSpeciesDAO().decrementUseCount(oldScientificName);
            getSurveyMethodDAO().decrementTimesUsed(oldMethodName);
            getSpeciesDAO().incrementUseCount(newScientificName);
            getSurveyMethodDAO().incrementTimesUsed(newMethodName);

            sightings.remove(findSightingIndex(id));
            sightings.add(newSighting);
            decrementSpeciesCount(oldScientificName);
            incrementSpeciesCount(newScientificName);
            decrementSurveyMethodCount(oldMethodName);
            incrementSurveyMethodCount(newMethodName);
        } else {
            successful = false;
        }

        return successful;
    }

    /**
     * Removes a sighting with the given id from both the database and ArrayList and updates species
     * and survey method counts.
     *
     * @param sightingId The _id of the sighting to remove
     * @return true if the sighting was removed, false otherwise
     * @throws JSONException If the data is malformed or missing
     */
    public boolean removeSighting(long sightingId) throws JSONException {
        boolean successful = true;
        JSONObject recordToRemove = getSightingsDAO().getSighting(sightingId);
        if (recordToRemove == null)
            return false;

        JSONObject data = new JSONObject(recordToRemove.get(SightingsKeys.DATA).toString());
        String scientificName = data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
        String methodName = data.getString(SightingsKeys.DATA_SURVEY_METHOD);

        if (getSightingsDAO().removeSighting(sightingId) != -1) {
            getSpeciesDAO().decrementUseCount(scientificName);
            getSurveyMethodDAO().decrementTimesUsed(methodName);

            sightings.remove(findSightingIndex(sightingId));
            decrementSpeciesCount(scientificName);
            decrementSurveyMethodCount(methodName);
        } else {
            successful = false;
        }
        return successful;
    }

    /**
     * Completely removes all sightings from the database and ArrayList and reset species and
     * survey method counts.
     *
     * @throws JSONException If the data is malformed or missing
     */
    public void removeAllRecords() throws JSONException {
        getSightingsDAO().removeAllSightings();
        getSpeciesDAO().resetUseCount();
        getSurveyMethodDAO().resetTimesUsed();

        sightings.clear();
        resetSpeciesUseCount();
        resetSurveyMethodTimesUsed();
    }

    /**
     * Calculates distance to and sorts sightings records.
     * <br>
     * It must be noted that if location is null distance to will not be calculated and all distance
     * sorting methods will have no effect.
     *
     * @param sortMethod How the data should be sorted
     * @param location   The current location
     */
    public void sortSightings(SightingsSortMethod sortMethod, Location location) {
        // Make copy so that any other threads can still use the data without a
        // ConcurrentModificationException being raised
        ArrayList<JSONObject> sightingsCopy = new ArrayList<>(sightings);

        if (location != null) {
            try {
                for (JSONObject sighting : sightingsCopy) {
                    JSONObject data = new JSONObject(sighting.getString(SightingsKeys.DATA));
                    JSONObject locationFrom = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));
                    String distanceToAsString = calcDistanceToAsString(locationFrom, location);
                    sighting.put(SightingsKeys.DISTANCE_TO, distanceToAsString);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        switch (sortMethod) {
            case DATE_NEWEST:
                Collections.sort(sightingsCopy, new SightingsComparators.DateNewestComparator());
                break;
            case DATE_OLDEST:
                Collections.sort(sightingsCopy, new SightingsComparators.DateOldestComparator());
                break;
            case DISTANCE_CLOSEST:
                if (location != null) {
                    SightingsComparators.DistanceClosestComparator comparator = new SightingsComparators.DistanceClosestComparator();
                    Collections.sort(sightingsCopy, comparator);
                }
                break;
            case DISTANCE_FURTHEST:
                if (location != null) {
                    SightingsComparators.DistanceFurthestComparator comparator = new SightingsComparators.DistanceFurthestComparator();
                    Collections.sort(sightingsCopy, comparator);
                }
                break;
        }

        sightings = sightingsCopy;
    }

    /**
     * Convenience method to find the index of a sighting in the stored ArrayList given a sighting
     * _id.
     *
     * @param sightingId The id of the sighting to locate
     * @return The index of the sighting on success or -1 on failure
     * @throws JSONException If the data is malformed or missing
     */
    private int findSightingIndex(long sightingId) throws JSONException {
        int index = -1;
        for (JSONObject sighting : sightings) {
            if (sighting.getInt(SightingsKeys.ID) == sightingId) {
                index = sightings.indexOf(sighting);
                break;
            }
        }

        return index;
    }

    // -- BEGIN: Species methods

    /**
     * Initialises the species ArrayList by loading in all data stored in the database.
     *
     * @throws JSONException If the database data is malformed or missing
     */
    private void initSpecies() throws JSONException {
        species = getSpeciesDAO().getAllSpecies();
    }

    /**
     * Returns the species array utilised by this class
     *
     * @return The species ArrayList
     */
    public ArrayList<JSONObject> getSpecies() {
        return species;
    }

    /**
     * Adds a species to the database and the associated ArrayList.
     * <br>
     * Clients should note that if an error occurs and the species cannot be added to the database,
     * it will also not be added to the ArrayList
     *
     * @param s The species to add
     * @throws JSONException If the data is malformed or missing
     */
    public void addSpecies(JSONObject s) throws JSONException {
        if (getSpeciesDAO().addSpecies(s) != -1) {
            species.add(s);
        }
    }

    /**
     * Increments the species USE_COUNT in the database and associated ArrayList for a given name.
     *
     * @param scientificName The scientific name of the species to increment
     * @throws JSONException If the data is malformed or missing
     */
    public void incrementSpeciesCount(String scientificName) throws JSONException {
        int index = findSpeciesIndex(scientificName);
        if (index != -1) {
            JSONObject speciesRecord = species.get(index);
            int useCount = speciesRecord.getInt(SpeciesKeys.USE_COUNT);
            speciesRecord.put(SpeciesKeys.USE_COUNT, ++useCount);
            species.set(index, speciesRecord);
        }
    }

    /**
     * Decrements the species USE_COUNT in the database and associated ArrayList for a given name.
     *
     * @param scientificName The scientific name of the species to decrement
     * @throws JSONException If the data is malformed or missing
     */
    public void decrementSpeciesCount(String scientificName) throws JSONException {
        int index = findSpeciesIndex(scientificName);
        if (index != -1) {
            JSONObject speciesRecord = species.get(index);
            int useCount = speciesRecord.getInt(SpeciesKeys.USE_COUNT);
            speciesRecord.put(SpeciesKeys.USE_COUNT, --useCount);
            species.set(index, speciesRecord);
        }
    }

    /**
     * Convenience method to find the index of a species in the stored ArrayList given a species'
     * scientific name
     *
     * @param scientificName The scientific name of the species to locate
     * @return The index of the species on success or -1 on failure
     * @throws JSONException If the data is malformed or missing
     */
    private int findSpeciesIndex(String scientificName) throws JSONException {
        int index = -1;
        for (JSONObject speciesRecord : species) {
            if (speciesRecord.getString(SpeciesKeys.SCIENTIFIC_NAME).equals(scientificName)) {
                index = species.indexOf(speciesRecord);
                break;
            }
        }

        return index;
    }

    /**
     * Resets the USE_COUNT of all species in the ArrayList
     *
     * @throws JSONException If the data is malformed or missing
     */
    private void resetSpeciesUseCount() throws JSONException {
        for (int i = 0; i < species.size(); i++) {
            JSONObject record = species.get(i);
            record.put(SpeciesKeys.USE_COUNT, 0);
            species.set(i, record);
        }
    }

    // -- BEGIN: SurveyMethod methods

    /**
     * Initialises the survey method ArrayList by loading in all data stored in the database.
     *
     * @throws JSONException If the database data is malformed or missing
     */
    private void initSurveyMethods() throws JSONException {
        surveyMethods = getSurveyMethodDAO().getAllSurveyMethods();
    }

    /**
     * Returns the survey method array utilised by this class
     *
     * @return The survey method ArrayList
     */
    public ArrayList<JSONObject> getSurveyMethods() {
        return surveyMethods;
    }

    /**
     * Increments the survey method TIMES_USED in the database and associated ArrayList for a given
     * name.
     *
     * @param methodName The method name of the survey method to increment
     * @throws JSONException If the data is malformed or missing
     */
    public void incrementSurveyMethodCount(String methodName) throws JSONException {
        int index = findSurveyMethodIndex(methodName);
        if (index != -1) {
            JSONObject surveyMethodRecord = surveyMethods.get(index);
            int timesUsed = surveyMethodRecord.getInt(SurveyMethodKeys.TIMES_USED);
            surveyMethodRecord.put(SurveyMethodKeys.TIMES_USED, ++timesUsed);
            surveyMethods.set(index, surveyMethodRecord);
        }
    }

    /**
     * Decrements the survey method TIMES_USED in the database and associated ArrayList for a given
     * name.
     *
     * @param methodName The method name of the survey method to decrement
     * @throws JSONException If the data is malformed or missing
     */
    public void decrementSurveyMethodCount(String methodName) throws JSONException {
        int index = findSurveyMethodIndex(methodName);
        if (index != -1) {
            JSONObject surveyMethodRecord = surveyMethods.get(index);
            int timesUsed = surveyMethodRecord.getInt(SurveyMethodKeys.TIMES_USED);
            surveyMethodRecord.put(SurveyMethodKeys.TIMES_USED, --timesUsed);
            surveyMethods.set(index, surveyMethodRecord);
        }
    }

    /**
     * Convenience method to find the index of a survey method in the stored ArrayList given a
     * survey method's name
     *
     * @param methodName The method name of the survey method to locate
     * @return The index of the survey method on success or -1 on failure
     * @throws JSONException If the data is malformed or missing
     */
    private int findSurveyMethodIndex(String methodName) throws JSONException {
        int index = -1;
        for (JSONObject surveyMethodRecord : surveyMethods) {
            if (surveyMethodRecord.getString(SurveyMethodKeys.METHOD_NAME).equals(methodName)) {
                index = surveyMethods.indexOf(surveyMethodRecord);
                break;
            }
        }

        return index;
    }

    /**
     * Resets the TIMES_USED of all survey methods in the ArrayList
     *
     * @throws JSONException If the data is malformed or missing
     */
    private void resetSurveyMethodTimesUsed() throws JSONException {
        for (int i = 0; i < surveyMethods.size(); i++) {
            JSONObject record = surveyMethods.get(i);
            record.put(SurveyMethodKeys.TIMES_USED, 0);
            surveyMethods.set(i, record);
        }
    }

    // -- BEGIN: Helper methods

    /**
     * Calculates the distance from a stored location to the current location and casts it to a
     * String.
     * <br>
     * Clients should note that if the retrieved latitude, longitude, altitude or accuracy doesn't
     * exist the return value will be null.
     *
     * @param locationFrom The JSONObject containing start distance data
     * @param to           The current location
     * @return The distance from stored to current as a String on success or null on failure
     */
    private String calcDistanceToAsString(JSONObject locationFrom, Location to) {
        String distanceToAsString = null;
        String lngAsString = locationFrom.optString(SightingsKeys.DATA_LOCATION_LONGITUDE, "");
        String latAsString = locationFrom.optString(SightingsKeys.DATA_LOCATION_LATITUDE, "");
        String altAsString = locationFrom.optString(SightingsKeys.DATA_LOCATION_ALTITUDE, "");
        String accAsString = locationFrom.optString(SightingsKeys.DATA_LOCATION_ACCURACY, "");

        if (!(lngAsString.equals("") || latAsString.equals("") || accAsString.equals("") || altAsString.equals(""))) {
            double longitude = Double.parseDouble(lngAsString);
            double latitude = Double.parseDouble(latAsString);
            double altitude = Double.parseDouble(altAsString);
            float accuracy = Float.parseFloat(accAsString);

            Location storedLocation = new Location("No_Provider");
            storedLocation.setLongitude(longitude);
            storedLocation.setLatitude(latitude);
            storedLocation.setAltitude(altitude);
            storedLocation.setAccuracy(accuracy);
            distanceToAsString = String.valueOf(storedLocation.distanceTo(to));
        }

        return distanceToAsString;
    }

    // -- BEGIN: Getters + Setters
    private SightingsDAO getSightingsDAO() {
        if (sightingsDAO == null)
            sightingsDAO = new SightingsDAO(getDatabaseHelper());
        return sightingsDAO;
    }

    private SpeciesDAO getSpeciesDAO() {
        if (speciesDAO == null)
            speciesDAO = new SpeciesDAO(getDatabaseHelper());
        return speciesDAO;
    }

    private SurveyMethodDAO getSurveyMethodDAO() {
        if (surveyMethodDAO == null)
            surveyMethodDAO = new SurveyMethodDAO(getDatabaseHelper());
        return surveyMethodDAO;
    }

    private SQLiteOpenHelper getDatabaseHelper() {
        if (databaseHelper == null)
            databaseHelper = DatabaseHelperSingleton.getInstance(context).getDbHelper();
        return databaseHelper;
    }
}
