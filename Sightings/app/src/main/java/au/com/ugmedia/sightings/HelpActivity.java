package au.com.ugmedia.sightings;

import android.content.Context;
import android.os.Bundle;

/**
 * Activity that displays basic "help" information.
 * <br>
 * As it is intended for informational use only, it contain little logic.
 */
public class HelpActivity extends DarkThemeActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
    }

    @Override
    protected Context getContext() {
        return this;
    }
}
