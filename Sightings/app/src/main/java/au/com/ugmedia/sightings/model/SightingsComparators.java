package au.com.ugmedia.sightings.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;

/**
 * Comparators used primarily by AppDataSingleton to sort ArrayList data.
 * {@see AppDataSingleton}
 */
public class SightingsComparators {
    private static final String TAG = SightingsComparators.class.getSimpleName();

    /**
     * A comparator used to sort a list from the newest timestamp to the oldest timestamp using a
     * default DateTime format.
     */
    public static class DateNewestComparator implements Comparator<JSONObject> {
        int sortValue = 0;

        @Override
        public int compare(JSONObject lhs, JSONObject rhs) {
            DateFormat formatter = SimpleDateFormat.getDateTimeInstance();
            try {
                Date lhsTimestamp = formatter.parse(lhs.getString(SightingsKeys.TIMESTAMP));
                Date rhsTimestamp = formatter.parse(rhs.getString(SightingsKeys.TIMESTAMP));
                sortValue = lhsTimestamp.compareTo(rhsTimestamp) * -1;
            } catch (JSONException | ParseException e) {
                e.printStackTrace();
            }

            return sortValue;
        }
    }

    /**
     * A comparator used to sort a list from the oldest timestamp to the newest timestamp using a
     * default DateTime format.
     */
    public static class DateOldestComparator implements Comparator<JSONObject> {
        @Override
        public int compare(JSONObject lhs, JSONObject rhs) {
            return new DateNewestComparator().compare(lhs, rhs) * -1;
        }
    }

    /**
     * A comparator used to sort a list from the closest "distance to" value to the furthest
     * "distance to" value.
     * <br>
     * Note that if either expression does not have a "distance to" value it will default to a
     * distance of 0, making it the closest.
     */
    public static class DistanceClosestComparator implements Comparator<JSONObject> {
        @Override
        public int compare(JSONObject lhs, JSONObject rhs) {
            int sortValue = 0;

            String lhsDistanceToAsString = lhs.optString(SightingsKeys.DISTANCE_TO, null);
            String rhsDistanceToAsString = rhs.optString(SightingsKeys.DISTANCE_TO, null);

            if (lhsDistanceToAsString == null) {
                lhsDistanceToAsString = "0";
            }
            if (rhsDistanceToAsString == null) {
                rhsDistanceToAsString = "0";
            }

            float lhsDistanceTo = Float.parseFloat(lhsDistanceToAsString);
            float rhsDistanceTo = Float.parseFloat(rhsDistanceToAsString);

            if (lhsDistanceTo < rhsDistanceTo) {
                sortValue = -1;
            } else if (lhsDistanceTo > rhsDistanceTo) {
                sortValue = 1;
            }

            return sortValue;
        }
    }

    /**
     * A comparator used to sort a list from the furthest "distance to" value to the closest
     * "distance to" value.
     */
    public static class DistanceFurthestComparator implements Comparator<JSONObject> {
        @Override
        public int compare(JSONObject lhs, JSONObject rhs) {
            DistanceClosestComparator comparator = new DistanceClosestComparator();
            return comparator.compare(lhs, rhs) * -1;
        }
    }
}
