package au.com.ugmedia.sightings.view;

import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import org.json.JSONException;

import au.com.ugmedia.sightings.R;
import au.com.ugmedia.sightings.model.database.DatabaseHelperSingleton;
import au.com.ugmedia.sightings.model.database.SightingsDAO;
import au.com.ugmedia.sightings.model.database.SpeciesDAO;

/**
 * A fragment used on the SettingsActivity to display all settings the user can choose from and
 * modify.
 */
public class SettingsFragment extends PreferenceFragment {
    private static final String TAG = SettingsFragment.class.getSimpleName();

    private SQLiteOpenHelper databaseHelper;
    private SightingsDAO sightingsDAO;
    private SpeciesDAO speciesDAO;

    /**
     * Creates the fragment with default values and updates views based on user preferences as
     * required.
     *
     * @param savedInstanceState The saved state of this activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_prefs);

        Preference defaultEmailPref = findPreference(getString(R.string.default_email_key));
        Preference deviceNamePref = findPreference(getString(R.string.device_name_key));
        Preference aboutAppVersion = findPreference(getString(R.string.about_sightings_screen_key));

        defaultEmailPref.setSummary(getPrefs().getString(getString(R.string.default_email_key), ""));
        deviceNamePref.setSummary(Build.MODEL);

        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            aboutAppVersion.setSummary(getString(R.string.about_version, packageInfo.versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        updateRecordSummaries();
    }

    /**
     * Updates the GUI to reflect the user's preferences and specific settings.
     */
    public void updateRecordSummaries() {
        Preference recordCountPref = findPreference(getString(R.string.record_count_key));
        Preference uniqueRecordNamesPref = findPreference(getString(R.string.unique_record_names_key));
        ScientificNamePreference mostUsedNamePref = (ScientificNamePreference) findPreference(getString(R.string.most_used_name_key));
        Preference csvFilesToKeepPref = findPreference(getString(R.string.csv_files_to_keep_key));
        String mostUsedSpecies = getSpeciesDAO().getMostUsedSpecies();
        long timesUsed = getSpeciesDAO().getTimesUsed(mostUsedSpecies);

        recordCountPref.setSummary(String.valueOf(getSightingsDAO().getNumberOfRecords()));
        try {
            uniqueRecordNamesPref.setSummary(String.valueOf(getSightingsDAO().getUniqueNumberOfRecords()));
        } catch (JSONException e) {
            e.printStackTrace();
            uniqueRecordNamesPref.setSummary("Error!");
        }
        mostUsedNamePref.setSummary(mostUsedSpecies, " - used " + timesUsed + " times");
        csvFilesToKeepPref.setSummary(String.valueOf(getPrefs().getInt(getString(R.string.csv_files_to_keep_key), NumberPickerPreference.DEFAULT_VALUE)));
    }

    // -- BEGIN: Getters and Setters
    private SharedPreferences getPrefs() {
        return getPreferenceScreen().getSharedPreferences();
    }

    private SQLiteOpenHelper getDatabaseHelper() {
        if (databaseHelper == null)
            databaseHelper = DatabaseHelperSingleton.getInstance(getActivity().getBaseContext()).getDbHelper();
        return databaseHelper;
    }

    private SightingsDAO getSightingsDAO() {
        if (sightingsDAO == null)
            sightingsDAO = new SightingsDAO(getDatabaseHelper());
        return sightingsDAO;
    }

    private SpeciesDAO getSpeciesDAO() {
        if (speciesDAO == null)
            speciesDAO = new SpeciesDAO(getDatabaseHelper());
        return speciesDAO;
    }
}
