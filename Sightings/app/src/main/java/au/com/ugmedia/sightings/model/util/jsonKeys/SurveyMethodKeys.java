package au.com.ugmedia.sightings.model.util.jsonKeys;

/**
 * Keys for survey method JSONObjects
 */
public final class SurveyMethodKeys {
    public static final String ID = "_id";
    public static final String METHOD_NAME = "Method_Name";
    public static final String TIMES_USED = "Times_Used";
}
