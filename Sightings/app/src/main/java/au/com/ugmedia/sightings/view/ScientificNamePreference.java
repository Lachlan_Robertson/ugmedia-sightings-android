package au.com.ugmedia.sightings.view;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import au.com.ugmedia.sightings.R;

/**
 * A custom preference that contains two 'sections' of text styled differently for scientific names
 * and other data.
 */
public class ScientificNamePreference extends Preference {
    private static final String TAG = ScientificNamePreference.class.getSimpleName();

    private String section1Text;
    private String section2Text;

    public ScientificNamePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ScientificNamePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScientificNamePreference(Context context) {
        super(context);
    }

    /**
     * Finds and inflates the custom view for this preference.
     *
     * @param parent The parent of this View
     * @return The newly inflated view
     */
    @Override
    protected View onCreateView(ViewGroup parent) {
        super.onCreateView(parent);
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return li.inflate(R.layout.preference_scientific_name, parent, false);
    }

    /**
     * Binds data to the parent view, allowing each text 'section' to have its data set as required.
     *
     * @param view The View of the entire preference
     */
    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        TextView section1 = (TextView) view.findViewById(R.id.section_1);
        TextView section2 = (TextView) view.findViewById(R.id.section_2);

        section1.setText(section1Text);
        section2.setText(section2Text);
    }

    /**
     * A convenience method that allows both section's text to be set for display when the data is
     * bound to the preference.
     *
     * @param section1Text The text to appear in the first, italicised section.
     *                     Usually a scientific name
     * @param section2Text The text to appear in the second, non-italicised section.
     */
    public void setSummary(String section1Text, String section2Text) {
        this.section1Text = section1Text;
        this.section2Text = section2Text;
    }
}