package au.com.ugmedia.sightings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;

import au.com.ugmedia.sightings.model.AppDataSingleton;
import au.com.ugmedia.sightings.model.database.DatabaseHelperSingleton;
import au.com.ugmedia.sightings.model.database.SightingsDAO;
import au.com.ugmedia.sightings.model.util.gps.GPSHelper;
import au.com.ugmedia.sightings.model.util.gps.GPSHelperSingleton;

// App background detection code sourced from
// http://stackoverflow.com/questions/20469619/detecting-android-application-going-to-background

/**
 * An abstract activity that implements functionality required by most other activities.
 */
public abstract class DarkThemeActivity extends Activity {
    private static final String TAG = DarkThemeActivity.class.getSimpleName();

    protected static int numOfRunningActivities = 0;

    private GPSHelperSingleton gpsSingleton;
    private GPSHelper gpsHelper;
    private LocationManager manager;
    private SQLiteOpenHelper databaseHelper;
    private DatabaseHelperSingleton databaseSingleton;
    private SightingsDAO sightingsDAO;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private AppDataSingleton appDataSingleton;

    /**
     * Sets the activity's theme to be the dark theme if enabled.
     *
     * @param savedInstanceState The activity's saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isDarkThemeSet()) {
            setTheme(R.style.DarkTheme);
        }
    }

    /**
     * Starts the activity and, if the activity was in the background, starts requesting new
     * locations and starts the GPS auto-off task if these options are enabled.
     */
    @Override
    protected void onStart() {
        super.onStart();
        if (numOfRunningActivities == 0) {
            // App now in foreground
            Log.i(TAG, "App in foreground.");
            if (isGpsEnabled()) {
                getGpsHelper().startRequestingLocations();
                if (shouldStartAutoOffTask()) {
                    getGpsSingleton().startGpsAutoOffTask();
                }
            }
        }

        numOfRunningActivities++;
    }

    /**
     * Stops the activity and, if the app is going into the background, disables the GPS and
     * auto-off task if set by the user.
     */
    @Override
    protected void onStop() {
        super.onStop();
        numOfRunningActivities--;
        if (numOfRunningActivities == 0) {
            // App now in BG
            Log.i(TAG, "App in background.");
            if (isGpsEnabled() && !isGpsEnabledInBg()) {
                getGpsSingleton().stopGpsAutoOffTask();
                getGpsHelper().stopRequestingLocations();
            }
        }
    }

    /**
     * Resumes the activity, resets the GPS auto-off task (if needed) and gets a 'wake lock' to turn
     * off the screen automatically if required.
     */
    @Override
    protected void onResume() {
        super.onResume();
        resetGpsTask();
        if (getPrefs().getBoolean(getString(R.string.screen_auto_off_key), false)) {
            if (getPowerManager().isWakeLockLevelSupported(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK)) {
                acquireWakeLock();
            }
        }
    }

    /**
     * Pauses the activity and releases the 'wake lock' to allow the screen to stay on if set by
     * the user.
     */
    @Override
    protected void onPause() {
        super.onPause();
        releaseWakeLock();
    }

    /**
     * Releases any previous 'wake locks' and acquires a new lock to disable the screen when the
     * device is placed into a user's pocket.
     */
    protected void acquireWakeLock() {
        releaseWakeLock();
        getWakeLock().acquire();
    }

    /**
     * Releases the active 'wake lock' if able to free resources
     */
    protected void releaseWakeLock() {
        if (getWakeLock().isHeld()) {
            getWakeLock().release();
        }
    }

    // ---- BEGIN: Getters and Setters ----
    protected abstract Context getContext();

    protected boolean isDarkThemeSet() {
        return getPrefs().getBoolean(getString(R.string.dark_interface_key), false);
    }

    protected SharedPreferences getPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    protected GPSHelper getGpsHelper() {
        if (gpsHelper == null)
            gpsHelper = getGpsSingleton().getHelper();
        return gpsHelper;
    }

    protected LocationManager getManager() {
        if (manager == null)
            manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return manager;
    }

    protected boolean isGpsEnabled() {
        return getPrefs().getBoolean(getString(R.string.gps_enabled), true) &&
                getManager().isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    protected boolean isGpsEnabledInBg() {
        return getPrefs().getBoolean(getString(R.string.enable_bg_gps_key), false);
    }

    protected SQLiteOpenHelper getDatabaseHelper() {
        if (databaseHelper == null)
            databaseHelper = getDatabaseSingleton().getDbHelper();
        return databaseHelper;
    }

    protected DatabaseHelperSingleton getDatabaseSingleton() {
        if (databaseSingleton == null)
            databaseSingleton = DatabaseHelperSingleton.getInstance(getContext());
        return databaseSingleton;
    }

    protected SightingsDAO getSightingsDAO() {
        if (sightingsDAO == null)
            sightingsDAO = new SightingsDAO(getDatabaseHelper());
        return sightingsDAO;
    }

    protected GPSHelperSingleton getGpsSingleton() {
        if (gpsSingleton == null)
            gpsSingleton = GPSHelperSingleton.getInstance(this, shouldStartAutoOffTask());
        return gpsSingleton;
    }

    protected boolean shouldStartAutoOffTask() {
        return getPrefs().getBoolean(getString(R.string.gps_auto_off_key), false);
    }

    protected void resetGpsTask() {
        if (shouldStartAutoOffTask()) {
            getGpsSingleton().resetGpsAutoOffTask();
        }
    }

    protected PowerManager getPowerManager() {
        if (powerManager == null)
            powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        return powerManager;
    }

    protected PowerManager.WakeLock getWakeLock() {
        if (wakeLock == null)
            wakeLock = getPowerManager().newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, TAG);
        return wakeLock;
    }

    protected AppDataSingleton getAppDataSingleton() {
        if (appDataSingleton == null) {
            try {
                appDataSingleton = AppDataSingleton.getInstance(getContext());
            } catch (JSONException e) {
                Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
        return appDataSingleton;
    }
}
