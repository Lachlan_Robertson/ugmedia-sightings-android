package au.com.ugmedia.sightings;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.ugmedia.sightings.model.adapter.SpeciesCompletionAdapter;
import au.com.ugmedia.sightings.model.adapter.SurveyMethodCompletionAdapter;
import au.com.ugmedia.sightings.model.util.SightingsMetadata;
import au.com.ugmedia.sightings.model.util.UTMLocation;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;
import au.com.ugmedia.sightings.view.AutoSpeciesTextView;
import au.com.ugmedia.sightings.view.AutoSurveyMethodTextView;

/**
 * An abstract activity that implements functionality used when a user needs to modify or add a
 * sighting.
 */
public abstract class SightingsActivity extends DarkThemeActivity implements OnMapReadyCallback {
    private static final String TAG = SightingsActivity.class.getSimpleName();

    protected TextView timestampView;
    protected TextView commonNameView;
    protected TextView altitudeView;
    protected TextView accuracyView;
    protected Switch photoView;
    protected Switch healthView;
    protected EditText notesView;
    protected EditText siteNameView;
    protected EditText distanceView;
    protected EditText latitudeView;
    protected EditText longitudeView;
    protected EditText quantityView;
    protected AutoSpeciesTextView speciesAutoCompleteView;
    protected AutoSurveyMethodTextView surveyMethodAutoCompleteView;
    protected MapFragment mapFragment;
    protected GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
    }

    // -- END: Lifecycle methods --

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(getMenuLayoutId(), menu);
        return true;
    }

    // -- END: Menu methods --

    /**
     * An abstract method that allows other activities to define the Views used in this activity's
     * methods.
     */
    protected abstract void initGui();

    /**
     * Creates a sighting from data on the GUI.
     * <br>
     * Note that if location data is not available, default data will be used instead.
     *
     * @return A JSONObject containing all data about the sighting
     */
    protected final JSONObject createSightingFromGuiData() {
        JSONObject createdSighting = new JSONObject();
        UTMLocation location = new UTMLocation();

        String timestamp = timestampView.getText().toString().substring(getString(R.string.timestamp).length());
        String scientificName = speciesAutoCompleteView.getText().toString();
        String commonName = commonNameView.getText().toString().substring(getString(R.string.common_name).length());
        String quantity = quantityView.getText().toString();
        String photoTaken = photoView.isChecked() ? "Photo" : "No Photo";
        String health = healthView.isChecked() ? "Alive" : "Not Alive";
        String notes = notesView.getText().toString();
        String siteName = siteNameView.getText().toString();
        String surveyMethod = surveyMethodAutoCompleteView.getText().toString();
        String distance = distanceView.getText().toString();

        String latitude = latitudeView.getText().toString();
        String longitude = longitudeView.getText().toString();
        String altitude = altitudeView.getText().toString().substring(0, altitudeView.getText().toString().length() - 1); //To remove trailing "m"
        String accuracy = accuracyView.getText().toString().substring(0, accuracyView.getText().toString().length() - 1); //To remove trailing "m"
        location.setLatitude(latitude.equals("") ? UTMLocation.INVALID_VALUE_FLAG : Double.parseDouble(latitude));
        location.setLongitude(longitude.equals("") ? UTMLocation.INVALID_VALUE_FLAG : Double.parseDouble(longitude));
        location.setAltitude(altitude.equals("") ? UTMLocation.INVALID_VALUE_FLAG : Double.parseDouble(altitude));
        location.setAccuracy(accuracy.equals("") ? UTMLocation.INVALID_VALUE_FLAG : Double.parseDouble(accuracy));

        try {
            SightingsMetadata metadata = new SightingsMetadata(getContext());
            JSONObject data = new JSONObject();
            JSONObject locationData = new JSONObject();
            JSONObject metadataStorage = new JSONObject();

            metadataStorage.put(SightingsKeys.DATA_METADATA_DEVICE_ID, metadata.getDeviceId());
            metadataStorage.put(SightingsKeys.DATA_METADATA_DEVICE_NAME, metadata.getDeviceName());
            metadataStorage.put(SightingsKeys.DATA_METADATA_APPLICATION_ID, metadata.getAppId());
            metadataStorage.put(SightingsKeys.DATA_METADATA_GUID, metadata.getNewGUID());

            locationData.put(SightingsKeys.DATA_LOCATION_DISTANCE, distance);
            locationData.put(SightingsKeys.DATA_LOCATION_LATITUDE, location.getLatitude() == UTMLocation.INVALID_VALUE_FLAG ? "" : location.getLatitude());
            locationData.put(SightingsKeys.DATA_LOCATION_LONGITUDE, location.getLongitude() == UTMLocation.INVALID_VALUE_FLAG ? "" : location.getLongitude());
            locationData.put(SightingsKeys.DATA_LOCATION_ALTITUDE, location.getAltitude() == UTMLocation.INVALID_VALUE_FLAG ? "" : location.getAltitude());
            locationData.put(SightingsKeys.DATA_LOCATION_ACCURACY, location.getAccuracy() == UTMLocation.INVALID_VALUE_FLAG ? "" : location.getAccuracy());
            locationData.put(SightingsKeys.DATA_LOCATION_EASTING, location.getEasting() == UTMLocation.INVALID_VALUE_FLAG ? "" : location.getEasting());
            locationData.put(SightingsKeys.DATA_LOCATION_NORTHING, location.getNorthing() == UTMLocation.INVALID_VALUE_FLAG ? "" : location.getNorthing());
            locationData.put(SightingsKeys.DATA_LOCATION_HEMISPHERE, location.getHemisphere());
            locationData.put(SightingsKeys.DATA_LOCATION_ZONE, location.getZone() == UTMLocation.INVALID_VALUE_FLAG ? "" : location.getZoneAsString());

            data.put(SightingsKeys.DATA_METADATA, metadataStorage);
            data.put(SightingsKeys.DATA_LOCATION, locationData);
            data.put(SightingsKeys.DATA_SCIENTIFIC_NAME, scientificName);
            data.put(SightingsKeys.DATA_COMMON_NAME, commonName);
            data.put(SightingsKeys.DATA_QUANTITY, quantity);
            data.put(SightingsKeys.DATA_PHOTO_TAKEN, photoTaken);
            data.put(SightingsKeys.DATA_HEALTH, health);
            data.put(SightingsKeys.DATA_NOTES, notes);
            data.put(SightingsKeys.DATA_SITE_NAME, siteName);
            data.put(SightingsKeys.DATA_SURVEY_METHOD, surveyMethod);

            createdSighting.put(SightingsKeys.DATA, data);
            createdSighting.put(SightingsKeys.TIMESTAMP, timestamp);
        } catch (PackageManager.NameNotFoundException | JSONException e) {
            Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return createdSighting;
    }

    /**
     * Resets the GPS auto-off task and, if the GPS is enabled, updates GUI elements with the most
     * recent location data.
     *
     * @param view The View that called this method
     */
    public void refreshCoordinates(View view) {
        resetGpsTask();
        if (isGpsEnabled()) {
            updateLocationElements(getGpsHelper().getLocation());
        }
    }

    /**
     * Updates GUI elements with location data from the GPS; including the map.
     * <br>
     * Note that if the location is null, no GUI elements will be affected.
     *
     * @param location The current location to get data from
     */
    protected void updateLocationElements(Location location) {
        if (location != null) {
            longitudeView.setText(String.valueOf(location.getLongitude()));
            latitudeView.setText(String.valueOf(location.getLatitude()));
            altitudeView.setText(String.valueOf(location.getAltitude()) + "m");
            accuracyView.setText(String.valueOf(location.getAccuracy()) + "m");

            if (map != null) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(latLng, 10);

                map.clear();
                map.addMarker(new MarkerOptions().position(latLng));
                map.animateCamera(camera);
            }
        }
    }

    /**
     * Sets up adapters with correct GUI layouts and data and assigns them to their respective
     * Views. Also sets up adapters to have listeners if needed.
     */
    protected void setupAdapters() {
        SpeciesCompletionAdapter speciesAdapter = new SpeciesCompletionAdapter(getContext(), R.layout.list_item_species_dropdown, getAppDataSingleton().getSpecies());
        SurveyMethodCompletionAdapter surveyMethodAdapter = new SurveyMethodCompletionAdapter(getContext(), R.layout.list_item_survey_method_dropdown, getAppDataSingleton().getSurveyMethods());
        speciesAutoCompleteView.setAdapter(speciesAdapter);
        surveyMethodAutoCompleteView.setAdapter(surveyMethodAdapter);
        setOnItemClickListenerForAdapter(speciesAdapter);
    }

    /**
     * Checks against a set of predefined criteria to determine if a sighting is valid.
     *
     * @param sighting The sighting to validate
     * @return true if the sighting passes all tests, false otherwise
     * @throws JSONException If the sighting data is malformed or missing
     */
    protected boolean isValidSighting(JSONObject sighting) throws JSONException {
        boolean isValid = true;
        JSONObject data = new JSONObject(sighting.get(SightingsKeys.DATA).toString());

        if (data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME) == null || data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME).equals("")) {
            isValid = false;
        }

        return isValid;
    }

    /**
     * Instantiates the GoogleMap and sets it to a class variable that can be references in other
     * methods.
     *
     * @param googleMap The newly instantiated GoogleMap.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    /**
     * Sets an onClick listener for the species adapter to set the value of the common name GUI
     * element when a name is selected from the autocomplete drop down menu.
     *
     * @param speciesAdapter The adapter to set the listener for
     */
    private void setOnItemClickListenerForAdapter(final SpeciesCompletionAdapter speciesAdapter) {
        speciesAutoCompleteView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    commonNameView.setText(getString(R.string.common_name) + speciesAdapter.getCommonName(position));
                } catch (JSONException e) {
                    Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
    }

    // --- BEGIN: Getters and Setters ---
    protected abstract int getLayoutResId();

    protected abstract int getMenuLayoutId();
}
