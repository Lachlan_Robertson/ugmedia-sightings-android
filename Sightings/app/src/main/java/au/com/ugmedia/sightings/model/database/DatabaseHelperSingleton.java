package au.com.ugmedia.sightings.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * A Singleton responsible for managing database helper instances.
 * <br>
 * By limiting the number of instances to a single instance many database connectivity issues can
 * be avoided.
 * <br>
 * Class was originally sourced from:
 * http://stackoverflow.com/questions/23387405/android-database-cannot-perform-this-operation-because-the-connection-pool-has
 */
public class DatabaseHelperSingleton {
    private static DatabaseHelperSingleton instance;
    private static SQLiteOpenHelper dbHelper;

    /**
     * Constructor.
     * <br>
     * Private as per singleton pattern
     *
     * @param context The Context used to setup the database helper.
     */
    private DatabaseHelperSingleton(Context context) {
        dbHelper = new SightingsAssetHelper(context);
    }

    /**
     * Gets an instance of this class or creates a new instance if it does not already exist.
     *
     * @param context The Context used to setup the database helper.
     * @return An instance of this class
     */
    public static synchronized DatabaseHelperSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelperSingleton(context);
        }

        return instance;
    }

    /**
     * Returns the database helper maintained by this singleton.
     * <br>
     * As only one instance can ever exist, this ensures there will be no database connectivity issues.
     *
     * @return The database helper held by this singleton
     */
    public SQLiteOpenHelper getDbHelper() {
        return dbHelper;
    }
}
