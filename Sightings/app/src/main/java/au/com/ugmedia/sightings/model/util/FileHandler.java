package au.com.ugmedia.sightings.model.util;

import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import au.com.ugmedia.sightings.model.database.SpeciesDAO;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;
import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;

/**
 * A class responsible for handling files and storing them on the device so that they can be used
 * later (such as sent through an email as an attachment).
 */
public class FileHandler {
    private static final String TAG = FileHandler.class.getSimpleName();

    public static final int DEFAULT_FILE_KEEP_LIMIT = 30;

    private static final String CSV_EXTENSION = ".csv";
    private static final String GPX_EXTENSION = ".gpx";
    private static final String KML_EXTENSION = ".kml";
    private static final String CSV_HEADER = "Group,Scientific Name,Common Name,Timestamp,Quantity," +
            "Site Name,Survey Method,Distance,Photo,Health,Latitude,Longitude,Altitude,Accuracy," +
            "Easting,Northing,Hemisphere,Zone,Comments,Device ID,Device Name,GUID,App ID";
    private static final String GPX_HEADER =
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                    "<gpx\n" +
                    "\tversion=\"1.1\"\n" +
                    "\tcreator=\"Global Mapper - http://www.globalmapper.com\"\n" +
                    "\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                    "\txmlns=\"http://www.topografix.com/GPX/1/1\"\n" +
                    "\txsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">";
    private static final String KML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<kml xmlns=\"http://www.opengis.net/kml/2.2\">";
    private static final String NEW_LINE_SEPARATOR = "\n";

    private File destDir;
    private String baseFilename;
    private long fileKeepLimit;
    private SpeciesDAO speciesDAO;
    private SQLiteOpenHelper databaseHelper;

    /**
     * Default constructor using default data.
     * <br>
     * As this initialises the database helper to null, care must be taken not to use methods that
     * require database data.
     */
    public FileHandler() {
        this(DEFAULT_FILE_KEEP_LIMIT, null);
    }

    /**
     * Constructor.
     *
     * @param fileKeepLimit  The number of files to keep on the user's system
     * @param databaseHelper The database helper, required for querying and stored database data
     */
    public FileHandler(long fileKeepLimit, SQLiteOpenHelper databaseHelper) {
        destDir = new File(Environment.getExternalStorageDirectory() + "/Sightings");
        this.fileKeepLimit = fileKeepLimit;
        this.databaseHelper = databaseHelper;
    }

    /**
     * Creates CSV, GPX and KML files from database data at a predefined location with dynamically
     * generated names.
     * <br>
     * Note that not all files may be generated depending on the parameters but a CSV file will
     * always be created.
     *
     * @param sightings       The sightings to be transformed into various files
     * @param generateGpxFile Whether or not to also generate a GPX file
     * @param generateKmlFile Whether or not to also generate a KML file
     * @return An ArrayList of recently created filenames.
     */
    public ArrayList<String> createFiles(ArrayList<JSONObject> sightings, boolean generateGpxFile, boolean generateKmlFile) {
        ArrayList<String> fileNames = new ArrayList<>();

        if (sightings != null) {
            baseFilename = destDir.toString() + "/sightings-data-" + (new Date().getTime());
            fileNames.add(createCsvFile(sightings));
            if (generateGpxFile) {
                fileNames.add(createGpxFile(sightings));
            }
            if (generateKmlFile) {
                fileNames.add(createKmlFile(sightings));
            }
        }

        return fileNames;
    }

    /**
     * Creates a CSV file with the given data and predefined header.
     *
     * @param sightings The sightings to convert to a CSV file
     * @return The name of the created CSV file
     */
    private String createCsvFile(ArrayList<JSONObject> sightings) {
        String csvFilename = baseFilename + CSV_EXTENSION;
        deleteOldFiles(CSV_EXTENSION);
        try {
            FileWriter writer = new FileWriter(csvFilename);
            writer.append(CSV_HEADER);
            writer.append(NEW_LINE_SEPARATOR);

            for (JSONObject sighting : sightings) {
                JSONObject data = new JSONObject(sighting.getString(SightingsKeys.DATA));
                JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));
                JSONObject metadata = new JSONObject(data.getString(SightingsKeys.DATA_METADATA));

                String scientificName = data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
                String commonName = data.getString(SightingsKeys.DATA_COMMON_NAME);
                String timestamp = sighting.getString(SightingsKeys.TIMESTAMP);
                String quantity = data.getString(SightingsKeys.DATA_QUANTITY);
                String siteName = data.getString(SightingsKeys.DATA_SITE_NAME);
                String surveyMethod = data.getString(SightingsKeys.DATA_SURVEY_METHOD);
                String distance = location.getString(SightingsKeys.DATA_LOCATION_DISTANCE);
                String photo = data.getString(SightingsKeys.DATA_PHOTO_TAKEN);
                String health = data.getString(SightingsKeys.DATA_HEALTH);
                String latitude = location.getString(SightingsKeys.DATA_LOCATION_LATITUDE);
                String longitude = location.getString(SightingsKeys.DATA_LOCATION_LONGITUDE);
                String altitude = location.getString(SightingsKeys.DATA_LOCATION_ALTITUDE);
                String accuracy = location.getString(SightingsKeys.DATA_LOCATION_ACCURACY);
                String easting = location.getString(SightingsKeys.DATA_LOCATION_EASTING);
                String northing = location.getString(SightingsKeys.DATA_LOCATION_NORTHING);
                String hemisphere = location.getString(SightingsKeys.DATA_LOCATION_HEMISPHERE);
                String zone = location.getString(SightingsKeys.DATA_LOCATION_ZONE);
                String comments = data.getString(SightingsKeys.DATA_NOTES);
                String deviceId = metadata.getString(SightingsKeys.DATA_METADATA_DEVICE_ID);
                String deviceName = metadata.getString(SightingsKeys.DATA_METADATA_DEVICE_NAME);
                String guid = metadata.getString(SightingsKeys.DATA_METADATA_GUID);
                String appId = metadata.getString(SightingsKeys.DATA_METADATA_APPLICATION_ID);
                String group = getSpeciesDAO().getGroup(scientificName);

                writer.append(group).append(",")
                        .append(scientificName).append(",")
                        .append(commonName).append(",")
                        .append(timestamp).append(",")
                        .append(quantity).append(",")
                        .append(siteName).append(",")
                        .append(surveyMethod).append(",")
                        .append(distance).append(",")
                        .append(photo).append(",")
                        .append(health).append(",")
                        .append(latitude).append(",")
                        .append(longitude).append(",")
                        .append(altitude).append(",")
                        .append(accuracy).append(",")
                        .append(easting).append(",")
                        .append(northing).append(",")
                        .append(hemisphere).append(",")
                        .append(zone).append(",")
                        .append(comments).append(",")
                        .append(deviceId).append(",")
                        .append(deviceName).append(",")
                        .append(guid).append(",")
                        .append(appId).append(NEW_LINE_SEPARATOR);
            }

            writer.flush();
            writer.close();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return csvFilename;
    }

    /**
     * Creates a GPX file with the given data and predefined header and additional data.
     *
     * @param sightings The sightings to convert to a GPX file
     * @return The name of the created GPX file
     */
    private String createGpxFile(ArrayList<JSONObject> sightings) {
        String gpxFilename = baseFilename + GPX_EXTENSION;
        deleteOldFiles(GPX_EXTENSION);

        try {
            FileWriter writer = new FileWriter(gpxFilename);
            writer.append(GPX_HEADER).append(NEW_LINE_SEPARATOR);
            writer.append("<metadata><name>").append("sightings-data-").append(new Date().toString()).append("</name></metadata>").append(NEW_LINE_SEPARATOR);

            for (JSONObject sighting : sightings) {
                JSONObject data = new JSONObject(sighting.getString(SightingsKeys.DATA));
                JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));

                String scientificName = data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
                String description = data.getString(SightingsKeys.DATA_NOTES);
                String latitude = location.getString(SightingsKeys.DATA_LOCATION_LATITUDE);
                String longitude = location.getString(SightingsKeys.DATA_LOCATION_LONGITUDE);

                writer.append("<wpt ")
                        .append("lat=\"").append(latitude).append("\" ")
                        .append("lon=\"").append(longitude).append("\" ")
                        .append(">")
                        .append(NEW_LINE_SEPARATOR)
                        .append("<name>").append(scientificName).append("</name>").append(NEW_LINE_SEPARATOR)
                        .append("<desc>").append(description).append("</desc>").append(NEW_LINE_SEPARATOR)
                        .append("<sym>Dot</sym>").append(NEW_LINE_SEPARATOR)
                        .append("</wpt>");
                writer.append(NEW_LINE_SEPARATOR);
            }
            writer.append("</gpx>");

            writer.flush();
            writer.close();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return gpxFilename;
    }

    /**
     * Creates a KML file with the given data and predefined header and additional data.
     *
     * @param sightings The sightings to convert to a KML file
     * @return The name of the created KML file
     */
    private String createKmlFile(ArrayList<JSONObject> sightings) {
        String kmlFilename = baseFilename + KML_EXTENSION;
        deleteOldFiles(KML_EXTENSION);

        try {
            FileWriter writer = new FileWriter(kmlFilename);
            writer.append(KML_HEADER)
                    .append("<Document>")
                    .append("<name>Sightings App Overlay</name>")
                    .append("<description>Sightings App Data</description>").append(NEW_LINE_SEPARATOR);

            for (JSONObject sighting : sightings) {
                JSONObject data = new JSONObject(sighting.getString(SightingsKeys.DATA));
                JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));

                String scientificName = data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
                String description = data.getString(SightingsKeys.DATA_NOTES);
                String latitude = location.getString(SightingsKeys.DATA_LOCATION_LATITUDE);
                String longitude = location.getString(SightingsKeys.DATA_LOCATION_LONGITUDE);

                writer.append("<Placemark>").append(NEW_LINE_SEPARATOR)
                        .append("<name>").append(scientificName).append("</name>").append(NEW_LINE_SEPARATOR)
                        .append("<description>").append(description).append("</description>").append(NEW_LINE_SEPARATOR)
                        .append("<Point>").append(NEW_LINE_SEPARATOR)
                        .append("<coordinates>").append(longitude).append(",").append(latitude).append("</coordinates>").append(NEW_LINE_SEPARATOR)
                        .append("</Point>").append(NEW_LINE_SEPARATOR)
                        .append("</Placemark>").append(NEW_LINE_SEPARATOR);
            }
            writer.append("</Document>").append(NEW_LINE_SEPARATOR).append("</kml>");

            writer.flush();
            writer.close();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return kmlFilename;
    }

    /**
     * Removes old files from a predefined directory with a given extension until only the number
     * of files specified by the user remain.
     * Also creates the directory if it does not exist.
     *
     * @param fileExtension
     */
    private void deleteOldFiles(final String fileExtension) {
        if (!destDir.exists()) {
            destDir.mkdir();
            //No dir = no files, no need to check to delete...
        } else {
            File[] files = destDir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    return filename.toLowerCase().endsWith(fileExtension);
                }
            });

            if (files != null) {
                while (files.length >= fileKeepLimit) {
                    deleteOldestFile(files);
                    //Requery to find number of files
                    files = destDir.listFiles(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String filename) {
                            return filename.toLowerCase().endsWith(fileExtension);
                        }
                    });
                }
            }
        }
    }

    /**
     * Removes the single oldest file from a list of files.
     * <br>
     * Note that the file is considered the oldest if it's lastModified time is the oldest.
     *
     * @param filesInDir An array of files to search through
     */
    private void deleteOldestFile(File[] filesInDir) {
        long olderFileModifiedTime = System.currentTimeMillis(); //Ensures we always use now
        File fileToDelete = null;

        if (filesInDir != null) {
            for (File currentFile : filesInDir) {
                //If current file is older than previous file
                if (currentFile.lastModified() < olderFileModifiedTime) {
                    fileToDelete = currentFile;
                    olderFileModifiedTime = currentFile.lastModified();
                }
            }
        }

        if (fileToDelete != null) {
            fileToDelete.delete();
        }
    }

    // --- Import methods ---

    /**
     * 'Parses' a list of species from a given InputStream into an ArrayList of Species objects
     * containing the original data from the stream plus several default values.
     *
     * @param is The stream containing the relevant data
     * @return An ArrayList of Species object created from the stream's data
     */
    public ArrayList<JSONObject> getSpeciesFromStream(InputStream is) throws JSONException {
        ArrayList<JSONObject> speciesRecords = new ArrayList<>();
        BufferedReader reader;
        String line;

        try {
            reader = new BufferedReader(new InputStreamReader(is));
            reader.readLine(); //Skip the header

            while ((line = reader.readLine()) != null) {
                String[] speciesAttributes = line.split(",");
                if (speciesAttributes.length != 6) {
                    continue;
                }

                JSONObject species = new JSONObject();
                JSONObject data = new JSONObject();
                JSONObject metadata = new JSONObject();

                //Generated values
                int useCount = 0;
                String lastUpdated = SimpleDateFormat.getDateTimeInstance().format(new Date());
                String builtIn = "false";

                //Values from data
                String group = speciesAttributes[0];
                String geography = speciesAttributes[1];
                String genusName = speciesAttributes[2];
                String scientificName = speciesAttributes[3];
                String commonName = speciesAttributes[4];
                String italicise = speciesAttributes[5].equals("1") ? "true" : "false";

                metadata.put(SpeciesKeys.DATA_METADATA_LAST_UPDATED, lastUpdated);
                metadata.put(SpeciesKeys.DATA_METADATA_ITALICISE, italicise);
                metadata.put(SpeciesKeys.DATA_METADATA_BUILT_IN, builtIn);

                data.put(SpeciesKeys.DATA_GROUP, group);
                data.put(SpeciesKeys.DATA_GEOGRAPHY, geography);
                data.put(SpeciesKeys.DATA_GENUS_NAME, genusName);
                data.put(SpeciesKeys.DATA_COMMON_NAME, commonName);
                data.put(SpeciesKeys.DATA_METADATA, metadata);

                species.put(SpeciesKeys.SCIENTIFIC_NAME, scientificName);
                species.put(SpeciesKeys.USE_COUNT, useCount);
                species.put(SpeciesKeys.DATA, data);

                speciesRecords.add(species);
            }
        } catch (IOException e) {
            Log.e(TAG, "Couldn't read data from InputStream! Stream may be corrupt.");
            return null;
        }

        return speciesRecords;
    }

    /**
     * Returns the species table helper.
     *
     * @return The helper for the species table in the database
     */
    private SpeciesDAO getSpeciesDAO() {
        if (speciesDAO == null)
            speciesDAO = new SpeciesDAO(databaseHelper);
        return speciesDAO;
    }
}