package au.com.ugmedia.sightings.view;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import au.com.ugmedia.sightings.R;

/**
 * A custom preference that contains basic text and a button that is only trigger when the button
 * itself is clicked on (not the entire preference).
 */
public class DeleteAllPreference extends Preference {
    private static final String TAG = DeleteAllPreference.class.getSimpleName();

    public DeleteAllPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DeleteAllPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DeleteAllPreference(Context context) {
        super(context);
    }

    /**
     * Finds and inflates the custom view for this preference.
     *
     * @param parent The parent of this View
     * @return The newly inflated view
     */
    @Override
    protected View onCreateView(ViewGroup parent) {
        super.onCreateView(parent);
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return li.inflate(R.layout.preference_delete_all, parent, false);
    }
}
