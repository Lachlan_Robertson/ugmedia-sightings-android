package au.com.ugmedia.sightings.model.util.gps;

import android.location.Location;

/**
 * An interface that works to listen for updates to GPS location data retrieved from a class implementing GPSSubject.
 * <p/>
 * {@see GPSSubject}
 */
public interface GPSListener {
    void onLocationAvailable(Location location);
}
