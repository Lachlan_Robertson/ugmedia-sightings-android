package au.com.ugmedia.sightings.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;

/**
 * An extension of Android's AutoCompleteTextView that makes subtle changes such as a change in
 * threshold (when the completion list is shown) and what to display in the textbox when a suggestion
 * is chosen.
 */
public class AutoSpeciesTextView extends AutoCompleteTextView {
    private static final String TAG = AutoSpeciesTextView.class.getSimpleName();

    private static final int THRESHOLD = 1; //Number of characters required before beginning autocomplete

    /**
     * Constructor
     *
     * @param context The context of the activity that instantiates this object
     */
    public AutoSpeciesTextView(Context context) {
        super(context);
        setThreshold(THRESHOLD);
    }

    /**
     * Constructor
     *
     * @param context The context of the activity that instantiates this object
     * @param attrs   A set of attributes to specify style
     */
    public AutoSpeciesTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setThreshold(THRESHOLD);
    }

    /**
     * Converts an object to it's String value which is then used to populate the associated textbox.
     * This implementation simply returns the Species object's scientific name attribute.
     *
     * @param selectedItem The selected object from the ListView
     * @return The scientific name of the Species object
     */
    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        JSONObject species = (JSONObject) selectedItem;
        String returnText = "ERROR";

        try {
            returnText = species.getString(SpeciesKeys.SCIENTIFIC_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return returnText;
    }
}