package au.com.ugmedia.sightings.model.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import java.util.UUID;

/**
 * A convenience class that contains the metadata required by a sighting.
 */
public class SightingsMetadata {
    private String deviceId;
    private String deviceName;
    private String appId;

    public SightingsMetadata() {
    }

    public SightingsMetadata(Context context) throws PackageManager.NameNotFoundException {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

        deviceId = manager.getDeviceId();
        deviceName = Build.MODEL;
        appId = String.format("%s-%s(%s)", context.getPackageName(), packageInfo.versionName, packageInfo.versionCode);
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getAppId() {
        return appId;
    }

    public String getNewGUID() {
        //To ensure the UUID is always unique it must be generated 'on the fly'
        return UUID.randomUUID().toString();
    }
}
