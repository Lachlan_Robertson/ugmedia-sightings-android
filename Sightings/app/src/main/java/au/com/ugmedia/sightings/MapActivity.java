package au.com.ugmedia.sightings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;

/**
 * An activity that displays a map of all sightings form the database and InfoWindows that allow a
 * user to click on them and edit the sighting in another activity.
 */
public class MapActivity extends DarkThemeActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
    private static final String TAG = MapActivity.class.getSimpleName();

    private static final String TITLE_ID_DELIMITER = ".";

    private MapFragment mapFragment;
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initGui();
    }

    /**
     * Resumes the activity and re-plots the sightings on the map, ensure data is always up to date.
     */
    @Override
    protected void onResume() {
        super.onResume();
        plotSightingsOnMap();
    }

    /**
     * Sets up the GUI elements and requests a new GoogleMap instance.
     */
    private void initGui() {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.full_map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Instantiates a map instance, sets up click listeners and plots points of all sightings on
     * the map.
     *
     * @param googleMap The newly instantiated GoogleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnInfoWindowClickListener(this);
        plotSightingsOnMap();
    }

    /**
     * Plots all sightings from the database onto the map, focusing the map 'camera' on the most
     * recent point.
     * <br>
     * Note that if a sighting does not have a valid position it will not be plotted on the map.
     */
    private void plotSightingsOnMap() {
        if (map != null) {
            try {
                CameraUpdate camera = null;
                ArrayList<JSONObject> sightings = getAppDataSingleton().getSightings();
                for (JSONObject sighting : sightings) {
                    JSONObject data = new JSONObject(sighting.getString(SightingsKeys.DATA));
                    JSONObject location = new JSONObject(data.getString(SightingsKeys.DATA_LOCATION));

                    try {
                        String title = sighting.getString(SightingsKeys.ID) + TITLE_ID_DELIMITER + " " +
                                data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
                        String snippet = getString(R.string.click_to_edit);
                        double lat = Double.parseDouble(location.getString(SightingsKeys.DATA_LOCATION_LATITUDE));
                        double lng = Double.parseDouble(location.getString(SightingsKeys.DATA_LOCATION_LONGITUDE));
                        LatLng latLng = new LatLng(lat, lng);
                        camera = CameraUpdateFactory.newLatLngZoom(latLng, 5);

                        map.addMarker(new MarkerOptions().position(latLng)
                                .title(title)
                                .snippet(snippet));
                    } catch (NumberFormatException e) {
                    }
                }
                if (camera != null) {
                    map.animateCamera(camera);
                }
            } catch (JSONException e) {
                Toast.makeText(this, R.string.toast_message_unexpected_error, Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets the ID from an InfoWindow and creates a new intent to allow the user to edit the data
     * pertaining to the selected sighting.
     *
     * @param marker The map marker associated with the clicked InfoWindow
     */
    @Override
    public void onInfoWindowClick(Marker marker) {
        resetGpsTask();
        String idAsString = marker.getTitle().substring(0, marker.getTitle().indexOf(TITLE_ID_DELIMITER));
        long id = Long.parseLong(idAsString);

        Intent intent = new Intent(this, EditSightingActivity.class);
        intent.putExtra(getString(R.string.intent_extra_id), id);
        startActivity(intent);
    }

    // -- BEGIN: Getters and Setters
    @Override
    protected Context getContext() {
        return this;
    }
}
