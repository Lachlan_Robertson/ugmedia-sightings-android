package au.com.ugmedia.sightings.model.util.jsonKeys;

/**
 * Keys for species JSONObjects
 */
public final class SpeciesKeys {
    public static final String ID = "_id";
    public static final String SCIENTIFIC_NAME = "Scientific_Name";
    public static final String USE_COUNT = "Use_Count";

    public static final String DATA = "Data";
    public static final String DATA_GROUP = "Group";
    public static final String DATA_GENUS_NAME = "Genus_Name";
    public static final String DATA_COMMON_NAME = "Common_Name";
    public static final String DATA_GEOGRAPHY = "Geography";

    public static final String DATA_METADATA = "Metadata";
    public static final String DATA_METADATA_LAST_UPDATED = "Last_Updated";
    public static final String DATA_METADATA_ITALICISE = "Italicise";
    public static final String DATA_METADATA_BUILT_IN = "Built_In";
}
