package au.com.ugmedia.sightings.model.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * A class responsible for database-wide interaction such as initialising the database schema or
 * updating it.
 */
public class SightingsAssetHelper extends SQLiteAssetHelper {
    private static final String TAG = SightingsAssetHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "sightings.db";
    private static final int DATABASE_VERSION = 1;

    /**
     * Constructor.
     *
     * @param context The context associated with this database
     */
    public SightingsAssetHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
