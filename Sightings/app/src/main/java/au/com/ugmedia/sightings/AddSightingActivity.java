package au.com.ugmedia.sightings;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.ugmedia.sightings.view.AutoSpeciesTextView;
import au.com.ugmedia.sightings.view.AutoSurveyMethodTextView;

/**
 * Activity that allows a user to add a new sighting.
 */
public class AddSightingActivity extends SightingsActivity {
    private static final String TAG = AddSightingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGui();
    }

    // ---- END: Lifecycle methods ----

    /**
     * Allows the user to save the sighting (adding it to the database if valid) or cancel out of
     * this activity and discard all changes.
     * <br>
     * The sighting is created from GUI data and validates under predefined conditions.
     *
     * @param item The selected menu item form the ActionBar
     * @return Whether or not to consume the menu event
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        resetGpsTask();
        int id = item.getItemId();

        if (id == R.id.action_save) {
            JSONObject sighting = createSightingFromGuiData();
            if (addSighting(sighting)) {
                Toast.makeText(this, getString(R.string.toast_message_added_sighting), Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (id == R.id.action_cancel) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    // ---- END: MenuBar methods ----

    /**
     * Initialises the GUI, setting up GUI elements, adapters for autocomplete views and the map.
     */
    protected void initGui() {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.add_map);
        timestampView = (TextView) findViewById(R.id.add_timestamp);
        commonNameView = (TextView) findViewById(R.id.add_common_name);
        quantityView = (EditText) findViewById(R.id.add_quantity);
        photoView = (Switch) findViewById(R.id.add_photo_taken_or_not);
        healthView = (Switch) findViewById(R.id.add_is_alive_or_dead);
        notesView = (EditText) findViewById(R.id.add_additional_notes);
        siteNameView = (EditText) findViewById(R.id.add_site_name);
        distanceView = (EditText) findViewById(R.id.add_distance);
        latitudeView = (EditText) findViewById(R.id.add_latitude_input);
        longitudeView = (EditText) findViewById(R.id.add_longitude_input);
        altitudeView = (TextView) findViewById(R.id.add_altitude);
        accuracyView = (TextView) findViewById(R.id.add_accuracy);
        speciesAutoCompleteView = (AutoSpeciesTextView) findViewById(R.id.add_scientific_name);
        surveyMethodAutoCompleteView = (AutoSurveyMethodTextView) findViewById(R.id.add_survey_method);

        setupAdapters();
        mapFragment.getMapAsync(this);
        timestampView.setText(getString(R.string.timestamp) + SimpleDateFormat.getDateTimeInstance().format(new Date()));
    }

    /**
     * Adds a sighting if it is considered valid or alerts the user if invalid.
     *
     * @param sighting The sighting to add.
     * @return true if the sighting was added, false otherwise
     */
    private boolean addSighting(JSONObject sighting) {
        boolean successful = true;

        try {
            if (isValidSighting(sighting)) {
                if (getAppDataSingleton().addSighting(sighting) == -1) {
                    successful = false;
                    Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                successful = false;
                Toast.makeText(getContext(), getString(R.string.toast_message_add_sighting_invalid_data), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            successful = false;
            Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return successful;
    }

    /**
     * When the map is ready for use, location data is refreshed and a new marker will be drawn on
     * the map.
     *
     * @param map The newly instantiated GoogleMap
     */
    @Override
    public void onMapReady(GoogleMap map) {
        super.onMapReady(map);
        refreshCoordinates(null);
    }

    // -- BEGIN: Getters and Setters
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_add_sighting;
    }

    @Override
    protected int getMenuLayoutId() {
        return R.menu.menu_add_sighting;
    }

    @Override
    protected Context getContext() {
        return this;
    }
}