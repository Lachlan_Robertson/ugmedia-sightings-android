package au.com.ugmedia.sightings.model.util;

/*
 * The original JavaScript code this class is based off and all original equations contained herein
 * are courtesy of "Chuck Taylor Toolbox", http://home.hiwaay.net/~taylorc/toolbox/geography/geoutm.html
 *
 * As per the original author's permission, this code may be "copied and reused without restriction."
 * Please check the aforementioned link for additional references.
 */
class UTMCalculator {
    private static final String TAG = UTMCalculator.class.getSimpleName();

    public static final int EASTING_INDEX = 0;
    public static final int NORTHING_INDEX = 1;

    //Ellipsoid model constants (actual values here are for WGS84)
    private double sm_a = 6378137.0;
    private double sm_b = 6356752.314;

    /**
     * Constructor.
     */
    public UTMCalculator() {
    }

    /**
     * Calculates and returns the easting/northing values for a particular lat/lng/zone group.
     *
     * @param lat  The latitude
     * @param lon  The longitude
     * @param zone The zone in which the lat/lng reside
     * @return The calculated easting/northing values
     */
    public double[] getEastingNorthing(double lat, double lon, double zone) {
        double UTM_SCALE_FACTOR = 0.9996;
        double[] eastingNorthing = convertLatLongToEastingNorthing(degToRad(lat), degToRad(lon), calcUTMCentralMeridian(zone));

        /* Adjust easting and northing for UTM system. */
        eastingNorthing[EASTING_INDEX] = eastingNorthing[EASTING_INDEX] * UTM_SCALE_FACTOR + 500000.0;
        eastingNorthing[NORTHING_INDEX] = eastingNorthing[NORTHING_INDEX] * UTM_SCALE_FACTOR;
        if (eastingNorthing[NORTHING_INDEX] < 0.0)
            eastingNorthing[NORTHING_INDEX] = eastingNorthing[NORTHING_INDEX] + 10000000.0;
        return eastingNorthing;
    }

    /**
     * Convenience method to convert degrees to radians.
     *
     * @param deg Degrees to convert
     * @return The radians equivalent of the given degrees
     */
    private double degToRad(double deg) {
        return (deg / 180.0 * Math.PI);
    }

    /**
     * Calculates the 'arc length' of the meridian.
     *
     * @param phi The value to determine arc length
     * @return The arc length of the meridian
     */
    private double calcArcLengthOfMeridian(double phi) {
        double result;

        double n = (sm_a - sm_b) / (sm_a + sm_b);
        double alpha = ((sm_a + sm_b) / 2.0) * (1.0 + (Math.pow(n, 2.0) / 4.0) + (Math.pow(n, 4.0) / 64.0));
        double beta = (-3.0 * n / 2.0) + (9.0 * Math.pow(n, 3.0) / 16.0) + (-3.0 * Math.pow(n, 5.0) / 32.0);
        double gamma = (15.0 * Math.pow(n, 2.0) / 16.0) + (-15.0 * Math.pow(n, 4.0) / 32.0);
        double delta = (-35.0 * Math.pow(n, 3.0) / 48.0) + (105.0 * Math.pow(n, 5.0) / 256.0);
        double epsilon = (315.0 * Math.pow(n, 4.0) / 512.0);

        result = alpha
                * (phi + (beta * Math.sin(2.0 * phi))
                + (gamma * Math.sin(4.0 * phi))
                + (delta * Math.sin(6.0 * phi))
                + (epsilon * Math.sin(8.0 * phi)));

        return result;
    }

    /**
     * Calculates the UTM central meridian.
     *
     * @param zone The zone for calculation
     * @return The central meridian
     */
    private double calcUTMCentralMeridian(double zone) {
        return degToRad(-183.0 + (zone * 6.0));
    }

    /**
     * Converts a lat/lng pairing to an easting/northing in UTM
     *
     * @param phi     phi
     * @param lambda  lambda
     * @param lambda0 lambda0
     * @return The easting/northing pairing
     */
    private double[] convertLatLongToEastingNorthing(double phi, double lambda, double lambda0) {
        double[] eastingNorthing = new double[2];

        double ep2 = (Math.pow(sm_a, 2.0) - Math.pow(sm_b, 2.0)) / Math.pow(sm_b, 2.0);
        double nu2 = ep2 * Math.pow(Math.cos(phi), 2.0);
        double N = Math.pow(sm_a, 2.0) / (sm_b * Math.sqrt(1 + nu2));
        double t = Math.tan(phi);
        double t2 = t * t;
        double l = lambda - lambda0;

        /*
         * Precalculate coefficients for l**n in the equations below so a normal human being can
         * read the expressions for easting and northing.
          * Note: l**1 and l**2 have coefficients of 1.0
         */
        double l3coef = 1.0 - t2 + nu2;
        double l4coef = 5.0 - t2 + 9 * nu2 + 4.0 * (nu2 * nu2);
        double l5coef = 5.0 - 18.0 * t2 + (t2 * t2) + 14.0 * nu2
                - 58.0 * t2 * nu2;
        double l6coef = 61.0 - 58.0 * t2 + (t2 * t2) + 270.0 * nu2
                - 330.0 * t2 * nu2;
        double l7coef = 61.0 - 479.0 * t2 + 179.0 * (t2 * t2) - (t2 * t2 * t2);
        double l8coef = 1385.0 - 3111.0 * t2 + 543.0 * (t2 * t2) - (t2 * t2 * t2);

        // Calculate easting (x)
        eastingNorthing[EASTING_INDEX] = N * Math.cos(phi) * l
                + (N / 6.0 * Math.pow(Math.cos(phi), 3.0) * l3coef * Math.pow(l, 3.0))
                + (N / 120.0 * Math.pow(Math.cos(phi), 5.0) * l5coef * Math.pow(l, 5.0))
                + (N / 5040.0 * Math.pow(Math.cos(phi), 7.0) * l7coef * Math.pow(l, 7.0));

        // Calculate northing (y)
        eastingNorthing[NORTHING_INDEX] = calcArcLengthOfMeridian(phi)
                + (t / 2.0 * N * Math.pow(Math.cos(phi), 2.0) * Math.pow(l, 2.0))
                + (t / 24.0 * N * Math.pow(Math.cos(phi), 4.0) * l4coef * Math.pow(l, 4.0))
                + (t / 720.0 * N * Math.pow(Math.cos(phi), 6.0) * l6coef * Math.pow(l, 6.0))
                + (t / 40320.0 * N * Math.pow(Math.cos(phi), 8.0) * l8coef * Math.pow(l, 8.0));

        return eastingNorthing;
    }
}
