package au.com.ugmedia.sightings.model.util.gps;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

/**
 * An application-wide singleton object that acts as a way to stop the GPS helper disconnecting
 * itself on each new activity. This means the GPS hardware will always remain in use (unless
 * toggled off) and will not lose signal under dense canopies, etc.
 */
public class GPSHelperSingleton {
    private static final String TAG = GPSHelperSingleton.class.getSimpleName();
    private static final long WAIT_TIME = 10 * 60 * 1000; // 10 minutes

    private static GPSHelper helper;
    private static GPSHelperSingleton instance;
    private static Handler handler;
    private static Runnable handlerTask;

    /**
     * Private constructor as per the singleton design pattern.
     * <br>
     * This constructor also sets up a task to 'disable' GPS requests after a certain amount of time
     * and starts it if the user has the correct settings.
     *
     * @param context The context needed to create a new GPSHelper.
     */
    private GPSHelperSingleton(Context context, boolean startTask) {
        if (helper == null) {
            helper = new GPSHelper(context);
        }

        handler = new Handler();
        initGpsAutoOffTask();
        if (startTask) {
            startGpsAutoOffTask();
        }
    }

    /**
     * Gets and returns an instance to this object attempting to set a new GPS helper.
     *
     * @return An instance of this object.
     */
    public static GPSHelperSingleton getInstance(Context context, boolean startTask) {
        if (instance == null) {
            instance = new GPSHelperSingleton(context, startTask);
        }

        return instance;
    }

    /**
     * Returns the GPS helper held by this singleton.
     *
     * @return The GPS helper of this singleton
     */
    public GPSHelper getHelper() {
        return helper;
    }

    /**
     * Sets up a task to disable GPS requests that will be run on another thread.
     */
    private void initGpsAutoOffTask() {
        handlerTask = new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "Turning off GPS.");
                getHelper().stopRequestingLocations();
            }
        };
    }

    /**
     * Starts a task to disable GPS requests after a amount of certain time.
     */
    public void startGpsAutoOffTask() {
        Log.i(TAG, "GPS auto-off task started.");
        handler.postDelayed(handlerTask, WAIT_TIME);
    }

    /**
     * Stops and removes the task to disable GPS requests; ensuring that the GPS will never
     * 'turn off'.
     */
    public void stopGpsAutoOffTask() {
        Log.i(TAG, "GPS auto-off task stopped.");
        handler.removeCallbacks(handlerTask);
    }

    /**
     * Refreshes the task to disable GPS requests, giving the user more time before the GPS will be
     * 'turned off',
     */
    public void resetGpsAutoOffTask() {
        stopGpsAutoOffTask();
        getHelper().startRequestingLocations();
        startGpsAutoOffTask();
    }
}