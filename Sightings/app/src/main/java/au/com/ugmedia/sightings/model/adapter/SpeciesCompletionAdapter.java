package au.com.ugmedia.sightings.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.R;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;
import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;

/**
 * An adapter made specifically for interacting with Species and AutoSpeciesTextView and
 * filtering by both scientific and common name so a user can enter either and enter the scientific
 * name of a species for form submission
 * <p/>
 * {@see AutoSpeciesTextView}
 */
public class SpeciesCompletionAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = SpeciesCompletionAdapter.class.getSimpleName();

    private Filter filter;
    private LayoutInflater inflater;
    private int resId;
    private ArrayList<JSONObject> originalSpecies;
    private ArrayList<JSONObject> filteredSpecies;

    /**
     * Constructor.
     *
     * @param context The context of the calling activity for the associated LayoutInflater
     * @param resId   The id of the layout file to use as a dropdown. This can be any layout as long
     *                as it contains two TextView objects named "list_item_species_scientific_name" and
     *                "list_item_species_common_name"
     * @param items   The ArrayList of Species objects associated with this adapter
     */
    public SpeciesCompletionAdapter(Context context, int resId, ArrayList<JSONObject> items) {
        super(context, resId, items);

        this.resId = resId;
        this.originalSpecies = new ArrayList<>(items);
        this.filteredSpecies = new ArrayList<>(items);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Gets the view associated with a particular instance in a ListView and populates it with data.
     * This particular version will populate an instance with a species' scientific and common names.
     *
     * @param position    The position in the ListView used to retrieve the correct data for this instance
     * @param convertView The view to be inflated and used in the ListView
     * @param parent      The parent of the view that will be used
     * @return A view populated with Species data
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                convertView = inflater.inflate(resId, parent, false);
            }

            JSONObject species = getItem(position);
            JSONObject data = new JSONObject(species.get(SpeciesKeys.DATA).toString());
            TextView sciNameView = (TextView) convertView.findViewById(R.id.list_item_species_scientific_name);
            TextView commonNameView = (TextView) convertView.findViewById(R.id.list_item_species_common_name);
            TextView useCountView = (TextView) convertView.findViewById(R.id.list_item_species_use_count);

            sciNameView.setText(species.getString(SpeciesKeys.SCIENTIFIC_NAME));
            commonNameView.setText(data.getString(SpeciesKeys.DATA_COMMON_NAME));
            useCountView.setText(getContext().getString(R.string.list_item_species_use_count, species.get(SpeciesKeys.USE_COUNT)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    /**
     * Gets the current size of the filtered array.
     *
     * @return The current size of the filteredSpecies array
     */
    @Override
    public int getCount() {
        return filteredSpecies.size();
    }

    /**
     * Returns a JSONObject containing species information from the filteredSpecies array based on
     * position.
     *
     * @param position The position to search for in the array
     * @return The JSONObject located at the given position
     */
    @Override
    public JSONObject getItem(int position) {
        return filteredSpecies.get(position);
    }

    public String getCommonName(int position) throws JSONException {
        JSONObject sighting = getItem(position);
        JSONObject data = new JSONObject(sighting.get(SightingsKeys.DATA).toString());
        return data.getString(SightingsKeys.DATA_COMMON_NAME);
    }

    /**
     * Returns the filter to be used when auto-completing the text.
     * If one does not exist it will be created.
     *
     * @return The Filter object used for filtering text
     */
    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new SpeciesFilter();
        return filter;
    }

    /**
     * A class used only for the SpeciesCompletionAdapter class.
     * <p/>
     * Provides a custom way to filter data by searching for the supplied text in the entirety of
     * both a Species object's scientific and common names
     */
    private class SpeciesFilter extends Filter {

        /**
         * The method responsible for the actual filtering of results.
         * <p/>
         * This implementation searches the entire species' scientific name and common name for a
         * match and returns any matches.
         *
         * @param constraint The text to search for
         * @return A FilterResult object containing species that matched the given criteria
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            String searchCondition = "";
            if (constraint != null) {
                searchCondition = constraint.toString().toLowerCase();
            }

            if (searchCondition.length() == 0) {
                ArrayList<JSONObject> speciesCopy = new ArrayList<>(originalSpecies);

                results.values = speciesCopy;
                results.count = speciesCopy.size();
            } else {
                ArrayList<JSONObject> speciesCopy = new ArrayList<>(originalSpecies);
                ArrayList<JSONObject> newSpecies = new ArrayList<>();

                try {
                    for (JSONObject currentSpecies : speciesCopy) {
                        JSONObject data = new JSONObject(currentSpecies.get(SpeciesKeys.DATA).toString());
                        String scientificName = currentSpecies.getString(SpeciesKeys.SCIENTIFIC_NAME);
                        String commonName = data.getString(SpeciesKeys.DATA_COMMON_NAME);

                        if (scientificName.toLowerCase().contains(searchCondition) || commonName.toLowerCase().contains(searchCondition)) {
                            newSpecies.add(currentSpecies);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                results.values = newSpecies;
                results.count = newSpecies.size();
            }
            return results;
        }

        /**
         * Sets the filteredSpecies ArrayList to contain the searched results and notifies all
         * listeners of the changed data if successful.
         * If no results are found, listeners are notified of an invalidated data set being returned.
         *
         * @param constraint The constraint used for filter publishing
         * @param results    The results containing an ArrayList of Species objects to be 'published'
         *                   to the filteredSpecies ArrayList
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                filteredSpecies.clear();
                filteredSpecies.addAll((ArrayList<JSONObject>) results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}