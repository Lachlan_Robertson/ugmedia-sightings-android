package au.com.ugmedia.sightings.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

/**
 * An extension of Android's AutoCompleteTextView that makes subtle changes such as a change in
 * threshold (when the completion list is shown) and what to display in the textbox when a suggestion
 * is chosen.
 */
public class AutoSurveyMethodTextView extends AutoCompleteTextView {
    private static final String TAG = AutoSurveyMethodTextView.class.getSimpleName();

    private static final int THRESHOLD = 1; //Number of characters required before beginning autocomplete

    /**
     * Constructor
     *
     * @param context The context of the activity that instantiates this object
     */
    public AutoSurveyMethodTextView(Context context) {
        super(context);
        setThreshold(THRESHOLD);
    }

    /**
     * Constructor
     *
     * @param context The context of the activity that instantiates this object
     * @param attrs   A set of attributes to specify style
     */
    public AutoSurveyMethodTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setThreshold(THRESHOLD);
    }

    /**
     * Converts an object to it's String value which is then used to populate the associated textbox.
     * This implementation simply returns the survey method's name.
     *
     * @param selectedItem The selected object from the ListView
     * @return The survey method's name
     */
    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        JSONObject surveyMethod = (JSONObject) selectedItem;
        String returnText = "ERROR";

        try {
            returnText = surveyMethod.getString(SurveyMethodKeys.METHOD_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return returnText;
    }
}
