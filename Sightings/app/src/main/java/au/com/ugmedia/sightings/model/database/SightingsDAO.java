package au.com.ugmedia.sightings.model.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import au.com.ugmedia.sightings.model.util.SightingsMetadata;
import au.com.ugmedia.sightings.model.util.UTMLocation;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;

/**
 * A Data Access Object that provides a simple way to make various requests to the Sightings table
 * in the database.
 */
public class SightingsDAO {
    private static final String TAG = SightingsDAO.class.getSimpleName();

    private static final String TABLE_NAME = "Sightings";
    public static final String ID_COLUMN = "_id";
    public static final String TIMESTAMP_COLUMN = "Timestamp";
    public static final String DATA_COLUMN = "Data";

    private SQLiteOpenHelper dbHelper;

    /**
     * Constructor.
     *
     * @param helper The helper required to actually access the database
     */
    public SightingsDAO(SQLiteOpenHelper helper) {
        dbHelper = helper;
    }

    /**
     * Adds a new sighting to the database.
     * <br>
     * If the sighting is null, no records will be added and no exceptions will be thrown. For this
     * reason you must check the return value to determine whether this method was successful or not.
     *
     * @param sighting The sighting to add
     * @return The inserted row id on success or -1 on failure
     * @throws JSONException If the sighting is malformed or missing data
     */
    public long addNewSighting(JSONObject sighting) throws JSONException {
        long insertedRowId = -1;
        if (sighting != null) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(TIMESTAMP_COLUMN, sighting.getString(SightingsKeys.TIMESTAMP));
            values.put(DATA_COLUMN, sighting.get(SightingsKeys.DATA).toString());

            insertedRowId = db.insert(TABLE_NAME, null, values);
        }

        return insertedRowId;
    }

    /**
     * Copies and inserts a sighting based on a sighting already stored in the database.
     * <br>
     * If the original sighting cannot be found, no location is provided or the metadata cannot does
     * not exist, no records will be added and no exceptions will be raised.
     * Clients must check the return value to determine if the method successfully duplicated or not.
     *
     * @param id       The _id of the original sighting to duplicate
     * @param l        The location of the current sighting
     * @param metadata The metadata associated with the current sighting
     * @return The inserted row id on success or -1 on failure
     * @throws JSONException If the sighting or metadata is malformed or missing
     */
    public long duplicateSighting(long id, UTMLocation l, SightingsMetadata metadata) throws JSONException {
        long insertedId = -1;
        JSONObject newRecord = getSighting(id);

        if (newRecord != null && l != null && metadata != null) {
            JSONObject newData = new JSONObject(newRecord.get(SightingsKeys.DATA).toString());
            JSONObject newLocationData = new JSONObject(newData.getString(SightingsKeys.DATA_LOCATION));
            JSONObject newMetadata = new JSONObject(newData.getString(SightingsKeys.DATA_METADATA));

            String timestamp = SimpleDateFormat.getDateTimeInstance().format(new Date());

            String latitude = l.getLatitude() == UTMLocation.INVALID_VALUE_FLAG ? "" : String.valueOf(l.getLatitude());
            String longitude = l.getLongitude() == UTMLocation.INVALID_VALUE_FLAG ? "" : String.valueOf(l.getLongitude());
            String altitude = l.getAltitude() == UTMLocation.INVALID_VALUE_FLAG ? "" : String.valueOf(l.getAltitude());
            String accuracy = l.getAccuracy() == UTMLocation.INVALID_VALUE_FLAG ? "" : String.valueOf(l.getAccuracy());
            String easting = l.getEasting() == UTMLocation.INVALID_VALUE_FLAG ? "" : String.valueOf(l.getEasting());
            String northing = l.getNorthing() == UTMLocation.INVALID_VALUE_FLAG ? "" : String.valueOf(l.getNorthing());
            String hemisphere = l.getHemisphere();
            String zone = l.getZone() == UTMLocation.INVALID_VALUE_FLAG ? "" : String.valueOf(l.getZoneAsString());

            String deviceId = metadata.getDeviceId();
            String deviceName = metadata.getDeviceName();
            String appId = metadata.getAppId();
            String guid = metadata.getNewGUID();

            newMetadata.put(SightingsKeys.DATA_METADATA_DEVICE_ID, deviceId);
            newMetadata.put(SightingsKeys.DATA_METADATA_DEVICE_NAME, deviceName);
            newMetadata.put(SightingsKeys.DATA_METADATA_APPLICATION_ID, appId);
            newMetadata.put(SightingsKeys.DATA_METADATA_GUID, guid);

            newLocationData.put(SightingsKeys.DATA_LOCATION_LATITUDE, latitude);
            newLocationData.put(SightingsKeys.DATA_LOCATION_LONGITUDE, longitude);
            newLocationData.put(SightingsKeys.DATA_LOCATION_ALTITUDE, altitude);
            newLocationData.put(SightingsKeys.DATA_LOCATION_ACCURACY, accuracy);
            newLocationData.put(SightingsKeys.DATA_LOCATION_EASTING, easting);
            newLocationData.put(SightingsKeys.DATA_LOCATION_NORTHING, northing);
            newLocationData.put(SightingsKeys.DATA_LOCATION_HEMISPHERE, hemisphere);
            newLocationData.put(SightingsKeys.DATA_LOCATION_ZONE, zone);

            newData.put(SightingsKeys.DATA_LOCATION, newLocationData.toString());
            newData.put(SightingsKeys.DATA_METADATA, newMetadata.toString());

            newRecord.put(SightingsKeys.DATA, newData);
            newRecord.put(SightingsKeys.TIMESTAMP, timestamp);

            insertedId = addNewSighting(newRecord);
        }

        return insertedId;
    }

    /**
     * Finds and returns a sighting with the given ID.
     *
     * @param id The _id of the sighting in the database
     * @return The sighting as a JSONObject or null if it cannot be found
     * @throws JSONException If the stored data is malformed or missing
     */
    public JSONObject getSighting(long id) throws JSONException {
        JSONObject sighting = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] columns = {TIMESTAMP_COLUMN, DATA_COLUMN};
        String whereClause = ID_COLUMN + " = " + id;
        Cursor c = db.query(true, TABLE_NAME, columns, whereClause, null, null, null, null, null);

        if (c.moveToNext()) {
            String timestamp = c.getString(c.getColumnIndexOrThrow(TIMESTAMP_COLUMN));
            String data = c.getString(c.getColumnIndexOrThrow(DATA_COLUMN));
            sighting = new JSONObject();
            JSONObject sightingData = new JSONObject(data);
            sighting.put(SightingsKeys.DATA, sightingData);
            sighting.put(SightingsKeys.TIMESTAMP, timestamp);
        }

        c.close();
        return sighting;
    }

    /**
     * Finds and returns all sightings in the database.
     *
     * @return All sightings in the database
     * @throws JSONException If the stored data is malformed or missing
     */
    public ArrayList<JSONObject> getAllSightings() throws JSONException {
        ArrayList<JSONObject> sightings = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {ID_COLUMN, TIMESTAMP_COLUMN, DATA_COLUMN};
        Cursor c = db.query(false, TABLE_NAME, columns, null, null, null, null, null, null);

        while (c.moveToNext()) {
            int id = c.getInt(c.getColumnIndexOrThrow(ID_COLUMN));
            JSONObject sighting = getSighting(id);
            sighting.put(SightingsKeys.ID, id);

            sightings.add(sighting);
        }

        c.close();
        return sightings;
    }

    /**
     * Determines and returns the number of sightings in the database, regardless of whether or not
     * they are duplicates.
     *
     * @return The number of sightings in the database
     */
    public long getNumberOfRecords() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return DatabaseUtils.queryNumEntries(db, TABLE_NAME);
    }

    /**
     * Returns the number of sightings in the database with different scientific names.
     *
     * @return The number of unique stored records
     * @throws JSONException If the stored data is malformed or missing
     */
    public long getUniqueNumberOfRecords() throws JSONException {
        int numOfUniqueRecords = 0;
        ArrayList<String> namesTaken = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {DATA_COLUMN};
        Cursor c = db.query(false, TABLE_NAME, columns, null, null, null, null, null, null);

        while (c.moveToNext()) {
            String stringData = c.getString(c.getColumnIndexOrThrow(DATA_COLUMN));
            JSONObject data = new JSONObject(stringData);
            String scientificName = data.getString(SightingsKeys.DATA_SCIENTIFIC_NAME);
            if (!namesTaken.contains(scientificName)) {
                namesTaken.add(scientificName);
                numOfUniqueRecords++;
            }
        }

        c.close();
        return numOfUniqueRecords;
    }

    /**
     * Updates a sighting to have new data.
     * <br>
     * If the sighting cannot be found, no sightings will be updated.
     *
     * @param id              The _id of the sighting ot update
     * @param updatedSighting The JSONObject containing the new data to be stored
     * @return The number of updated rows on success
     * @throws JSONException If the stored data or new data is malformed or missing
     */
    public int updateSighting(long id, JSONObject updatedSighting) throws JSONException {
        int numOfAffectedRows = 0;
        if (updatedSighting != null) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(TIMESTAMP_COLUMN, updatedSighting.get(SightingsKeys.TIMESTAMP).toString());
            values.put(DATA_COLUMN, updatedSighting.get(SightingsKeys.DATA).toString());
            String whereClause = ID_COLUMN + " = " + id;
            numOfAffectedRows = db.update(TABLE_NAME, values, whereClause, null);
        }

        return numOfAffectedRows;
    }

    /**
     * Removes all sightings from the table, effectively clearing the table.
     *
     * @return The number of removed sightings
     */
    public int removeAllSightings() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return db.delete(TABLE_NAME, null, null);
    }

    /**
     * Removes a sighting with a given _id from the table.
     *
     * @param id The _id of the sighting to remove
     * @return The number of affected rows
     */
    public int removeSighting(long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String whereClause = ID_COLUMN + " = " + id;
        return db.delete(TABLE_NAME, whereClause, null);
    }
}
