package au.com.ugmedia.sightings.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.R;
import au.com.ugmedia.sightings.model.util.jsonKeys.SurveyMethodKeys;

/**
 * An adapter made specifically for interacting with Survey methods and AutoSurveyMethodTextView to
 * filter by the method name.
 * <p/>
 * {@see AutoSurveyMethodTextView}
 */
public class SurveyMethodCompletionAdapter extends ArrayAdapter<JSONObject> {
    private static final String TAG = SurveyMethodCompletionAdapter.class.getSimpleName();

    private Filter filter;
    private LayoutInflater inflater;
    private int resId;
    private ArrayList<JSONObject> originalSurveyMethods;
    private ArrayList<JSONObject> filteredSurveyMethods;

    /**
     * Constructor.
     *
     * @param context The context of the calling activity for the associated LayoutInflater
     * @param resId   The id of the layout file to use as a dropdown. This can be any layout as long
     *                as it contains two TextView objects named "list_item_survey_method_survey_name" and
     *                "list_item_survey_method_times_used"
     * @param items   The ArrayList of Species objects associated with this adapter
     */
    public SurveyMethodCompletionAdapter(Context context, int resId, ArrayList<JSONObject> items) {
        super(context, resId, items);

        this.resId = resId;
        this.originalSurveyMethods = new ArrayList<>(items);
        this.filteredSurveyMethods = new ArrayList<>(items);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Gets the view associated with a particular instance in a ListView and populates it with data.
     * This particular version will populate an instance with a survey method's name and times used.
     *
     * @param position    The position in the ListView used to retrieve the correct data for this instance
     * @param convertView The view to be inflated and used in the ListView
     * @param parent      The parent of the view that will be used
     * @return A view populated with survey method data
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                convertView = inflater.inflate(resId, parent, false);
            }

            JSONObject surveyMethod = getItem(position);
            TextView surveyMethodNameView = (TextView) convertView.findViewById(R.id.list_item_survey_method_survey_name);
            TextView timesUsedView = (TextView) convertView.findViewById(R.id.list_item_survey_method_times_used);

            surveyMethodNameView.setText(surveyMethod.getString(SurveyMethodKeys.METHOD_NAME));
            timesUsedView.setText(getContext().getString(R.string.list_item_survey_method_times_used, surveyMethod.get(SurveyMethodKeys.TIMES_USED)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    /**
     * Gets the current size of the filtered array
     *
     * @return The current size of the filteredSpecies array
     */
    @Override
    public int getCount() {
        return filteredSurveyMethods.size();
    }

    /**
     * Returns a JSONObject containing survey method information from the filteredSpecies array
     * based on position.
     *
     * @param position The position to search for in the array
     * @return The JSONObject located at the given position
     */
    @Override
    public JSONObject getItem(int position) {
        return filteredSurveyMethods.get(position);
    }

    /**
     * Returns the filter to be used when auto-completing the text.
     * If one does not exist it will be created.
     *
     * @return The Filter object used for filtering text
     */
    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new SurveyMethodFilter();
        return filter;
    }

    /**
     * A class used only by this adapter for filtering.
     * <p/>
     * Provides a custom way to filter data by searching for the supplied text in the survey
     * method's name.
     */
    private class SurveyMethodFilter extends Filter {

        /**
         * Filters through the given ArrayList matching any text to the survey method name.
         *
         * @param constraint The text to search for
         * @return A FilterResult object containing survey method(s) that matched the given criteria
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            String searchCondition = "";
            if (constraint != null) {
                searchCondition = constraint.toString().toLowerCase();
            }

            if (searchCondition.length() == 0) {
                ArrayList<JSONObject> speciesCopy = new ArrayList<>(originalSurveyMethods);

                results.values = speciesCopy;
                results.count = speciesCopy.size();
            } else {
                ArrayList<JSONObject> surveyMethodsCopy = new ArrayList<>(originalSurveyMethods);
                ArrayList<JSONObject> newSurveyMethods = new ArrayList<>();

                try {
                    for (JSONObject currentSurveyMethod : surveyMethodsCopy) {
                        String methodName = currentSurveyMethod.getString(SurveyMethodKeys.METHOD_NAME);

                        if (methodName.toLowerCase().contains(searchCondition)) {
                            newSurveyMethods.add(currentSurveyMethod);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                results.values = newSurveyMethods;
                results.count = newSurveyMethods.size();
            }
            return results;
        }

        /**
         * Adds all found matches from the performFiltering method to the ArrayList of filtered
         * survey methods.
         * <br>
         * If no results are found, listeners are notified of an invalidated data set being returned.
         *
         * @param constraint The constraint used for filter publishing
         * @param results    The results containing an ArrayList of JSONObjects to be 'published'
         *                   to the filtered survey method ArrayList
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                filteredSurveyMethods.clear();
                filteredSurveyMethods.addAll((ArrayList<JSONObject>) results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
