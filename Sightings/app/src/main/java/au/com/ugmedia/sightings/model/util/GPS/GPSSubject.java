package au.com.ugmedia.sightings.model.util.gps;

import android.location.Location;

import java.util.ArrayList;

/**
 * An abstract class that provides the basic functionality for the GPS.
 * <p/>
 * {@see GPSListener}
 */
public abstract class GPSSubject {
    private static final String TAG = GPSSubject.class.getSimpleName();

    protected ArrayList<GPSListener> listeners = new ArrayList<>();
    protected Location location;

    /**
     * Notifies each listener of the availability of a new location by calling its onLocationAvailable(Location)
     * <p/>
     * {@see GPSListener#onLocationAvailable(Location)}
     */
    public void notifyOfNewLocation() {
        for (GPSListener listener : listeners) {
            listener.onLocationAvailable(location);
        }
    }

    /**
     * Adds a listener to observe details and location changes to this class
     *
     * @param listener The GPSListener to register
     */
    public void addListener(GPSListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener from the listeners array
     *
     * @param listener The GPSListener to remove
     */
    public void removeListener(GPSListener listener) {
        listeners.remove(listener);
    }

    /**
     * Removes all GPSListener objects in the listeners array
     */
    public void removeAllListeners() {
        listeners.clear();
    }

    /**
     * An abstract method that allows another object to request a new location.
     */
    public abstract void startRequestingLocations();

}
