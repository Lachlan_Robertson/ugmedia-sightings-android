package au.com.ugmedia.sightings;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;
import au.com.ugmedia.sightings.view.AutoSpeciesTextView;
import au.com.ugmedia.sightings.view.AutoSurveyMethodTextView;

/**
 * Activity that allows a user to edit an existing sighting
 */
public class EditSightingActivity extends SightingsActivity {
    private static final String TAG = EditSightingActivity.class.getSimpleName();

    private long originalSightingId;
    private JSONObject originalSighting;

    /**
     * Creates the activity and gets the id of the original sighting for later use.
     *
     * @param savedInstanceState The saved state of this activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        originalSightingId = getIntent().getExtras().getLong(getString(R.string.intent_extra_id));
        initGui();
    }

    // ---- END: Lifecycle methods ----

    /**
     * Allows a user to update the sighting they are currently editing (if it is valid) or exit this
     * activity.
     *
     * @param item The selected menu item
     * @return Whether or not to consume the menu event
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        resetGpsTask();
        int menuId = item.getItemId();

        if (menuId == R.id.action_update) {
            JSONObject sighting = createSightingFromGuiData();
            if (updateSighting(originalSightingId, sighting)) {
                Toast.makeText(this, getString(R.string.toast_message_updated_sighting), Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (menuId == R.id.action_cancel) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    // ---- END: MenuBar methods ----

    /**
     * Populates the GUI with data from a Sightings object, specifically the object with the
     * originalSightingId attribute of this activity.
     * <br>
     * Note that if the sighting does not contain some data, these fields will be left blank.
     */
    protected void initGui() {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.edit_map);
        timestampView = (TextView) findViewById(R.id.edit_timestamp);
        commonNameView = (TextView) findViewById(R.id.edit_common_name);
        quantityView = (EditText) findViewById(R.id.edit_quantity);
        photoView = (Switch) findViewById(R.id.edit_photo_taken_or_not);
        healthView = (Switch) findViewById(R.id.edit_is_alive_or_dead);
        notesView = (EditText) findViewById(R.id.edit_additional_notes);
        siteNameView = (EditText) findViewById(R.id.edit_site_name);
        distanceView = (EditText) findViewById(R.id.edit_distance);
        latitudeView = (EditText) findViewById(R.id.edit_latitude_input);
        longitudeView = (EditText) findViewById(R.id.edit_longitude_input);
        altitudeView = (TextView) findViewById(R.id.edit_altitude);
        accuracyView = (TextView) findViewById(R.id.edit_accuracy);
        speciesAutoCompleteView = (AutoSpeciesTextView) findViewById(R.id.edit_scientific_name);
        surveyMethodAutoCompleteView = (AutoSurveyMethodTextView) findViewById(R.id.edit_survey_method);

        setupAdapters();

        try {
            originalSighting = getSightingsDAO().getSighting(originalSightingId);
            mapFragment.getMapAsync(this);
            JSONObject data = new JSONObject(originalSighting.get(SightingsKeys.DATA).toString());
            JSONObject location = new JSONObject(data.get(SightingsKeys.DATA_LOCATION).toString());

            timestampView.setText(getString(R.string.timestamp) + SimpleDateFormat.getDateTimeInstance().format(new Date()));
            speciesAutoCompleteView.setText(data.optString(SightingsKeys.DATA_SCIENTIFIC_NAME));
            commonNameView.setText(getString(R.string.common_name) + data.optString(SightingsKeys.DATA_COMMON_NAME));
            quantityView.setText(data.optString(SightingsKeys.DATA_QUANTITY));
            photoView.setChecked(data.optString(SightingsKeys.DATA_PHOTO_TAKEN).equals("Photo"));
            healthView.setChecked(data.optString(SightingsKeys.DATA_HEALTH).equals("Alive"));
            notesView.setText(data.optString(SightingsKeys.DATA_NOTES));
            siteNameView.setText(data.optString(SightingsKeys.DATA_SITE_NAME));
            surveyMethodAutoCompleteView.setText(data.optString(SightingsKeys.DATA_SURVEY_METHOD));
            distanceView.setText(location.optString(SightingsKeys.DATA_LOCATION_DISTANCE));

            latitudeView.setText(location.optString(SightingsKeys.DATA_LOCATION_LATITUDE));
            longitudeView.setText(location.optString(SightingsKeys.DATA_LOCATION_LONGITUDE));
            altitudeView.setText(location.optString(SightingsKeys.DATA_LOCATION_ALTITUDE) + "m");
            accuracyView.setText(location.optString(SightingsKeys.DATA_LOCATION_ACCURACY) + "m");
        } catch (JSONException e) {
            Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    /**
     * Updates the current sighting with new data from the GUI.
     *
     * @param id          The id of the original sighting
     * @param newSighting The new sighting containing data to be updated to
     * @return true if sighting was updated, false otherwise
     */
    public boolean updateSighting(long id, JSONObject newSighting) {
        boolean successful = true;

        try {
            if (isValidSighting(newSighting)) {
                if (!getAppDataSingleton().updateSighting(id, newSighting)) {
                    successful = false;
                    Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                successful = false;
                Toast.makeText(getContext(), getString(R.string.toast_message_add_sighting_invalid_data), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            successful = false;
            Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        return successful;
    }

    /**
     * Places a marker on the instantiated map with the original location data from the sighting.
     * <br>
     * If the sighting does not have any location data no marker will be added to the map.
     *
     * @param map The newly instantiated GoogleMap
     */
    @Override
    public void onMapReady(GoogleMap map) {
        super.onMapReady(map);

        if (originalSighting != null) {
            try {
                JSONObject data = new JSONObject(originalSighting.get(SightingsKeys.DATA).toString());
                JSONObject location = new JSONObject(data.get(SightingsKeys.DATA_LOCATION).toString());
                if (!(location.getString(SightingsKeys.DATA_LOCATION_LONGITUDE).equals("") ||
                        location.getString(SightingsKeys.DATA_LOCATION_LATITUDE).equals(""))) {
                    LatLng latLng = new LatLng(
                            location.getDouble(SightingsKeys.DATA_LOCATION_LATITUDE),
                            location.getDouble(SightingsKeys.DATA_LOCATION_LONGITUDE));
                    CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(latLng, 10);

                    map.addMarker(new MarkerOptions().position(latLng));
                    map.animateCamera(camera);
                }
            } catch (JSONException e) {
                Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    // -- BEGIN: Getters and Setters
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_sighting;
    }

    @Override
    protected int getMenuLayoutId() {
        return R.menu.menu_edit_sighting;
    }

    @Override
    protected Context getContext() {
        return this;
    }
}