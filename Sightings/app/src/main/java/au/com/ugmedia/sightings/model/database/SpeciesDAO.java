package au.com.ugmedia.sightings.model.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;

/**
 * A Data Access Object that provides a simple way to make various requests to the Species table
 * in the database.
 */
public class SpeciesDAO {
    private static final String TAG = SpeciesDAO.class.getSimpleName();

    public static final String TABLE_NAME = "Species";

    public static final String ID_COLUMN = "_id";
    public static final String SCIENTIFIC_NAME_COLUMN = "Scientific_Name";
    public static final String USE_COUNT_COLUMN = "Use_Count";
    public static final String DATA_COLUMN = "Data";

    private SQLiteOpenHelper dbHelper;

    /**
     * Constructor.
     *
     * @param helper The helper required to actually access the database
     */
    public SpeciesDAO(SQLiteOpenHelper helper) {
        dbHelper = helper;
    }

    /**
     * Adds a single species record to the database.
     * <br>
     * If the species to insert is null, no data will be inserted and no exception raised.
     * Therefore clients should check the return value to determine whether or not this method
     * successfully inserted.
     *
     * @param species The species to insert
     * @return The _id of the inserted row on success or -1 on failure
     * @throws JSONException If the inserted species contains malformed or missing data
     */
    public long addSpecies(JSONObject species) throws JSONException {
        long insertedRowId = -1;
        if (species != null) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(SCIENTIFIC_NAME_COLUMN, species.get(SpeciesKeys.SCIENTIFIC_NAME).toString());
            values.put(USE_COUNT_COLUMN, species.getInt(SpeciesKeys.USE_COUNT));
            values.put(DATA_COLUMN, species.get(SpeciesKeys.DATA).toString());

            insertedRowId = db.insert(TABLE_NAME, null, values);
        }

        return insertedRowId;
    }

    /**
     * Gets all species records from the database.
     *
     * @return An ArrayList of all species records stored in the database
     * @throws JSONException If the stored data is malformed or missing
     */
    public ArrayList<JSONObject> getAllSpecies() throws JSONException {
        ArrayList<JSONObject> allSpecies = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {SCIENTIFIC_NAME_COLUMN, USE_COUNT_COLUMN, DATA_COLUMN};
        Cursor c = db.query(true, TABLE_NAME, columns, null, null, null, null, null, null);

        while (c.moveToNext()) {
            JSONObject currentSpecies = new JSONObject();
            JSONObject currentSpeciesData = new JSONObject();
            JSONObject storedData = new JSONObject(c.getString(c.getColumnIndexOrThrow(SpeciesKeys.DATA)));

            String scientificName = c.getString(c.getColumnIndexOrThrow(SCIENTIFIC_NAME_COLUMN));
            long timesUsed = c.getLong(c.getColumnIndexOrThrow(USE_COUNT_COLUMN));
            String commonName = storedData.optString(SpeciesKeys.DATA_COMMON_NAME, "");

            currentSpeciesData.put(SpeciesKeys.DATA_COMMON_NAME, commonName);

            currentSpecies.put(SpeciesKeys.SCIENTIFIC_NAME, scientificName);
            currentSpecies.put(SpeciesKeys.USE_COUNT, timesUsed);
            currentSpecies.put(SpeciesKeys.DATA, currentSpeciesData);

            allSpecies.add(currentSpecies);
        }

        c.close();
        return allSpecies;
    }

    /**
     * Finds and returns the scientific name of the species with the highest USE_COUNT in the database.
     * <br>
     * Note that this method only returns the first name it finds and only if the USE_COUNT is
     * greater than 0.
     *
     * @return The scientific name of the most used species
     */
    public String getMostUsedSpecies() {
        String mostUsedSpecies = "N/A";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {SCIENTIFIC_NAME_COLUMN};
        String whereClause = "NOT(" + USE_COUNT_COLUMN + " = 0) AND " +
                USE_COUNT_COLUMN + " = (SELECT MAX(" + USE_COUNT_COLUMN + ") FROM " + TABLE_NAME + ")";
        Cursor c = db.query(true, TABLE_NAME, columns, whereClause, null, null, null, null, null);

        //Only need the first value. Others can be ignored.
        if (c.moveToNext()) {
            mostUsedSpecies = c.getString(c.getColumnIndexOrThrow(SCIENTIFIC_NAME_COLUMN));
        }

        c.close();
        return mostUsedSpecies;
    }

    /**
     * Returns the number of times a species has been used.
     *
     * @param scientificName The scientific name of the species to search for
     * @return The USE_COUNT of the species with the given scientificName
     */
    public long getTimesUsed(String scientificName) {
        long timesUsed = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {USE_COUNT_COLUMN};
        String whereClause = SCIENTIFIC_NAME_COLUMN + " = \"" + scientificName + "\"";
        Cursor c = db.query(true, TABLE_NAME, columns, whereClause, null, null, null, null, null);

        if (c.moveToNext()) {
            timesUsed = c.getLong(c.getColumnIndexOrThrow(USE_COUNT_COLUMN));
        }

        c.close();
        return timesUsed;
    }

    /**
     * Gets the "species group" of the requested species.
     * <br>
     * If a species does not have a group, an empty String will be returned.
     *
     * @param scientificName The scientific name of the species to search for
     * @return The "species group" of the species with the requested scientific name
     * @throws JSONException If the stored data is malformed or missing
     */
    public String getGroup(String scientificName) throws JSONException {
        String group = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {DATA_COLUMN};
        String whereClause = SCIENTIFIC_NAME_COLUMN + " = \"" + scientificName + "\"";
        Cursor c = db.query(true, TABLE_NAME, columns, whereClause, null, null, null, null, null);

        if (c.moveToNext()) {
            JSONObject data = new JSONObject(c.getString(c.getColumnIndexOrThrow(DATA_COLUMN)));
            group = data.getString(SpeciesKeys.DATA_GROUP);
        }

        c.close();
        return group;
    }

    /**
     * Returns whether or not a species exists in the database.
     *
     * @param scientificName The scientific name of the species to search for
     * @return true if the species is in the database or false otherwise
     */
    public boolean speciesExists(String scientificName) {
        boolean exists = false;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {ID_COLUMN};
        String whereClause = SCIENTIFIC_NAME_COLUMN + " = \"" + scientificName + "\"";
        Cursor c = db.query(true, TABLE_NAME, columns, whereClause, null, null, null, null, null);

        if (c.getCount() > 0) {
            exists = true;
        }

        c.close();
        return exists;
    }

    /**
     * Sets the USE_COUNT of every species in database to 0.
     */
    public void resetUseCount() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "UPDATE " + TABLE_NAME + " SET "
                + USE_COUNT_COLUMN + " = 0";
        db.execSQL(sql);
    }

    /**
     * Increments the USE_COUNT of a particular species.
     * <br>
     * If the species does not exist, no records will be updated.
     *
     * @param scientificName The scientific name of the species to search for
     */
    public void incrementUseCount(String scientificName) {
        updateUseCount(scientificName, "+1");
    }

    /**
     * Decrements the USE_COUNT of a particular species.
     * <br>
     * If the species does not exist, no records will be updated.
     *
     * @param scientificName The scientific name of the species to search for
     */
    public void decrementUseCount(String scientificName) {
        updateUseCount(scientificName, "-1");
    }

    /**
     * Updates a species' USE_COUNT by a given amount; increasing / decreasing it as needed.
     * <br>
     * If the species does not exist, no records will be updated.
     *
     * @param scientificName The scientific name of the species to search for
     * @param amount         The amount to increase / decrease USE_COUNT by
     */
    private void updateUseCount(String scientificName, String amount) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String whereClause = SCIENTIFIC_NAME_COLUMN + " = \"" + scientificName + "\"";
        //Standard "update" method doesn't work. Must fall back to standard SQL
        String sql = "UPDATE " + TABLE_NAME + " SET "
                + USE_COUNT_COLUMN + " = " + USE_COUNT_COLUMN + " " + amount + " WHERE "
                + whereClause;
        db.execSQL(sql);
    }
}
