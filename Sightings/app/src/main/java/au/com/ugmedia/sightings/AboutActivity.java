package au.com.ugmedia.sightings;

import android.content.Context;
import android.os.Bundle;

/**
 * Activity that displays basic "about" information.
 * <br>
 * As it is intended for informational use only, it contain little logic.
 */
public class AboutActivity extends DarkThemeActivity {
    private static final String TAG = AboutActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    @Override
    protected Context getContext() {
        return this;
    }
}
