package au.com.ugmedia.sightings.view;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import au.com.ugmedia.sightings.R;

/**
 * A custom dialog preference that allows a user to pick a number from a specified range via a
 * NumberPicker.
 * <br>
 * Original idea sourced from:
 * http://stackoverflow.com/questions/23038030/using-a-numberpicker-as-a-dialogpreference-why-wont-my-setting-save
 */
public class NumberPickerPreference extends DialogPreference {
    public static final int DEFAULT_VALUE = 30;
    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 100;

    private NumberPicker picker;

    /**
     * Constructor.
     *
     * @param context The context of the preference
     * @param attrs   Attributes for the dialog
     */
    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.number_picker_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
        setDialogIcon(null);
    }

    /**
     * Called when the dialog is created, this method sets the minimum, maximum and default selection.
     * Default selection is based on previous user preference or a default value if none is found.
     *
     * @return The created, fully initialised dialog View
     */
    @Override
    protected View onCreateDialogView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.number_picker_dialog, null);
        picker = (NumberPicker) view.findViewById(R.id.picker);

        picker.setMinValue(MIN_VALUE);
        picker.setMaxValue(MAX_VALUE);
        picker.setValue(getPersistedInt(DEFAULT_VALUE));
        picker.setWrapSelectorWheel(false);
        return view;
    }

    /**
     * Save and persist data when dialog is closed only if the user did not cancel.
     *
     * @param positiveResult Whether or not the user cancelled out of the dialog
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            persistInt(picker.getValue());
        }
    }
}
