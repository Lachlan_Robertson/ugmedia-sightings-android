package au.com.ugmedia.sightings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.Preference;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;

import au.com.ugmedia.sightings.view.NumberPickerPreference;
import au.com.ugmedia.sightings.view.SettingsFragment;

/**
 * Activity that allows a user to modify app settings.
 */
public class SettingsActivity extends DarkThemeActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = SettingsActivity.class.getSimpleName();

    private SettingsFragment settingsFragment = new SettingsFragment();

    /**
     * Creates the activity and loads the associated fragment to actually display a non-empty screen
     * to the user.
     *
     * @param savedInstanceState The activity's saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, settingsFragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPrefs().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        getPrefs().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    /**
     * Applies all settings when the user presses the 'back' button to ensure no data is discarded.
     */
    @Override
    public void onBackPressed() {
        resetGpsTask();
        setResult(RESULT_OK);
        finish();
    }

    /**
     * Displays a dialog to ask the user to confirm whether or not they truly wish to erase all
     * sightings records.
     *
     * @param v The view that called this function
     */
    public void handleDeleteAll(View v) {
        resetGpsTask();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_dialog_confirm_delete))
                .setMessage(getString(R.string.message_dialog_confirm_delete))
                .setPositiveButton(getString(R.string.confirm_delete_positive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAllRecords();
                        settingsFragment.updateRecordSummaries();
                    }
                })
                .setNegativeButton(getString(R.string.confirm_delete_negative), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do nothing.
                    }
                });
        builder.show();
    }

    /**
     * Removes all sightings records and displays a message to the user indicating success or
     * failure.
     */
    private void deleteAllRecords() {
        try {
            getAppDataSingleton().removeAllRecords();
            Toast.makeText(this, getString(R.string.toast_message_removed_all_records), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Toast.makeText(this, getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Updates aspects of the GUI when the user changes their settings.
     *
     * @param sharedPreferences The preferences object containing all user settings
     * @param key               The key of the modified preference
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.default_email_key))) {
            Preference defaultEmailPref = settingsFragment.findPreference(key);
            defaultEmailPref.setSummary(getPrefs().getString(key, ""));

        } else if (key.equals(getString(R.string.csv_files_to_keep_key))) {
            Preference csvFilesToKeepPref = settingsFragment.findPreference(key);
            csvFilesToKeepPref.setSummary(String.valueOf(getPrefs().getInt(key, NumberPickerPreference.DEFAULT_VALUE)));

        } else if (key.equals(getString(R.string.dark_interface_key))) {
            Toast.makeText(this, getString(R.string.toast_message_dark_interface_changed), Toast.LENGTH_SHORT).show();

        } else if (key.equals(getString(R.string.gps_auto_off_key))) {
            if (getPrefs().getBoolean(key, true)) {
                getGpsSingleton().resetGpsAutoOffTask();
            } else {
                getGpsSingleton().stopGpsAutoOffTask();
                getGpsHelper().startRequestingLocations();
            }

        } else if (key.equals(getString(R.string.screen_auto_off_key))) {
            if (getPrefs().getBoolean(key, false)) {
                if (getPowerManager().isWakeLockLevelSupported(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK)) {
                    acquireWakeLock();
                } else {
                    Toast.makeText(getContext(), getString(R.string.toast_message_auto_off_not_available), Toast.LENGTH_SHORT).show();
                }
            } else {
                releaseWakeLock();
            }
        }
    }

    // -- BEGIN: Getters and Setters
    @Override
    protected Context getContext() {
        return this;
    }
}