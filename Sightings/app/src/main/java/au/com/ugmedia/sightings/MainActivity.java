package au.com.ugmedia.sightings;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import au.com.ugmedia.sightings.model.AppDataSingleton;
import au.com.ugmedia.sightings.model.adapter.SightingsAdapter;
import au.com.ugmedia.sightings.model.database.SpeciesDAO;
import au.com.ugmedia.sightings.model.util.FileHandler;
import au.com.ugmedia.sightings.model.util.SightingsMetadata;
import au.com.ugmedia.sightings.model.util.UTMLocation;
import au.com.ugmedia.sightings.model.util.gps.GPSHelperSingleton;
import au.com.ugmedia.sightings.model.util.gps.GPSListener;
import au.com.ugmedia.sightings.model.util.jsonKeys.SightingsKeys;
import au.com.ugmedia.sightings.model.util.jsonKeys.SpeciesKeys;

/**
 * Activity responsible for showing an overview of all sightings to the user. It is also indirectly
 * responsible for the importing and exporting of sighting data.
 */
public class MainActivity extends DarkThemeActivity implements GPSListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int SETTINGS_REQUEST_CODE = 1;
    private static final int CONTEXT_MENU_COPY_ID = 1;
    private static final int CONTEXT_MENU_DELETE_ID = 2;
    private final Context CONTEXT = this;

    private GPSHelperSingleton gpsSingleton;
    private AppDataSingleton appDataSingleton;
    private FileHandler fileHandler;

    private ListView sightingsList;
    private SightingsAdapter adapter;

    private AppDataSingleton.SightingsSortMethod sortMethod;
    private ProgressDialog waitDialog;
    private Dialog sortDialog;

    /**
     * Creates the activity, initialises key variables and, if the user is attempting to import
     * species, starts a background task to import species.
     *
     * @param savedInstanceState The activity's saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gpsSingleton = getGpsSingleton();
        appDataSingleton = getAppDataSingleton();
        sortMethod = AppDataSingleton.SightingsSortMethod.convertToSortMethod(
                getPrefs().getString(getString(R.string.sort_by_key),
                        AppDataSingleton.SightingsSortMethod.DATE_NEWEST.toString()));

        if (getIntent().getData() != null) { //User is attempting to import Species data
            new ImportSpeciesTask().execute(getIntent());
        }

        initGui();
    }

    /**
     * Resumes the activity and, if GPS is available, adds a listener for new GPS locations.
     * If the GPS is disabled, the activity title is changed to reflect this.
     * The GUI is then updated.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (isGpsEnabled()) {
            getGpsHelper().addListener(this);
            if (getActionBar() != null) {
                getActionBar().setTitle(R.string.title_gps_searching);
            }
        } else {
            if (getActionBar() != null) {
                getActionBar().setTitle(R.string.title_gps_disabled);
            }
        }

        updateGui(null);
    }

    /**
     * Pauses the activity and, if needed, removes GPS listeners to free resources.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (isGpsEnabled()) {
            getGpsHelper().removeListener(this);
        }
    }

    /**
     * Upon successfully modifying settings, the activity is recreated with the new settings applied.
     * <br>
     * Note that when the activity is recreated it must be recreated after all other lifecycle
     * methods hav been called or the GPS may malfunction.
     *
     * @param requestCode The code sent when the intent to the other activity was sent
     * @param resultCode  The code retrieved from the finishing of the activity
     * @param data        Additional data sent from the finished activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SETTINGS_REQUEST_CODE && resultCode == RESULT_OK) {
            // Delay the recreation of the activity to ensure the lifecycle completes properly.
            // Failure to do so can lead to a malfunctioning GPS.
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recreate();
                }
            }, 0);
        }
    }

    // ---- END: Lifecycle methods ----

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Allows a user to add a new sighting, export data and email it (if there is at least 1 record
     * to export), sort records or move to settings depending on their choice in the menu.
     *
     * @param item The selected menu item
     * @return Whether or not to consume the event
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        resetGpsTask();
        int menuId = item.getItemId();

        if (menuId == R.id.action_add_new) {
            Intent intent = new Intent(this, AddSightingActivity.class);
            startActivity(intent);
        } else if (menuId == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, SETTINGS_REQUEST_CODE);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Initialises all GUI elements
     */
    private void initGui() {
        initSightingsList();
        initToolbar();
    }

    /**
     * Initialises the sightings list by sorting the data and setting an onClick listener for it.
     * <br>
     * The listener allows a user to edit the selected sighting by moving to a new screen.
     */
    private void initSightingsList() {
        resetListAdapter();
        registerForContextMenu(getSightingsList());

        getSightingsList().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    JSONObject sighting = getAdapter().getItem(position);
                    long sightingId = sighting.getLong(SightingsKeys.ID);

                    Intent edit = new Intent(CONTEXT, EditSightingActivity.class);
                    edit.putExtra(getString(R.string.intent_extra_id), sightingId);
                    startActivity(edit);
                } catch (JSONException e) {
                    Toast.makeText(CONTEXT, getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Initialises the bottom toolbar.
     * <br>
     * This method also sets up icons and defines actions to do when an option is selected.
     */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_bottom);
        toolbar.inflateMenu(R.menu.menu_bottom_toolbar);

        final Menu menu = toolbar.getMenu();
        if (!isGpsEnabled()) {
            menu.findItem(R.id.action_toggle_gps).setIcon(R.drawable.location_disabled);
        }

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                resetGpsTask();
                int menuId = item.getItemId();

                if (menuId == R.id.action_toggle_gps) {
                    if (isGpsEnabled()) {
                        disableGps();
                        menu.findItem(R.id.action_toggle_gps).setIcon(R.drawable.location_disabled);

                        if (getActionBar() != null) {
                            getActionBar().setTitle(R.string.title_gps_disabled);
                        }
                    } else {
                        enableGps();
                        menu.findItem(R.id.action_toggle_gps).setIcon(R.drawable.location_active);

                        if (getActionBar() != null) {
                            getActionBar().setTitle(R.string.title_gps_searching);
                        }
                    }

                } else if (menuId == R.id.action_export_data) {
                    if (getSightingsDAO().getNumberOfRecords() != 0) {
                        new ExportSpeciesTask().execute();
                    } else {
                        Toast.makeText(CONTEXT, getString(R.string.toast_message_export_error), Toast.LENGTH_SHORT).show();
                    }

                } else if (menuId == R.id.action_sort_records) {
                    displaySortDialog();
                }

                return false;
            }
        });
    }

    /**
     * Disables the GPS for this app only, preventing further use.
     */
    private void disableGps() {
        getPrefs().edit().putBoolean(getString(R.string.gps_enabled), false).commit();
        getGpsHelper().stopRequestingLocations();
    }

    /**
     * Enables the GPS and attempts to retrieve a new location.
     * <br>
     * Note that this may fail if the user has disabled GPS on the device.
     */
    private void enableGps() {
        getPrefs().edit().putBoolean(getString(R.string.gps_enabled), true).commit();
        getGpsHelper().startRequestingLocations();
    }

    // --- END init...() methods ---

    // Original ContextMenu code from
    // http://www.mikeplate.com/2010/01/21/show-a-context-menu-for-long-clicks-in-an-android-listview/

    /**
     * Adds two menu items to a popup dialog that allows the user to select whether they wish to
     * copy or delete the selected sighting.
     *
     * @param menu     The created context menu
     * @param v        The View that caused the menu to be created
     * @param menuInfo Additional info about the context menu
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        resetGpsTask();
        // If ContextMenu is the result of a long click on the ListView...
        if (v.getId() == getSightingsList().getId()) {
            menu.setHeaderTitle(getString(R.string.context_menu_heading));
            menu.add(Menu.NONE, CONTEXT_MENU_COPY_ID, 0, getString(R.string.context_menu_copy));
            menu.add(Menu.NONE, CONTEXT_MENU_DELETE_ID, 1, getString(R.string.context_menu_delete));
        }
    }

    /**
     * Copies or deletes a sighting from the sightings list depending on selected choice.
     *
     * @param item The selected menu option
     * @return true to consume the event
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        resetGpsTask();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int selectedMenuItemId = item.getItemId();

        try {
            JSONObject sighting = getAdapter().getItem(info.position);
            long sightingId = sighting.getLong(SightingsKeys.ID);

            if (selectedMenuItemId == CONTEXT_MENU_COPY_ID) {
                duplicateRecord(sightingId);
            } else if (selectedMenuItemId == CONTEXT_MENU_DELETE_ID) {
                deleteRecord(sightingId);
                Toast.makeText(this, getString(R.string.toast_message_delete_one), Toast.LENGTH_SHORT).show();
            }

            updateGui(null);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Duplicates a sighting, making a new record with data like an existing sighting
     *
     * @param sightingId The _id of the original sighting to duplicate
     */
    private void duplicateRecord(long sightingId) {
        Location location = null;
        if (isGpsEnabled()) {
            location = getGpsHelper().getLocation();
        }
        UTMLocation utmLocation = new UTMLocation(location);

        try {
            if (!getAppDataSingleton().duplicateSighting(sightingId, utmLocation, new SightingsMetadata(this))) {
                Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.toast_message_unexpected_error), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Removes a record from the sightings list.
     *
     * @param sightingId The _id of the sighting to remove
     * @throws JSONException If the data is malformed or missing
     */
    private void deleteRecord(long sightingId) throws JSONException {
        getAppDataSingleton().removeSighting(sightingId);
    }

    /**
     * Updates the GUI when a new location becomes available.
     *
     * @param location The new location
     */
    @Override
    public void onLocationAvailable(Location location) {
        updateGui(location);
    }

    /**
     * Updates elements of the GUI, including the activity title.
     * <br>
     * Note that if the location is null, nothing will happen.
     *
     * @param location The location containing relevant data
     */
    private void updateGui(Location location) {
        try {
            resetListAdapter();
            if (location != null && getActionBar() != null) {
                String actionBarTitle = getString(R.string.title_action_bar_starter_records) +
                        getSightingsDAO().getNumberOfRecords() + " | " +
                        getString(R.string.title_action_bar_starter_accuracy) +
                        location.getAccuracy() + "m";
                getActionBar().setTitle(actionBarTitle);
            }
        } catch (NullPointerException e) {
            Toast.makeText(this, getString(R.string.toast_message_unexpected_error), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    /**
     * Displays a sort dialog asking the user how they would like to sort the list.
     * <br>
     * Note that the currently chosen sort method will be disabled and choosing an option
     * instantly sorts the data.
     */
    private void displaySortDialog() {
        Button option;
        ArrayList<Integer> ids = new ArrayList<>(Arrays.asList(
                R.id.sort_date_newest, R.id.sort_date_oldest, R.id.sort_distance_closest, R.id.sort_distance_furthest));
        sortDialog = new Dialog(CONTEXT);
        sortDialog.setContentView(R.layout.dialog_sort_sightings);
        sortDialog.setTitle(getString(R.string.title_dialog_sort_records));

        for (int id : ids) {
            option = (Button) sortDialog.findViewById(id);
            option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetGpsTask();
                    sortSightings(v);
                }
            });
        }

        switch (sortMethod) {
            case DATE_NEWEST:
                sortDialog.findViewById(R.id.sort_date_newest).setEnabled(false);
                break;
            case DATE_OLDEST:
                sortDialog.findViewById(R.id.sort_date_oldest).setEnabled(false);
                break;
            case DISTANCE_CLOSEST:
                sortDialog.findViewById(R.id.sort_distance_closest).setEnabled(false);
                break;
            case DISTANCE_FURTHEST:
                sortDialog.findViewById(R.id.sort_distance_furthest).setEnabled(false);
                break;
        }

        sortDialog.show();
    }

    /**
     * Sorts sightings according to the chosen sort method.
     *
     * @param v The menu item option
     */
    public void sortSightings(View v) {
        Button button = (Button) v;
        String option = button.getText().toString();

        if (option.equals(getString(R.string.dialog_sort_date_newest))) {
            sortMethod = AppDataSingleton.SightingsSortMethod.DATE_NEWEST;
        } else if (option.equals(getString(R.string.dialog_sort_date_oldest))) {
            sortMethod = AppDataSingleton.SightingsSortMethod.DATE_OLDEST;
        } else if (option.equals(getString(R.string.dialog_sort_distance_closest))) {
            sortMethod = AppDataSingleton.SightingsSortMethod.DISTANCE_CLOSEST;
        } else if (option.equals(getString(R.string.dialog_sort_distance_furthest))) {
            sortMethod = AppDataSingleton.SightingsSortMethod.DISTANCE_FURTHEST;
        }

        getPrefs().edit().putString(getString(R.string.sort_by_key), sortMethod.toString()).commit();
        resetListAdapter();
        Toast.makeText(this, getString(R.string.toast_message_sorted_success), Toast.LENGTH_SHORT).show();

        sortDialog.dismiss();
    }

    /**
     * Resets the list adapter after sorting all sightings to ensure data is up-to-date.
     */
    private void resetListAdapter() {
        try {
            getAppDataSingleton().sortSightings(sortMethod, getGpsHelper().getLocation());
            getSightingsList().setAdapter(getAdapter());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // -- BEGIN: Getters and Setters
    private ListView getSightingsList() {
        if (sightingsList == null)
            sightingsList = (ListView) findViewById(R.id.mainSightingsList);
        return sightingsList;
    }

    private SightingsAdapter getAdapter() throws JSONException {
        adapter = new SightingsAdapter(this, R.layout.list_item_sighting, getAppDataSingleton().getSightings());
        return adapter;
    }

    private FileHandler getFileHandler() {
        if (fileHandler == null) {
            int fileKeepLimit = getPrefs().getInt(getString(R.string.csv_files_to_keep_key), FileHandler.DEFAULT_FILE_KEEP_LIMIT);
            fileHandler = new FileHandler(fileKeepLimit, getDatabaseHelper());
        }
        return fileHandler;
    }

    @Override
    protected Context getContext() {
        return CONTEXT;
    }

    /**
     * An task that executes on a background thread to import species into the app and store them
     * in the database.
     */
    private class ImportSpeciesTask extends AsyncTask<Intent, Void, Void> {
        private long numOfAddedRows = 0;

        /**
         * Display the wait dialog.
         */
        @Override
        protected void onPreExecute() {
            waitDialog = new ProgressDialog(CONTEXT);
            waitDialog.setTitle(getString(R.string.title_dialog_importing_species));
            waitDialog.setMessage(getString(R.string.message_dialog_import_species));
            waitDialog.setIndeterminate(true);
            waitDialog.setCancelable(false);
            waitDialog.show();
        }

        /**
         * Import species into the database using either a file or the input stream from an email.
         *
         * @param params Additional parameters such as file name, etc.
         * @return Nothing
         */
        @Override
        protected Void doInBackground(Intent... params) {
            Intent importIntent = params[0];
            InputStream is = null;

            try {
                if (importIntent.getScheme().equals("file")) { //Import is file
                    File file = new File(importIntent.getData().getPath());
                    is = new FileInputStream(file);

                } else if (importIntent.getScheme().equals("content")) { //Import is a data stream
                    is = getContentResolver().openInputStream(importIntent.getData());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (is != null) {
                try {
                    importSpecies(is);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        /**
         * Dismiss the wait dialog and display a success message telling the user how many rows were
         * inserted.
         *
         * @param aVoid Unused
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            if (waitDialog.isShowing()) {
                waitDialog.dismiss();
            }

            Toast.makeText(CONTEXT, getString(R.string.toast_message_imported_species, numOfAddedRows), Toast.LENGTH_SHORT).show();
        }

        /**
         * Actually imports the species into the database from a given stream.
         * <br>
         * Note that if the stream is invalid, nothing will be imported.
         *
         * @param is The stream containing the data to import
         * @throws JSONException If the data is malformed
         */
        private void importSpecies(InputStream is) throws JSONException {
            FileHandler fHandler = new FileHandler();
            SpeciesDAO speciesDAO = new SpeciesDAO(getDatabaseHelper());
            ArrayList<JSONObject> speciesToImport = fHandler.getSpeciesFromStream(is);

            if (speciesToImport != null) {
                for (JSONObject species : speciesToImport) {
                    String scientificName = species.getString(SpeciesKeys.SCIENTIFIC_NAME);
                    if (!speciesDAO.speciesExists(scientificName)) {
                        getAppDataSingleton().addSpecies(species);
                        numOfAddedRows++;
                    }
                }
            }
        }
    }

    /**
     * A task that runs on a background thread to generate export files and make them available to be
     * emailed using an email client.
     */
    private class ExportSpeciesTask extends AsyncTask<Void, Void, Void> {
        /**
         * Display the wait dialog.
         */
        @Override
        protected void onPreExecute() {
            waitDialog = new ProgressDialog(CONTEXT);
            waitDialog.setTitle(getString(R.string.title_dialog_exporting_species));
            waitDialog.setMessage(getString(R.string.message_dialog_import_species));
            waitDialog.setIndeterminate(true);
            waitDialog.setCancelable(false);
            waitDialog.show();
        }

        /**
         * Generate the files depending on the user's preferences and show a chooser to allow the
         * client to email out the data.
         *
         * @param params Unused
         * @return Nothing
         */
        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<JSONObject> sightings = getAppDataSingleton().getSightings();
            boolean generateGpx = getPrefs().getBoolean(getString(R.string.gpx_key), true);
            boolean generateKml = getPrefs().getBoolean(getString(R.string.kml_key), true);
            ArrayList<String> exportedFilenames = getFileHandler().createFiles(sightings, generateGpx, generateKml);

            Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            String defaultEmail = getPrefs().getString(getString(R.string.default_email_key), "");
            ArrayList<Uri> uris = new ArrayList<>();

            for (String filename : exportedFilenames) {
                if (filename != null && !filename.equals("")) {
                    Uri uri = Uri.fromFile(new File(filename));
                    uris.add(uri);
                }
            }

            emailIntent.setType("message/rfc822");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{defaultEmail});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Sightings Data");
            // Due to a bug in the Android framework this will generate a warning.
            // However it is completely harmless and safe to ignore.
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Data from the UgMedia Sightings Android app.");
            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            startActivity(Intent.createChooser(emailIntent, "Send Mail"));

            return null;
        }

        /**
         * Dismiss the wait dialog.
         *
         * @param aVoid Unused
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            if (waitDialog.isShowing()) {
                waitDialog.dismiss();
            }
        }
    }
}